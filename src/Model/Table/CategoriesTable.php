<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class CategoriesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('categories');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        
        $this->hasMany('Products', ['dependent' => false]);
        
        $this->hasMany('SubCategories', [
            'className' => 'Categories',
            'dependent' => false
        ]);

        $this->belongsTo('ParentCategories', [
            'foreignKey' => 'category_id',
            'className' => 'Categories',
            'dependent' => false
        ]);
        
    }

    public function validationDefault(Validator $validator)
    {
        return $validator;         
    }

}