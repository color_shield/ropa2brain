<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class OrdersTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        //nombre real de tabla en la base de datos 
        $this->table('orders');
        $this->addBehavior('Timestamp');

        //identificador primario de la tabla
        $this->primaryKey('id');

        //Relaciones
        
        
        $this->belongsTo('Statuses', [
           'dependent' => false
        ]);
        
        $this->HasMany('Suborders', [
           'dependent' => false
        ]);
    }

    public function validationDefault(Validator $validator) {
        return $validator;
    }

}