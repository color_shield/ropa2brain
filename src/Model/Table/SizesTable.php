<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SizesTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        //nombre real de tabla en la base de datos 
        $this->table('sizes');

        //identificador primario de la tabla
        $this->primaryKey('id');

        //Relaciones
        $this->belongsToMany('Properties', [
            'joinTable' => 'features',
            'dependent' => false
        ]);
        $this->belongsToMany('Products', [
            'joinTable' => 'features',
            'dependent' => false
        ]);        
        
    }

    public function validationDefault(Validator $validator) {
        return $validator;
    }

}