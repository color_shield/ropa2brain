<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class VinylsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        //nombre real de tabla en la base de datos 
        $this->table('vinyls');

        //identificador primario de la tabla
        $this->primaryKey('id');

        //Relaciones
        $this->belongsToMany('Options', [
            'dependent' => false,
        ]);
        $this->belongsToMany('Dyes', ['dependent' => false]);
    }

    public function validationDefault(Validator $validator) {
        return $validator;
    }

}
