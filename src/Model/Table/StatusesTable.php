<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class StatusesTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        //nombre real de tabla en la base de datos 
        $this->table('statuses');

        //identificador primario de la tabla
        $this->primaryKey('id');

        //Relaciones
        $this->hasMany('Products', [
            'dependent' => false,
        ]);
    }

    public function validationDefault(Validator $validator) {
        return $validator;
    }

}
