<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class AttributesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('attributes');
        $this->primaryKey('id');
        //$this->addBehavior('Timestamp');
        
        $this->belongsToMany('Products', ['dependent' => false]);
        
    }

    public function validationDefault(Validator $validator)
    {
        return $validator;         
    }

}