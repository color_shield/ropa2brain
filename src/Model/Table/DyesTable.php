<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class DyesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('dyes');
        $this->primaryKey('id');
        //$this->addBehavior('Timestamp');
                
        $this->belongsToMany('Vinyls', ['dependent' => false]);
    }

    public function validationDefault(Validator $validator)
    {
        return $validator;         
    }

}