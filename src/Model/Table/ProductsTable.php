<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        //nombre real de tabla en la base de datos 
        $this->table('products');
        $this->addBehavior('Timestamp');

        //identificador primario de la tabla
        $this->primaryKey('id');

        //Relaciones
        $this->belongsToMany('Properties', [
            'joinTable' => 'features',
            'dependent' => false
        ]);
        $this->belongsToMany('Sizes', [
            'joinTable' => 'features',
            'dependent' => false
        ]);
        $this->belongsTo('Templates', [
           'dependent' => false
        ]);
        $this->belongsTo('Categories', [
           'dependent' => false
        ]);
        $this->belongsToMany('Attributes', [
            'dependent' => false
        ]);
        
        
    }

    public function validationDefault(Validator $validator) {
        return $validator;
    }

}