<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class OptionsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('options');
        $this->primaryKey('id');
        //$this->addBehavior('Timestamp');
                
        $this->belongsToMany('Templates', ['dependent' => false]);
        
        $this->belongsToMany('Vinyls', [
            'dependent' => false,
        ]);
        $this->belongsToMany('Suborders', ['dependent' => false]);
        
    }

    public function validationDefault(Validator $validator)
    {
        return $validator;         
    }

}