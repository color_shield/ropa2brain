<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class OptionsSubordersTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        //nombre real de tabla en la base de datos 
        $this->table('options_suborders');

        //identificador primario de la tabla
        $this->primaryKey('id');

        //Relaciones
        
        $this->belongsTo('Options', [
           'dependent' => false
        ]);
        
        $this->belongsTo('Suborders', [
           'dependent' => false
        ]);
        
        $this->belongsTo('Vinyls', [
           'dependent' => false
        ]);
        
        $this->belongsTo('Dyes', [
           'dependent' => false
        ]);
        
        
        
    }

    public function validationDefault(Validator $validator) {
        return $validator;
    }

}