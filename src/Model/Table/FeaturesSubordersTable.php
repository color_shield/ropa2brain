<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class FeaturesSubordersTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        //nombre real de tabla en la base de datos 
        $this->table('features_suborders');

        //identificador primario de la tabla
        $this->primaryKey('id');

        //Relaciones
       
        $this->belongsTo('Features', [
           'dependent' => false
        ]);
        
        $this->belongsTo('Suborders', [
           'dependent' => false
        ]);
        
            
        
    }

    public function validationDefault(Validator $validator) {
        return $validator;
    }

}