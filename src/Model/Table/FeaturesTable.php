<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class FeaturesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('features');
        $this->primaryKey('id');
        //$this->addBehavior('Timestamp');
                
        $this->belongsTo('Properties', ['dependent' => false]);
        $this->belongsTo('Products', ['dependent' => false]);
        $this->belongsTo('Sizes', ['dependent' => false]);
        
    }

    public function validationDefault(Validator $validator)
    {
        return $validator;         
    }

}