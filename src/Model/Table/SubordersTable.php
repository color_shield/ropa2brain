<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SubordersTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        //nombre real de tabla en la base de datos 
        $this->table('suborders');

        //identificador primario de la tabla
        $this->primaryKey('id');

        //Relaciones
        $this->belongsTo('Orders', [
            'dependent' => false
        ]);
        
        /*$this->belongsTo('Products', [
           'dependent' => false
        ]);*/
        
        $this->belongsToMany('Features', [            
            'dependent' => false
        ]);
        
        $this->belongsToMany('Options', [
           'dependent' => false
        ]);
        
        $this->belongsToMany('Vinyls', [
            'joinTable' => 'options_suborders',
            'foreignKey' => 'vinyl_id',
            'dependent' => false
        ]);
        
        $this->belongsToMany('Dyes', [
            'joinTable' => 'options_suborders',
            'foreignKey' => 'dye_id',
            'dependent' => false
        ]);
        
        $this->HasMany('OptionsSuborders', [
           'dependent' => false
        ]);
        
    }

    public function validationDefault(Validator $validator) {
        return $validator;
    }

}
