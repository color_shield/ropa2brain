<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ColoursTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('colours');
        $this->primaryKey('id');
        //$this->addBehavior('Timestamp');
        
        $this->hasMany('Properties', ['dependent' => false]);
      
        
    }

    public function validationDefault(Validator $validator)
    {
        return $validator;         
    }

}