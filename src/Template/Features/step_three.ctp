<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="product-to-edit-title-cstm">
                <ol class="breadcrumb">
                    <li><?= $this->Html->link('Productos',['controller'=> 'Products', 'action' => 'productsList']); ?>  </li>
                    <li><a href="<?= $this->Url->build( $this->request->getRequestTarget(), true); ?>"><?= $product->name ?> - Stock</a> </li>
                  </ol>
            </div>
        </div>
        
        <div class="col-md-12">
            <?= $this->Form->create(null,['enctype' => 'multipart/form-data', 'id' => 'admin-form-stock', 'class' => 'no-zero-price-element-form']) ?>
            <fieldset>
                <legend><?= __('Cantidades y precios') ?></legend>
                
                <div class="col-md-12">                      
                    <div id="stock-starts-enabled" style="display: none;">1</div>                  
                </div>
                
                <div class="col-md-12">  
                    
                    <div class="multiple-selection-edit-wrap">
                        <div class="msew-main-title">Edición múltiple</div>
                        <div class="msew-title">
                            <?= __('Colores') ?>
                            [ <a class="edit-multiple-select-all" cstm-id="stock-edit-multiple-colors-content">Todos</a> | <a class="edit-multiple-select-none" cstm-id="stock-edit-multiple-colors-content">Ninguno</a> ]
                        </div>
                        <div class="msew-content" id="stock-edit-multiple-colors-content">
                            <?php $colour_ids_wrotte = []; ?>
                            <?php foreach($features as $feature): ?>
                                <?php if(!in_array($feature->property->colour->id, $colour_ids_wrotte)): ?>
                                    <?php $colour_ids_wrotte[] = $feature->property->colour->id; ?>
                                    <label for="msew-item-colour-<?= $feature->property->colour->id ?>">                                   
                                        <?= $this->Form->checkbox('multiple[]', [ 'value' => $feature->property->colour->id, 'class' => 'stock-edit-multiple-color-checkbox' ,'id' => 'msew-item-colour-'.$feature->property->colour->id ,'hiddenField' => false]); ?> <?= $feature->property->colour->name; ?>
                                    </label>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <div class="msew-title"><?= __('') ?></div>
                        <div class="msew-content" id="stock-edit-multiple-inputs-content">
                            <div class="col-md-6">
                                <p>Cantidad</p>
                                <?= $this->Form->input('quantity', ['type' => 'number', 'value' => '' , 'class' => 'form-control' ,'id' => 'stock-edit-multiple-input-quantity', 'required' => false, 'label' => false]) ?>
                            </div>
                            <div class="col-md-6">
                                <p>Precio</p>
                                <?= $this->Form->input('price', ['type' => 'number', 'value' => '' , 'step' =>'any', 'class' => 'form-control' ,'id' => 'stock-edit-multiple-input-price', 'required' => false, 'label' => false]) ?>
                            </div>
                            <?= $this->element('ElmEmptySpace', ['altura' => 15]) ?>
                        </div>
                        <div class="msew-submit">
                            <div class="btn btn-gold" id="stock-edit-multiple-input-btn"><i class="fa fa-check-square-o" aria-hidden="true"></i> Aplicar</div> 
                            <div class="btn btn-gold multiple-edit-options-input-clear-btn"><i class="fa fa-trash" aria-hidden="true"></i> Vaciar todas las opciones</div> 
                            <a class="btn btn-gold" href="<?= $this->Url->build( $this->request->getRequestTarget(), true); ?>"><i class="fa fa-refresh" aria-hidden="true"></i> Recargar página</a>
                        </div>
                    </div>
                        
                        
                    <table border="0" class="table" style="float: left; width: 100%" >
                        <thead>
                            <tr>
                                <td>
                                    <span><?= __('Color') ?></span>
                                </td>
                                <td>
                                    <span><?= __('Talla') ?></span>
                                </td>
                                <td>
                                    <span><?= __('Imagen') ?></span>
                                </td>
                                <td>
                                    <span><?= __('Cantidad') ?></span>
                                </td>                                
                                <td>
                                    <span><?= __('Precio') ?></span>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                    
                            <?php $cont = 0; ?>
                            <?php foreach($features as $feature): ?>
                                


                                <?= $this->Form->input('features['.$cont.'][id]', [ 'type' => 'hidden' , 'value' => $feature->id]); ?> 
                                <?= $this->Form->input('features['.$cont.'][product_id]', [ 'type' => 'hidden' , 'value' => $product->id]); ?> 


                                        <tr>
                                            <td>
                                                <img style="height: 35px;" src="<?= $this->Url->image('colours/'.$feature->property->colour->icon);  ?>" /> <?= $feature->property->colour->name ?> 
                                            </td>
                                            <td>
                                                <img style="height: 35px;" src="<?= $this->Url->image('sizes/'.$feature->size->icon);  ?>" /> <?= $feature->size->name ?>
                                            </td>
                                            <td>
                                                <img style="height: 35px;" src="<?= $this->Url->image('properties/'.$feature->property->image);  ?>" /> 
                                                
                                            </td>
                                            <td>                                        
                                                <?= $this->Form->input('features['.$cont.'][quantity]', ['type' => 'number', 'value' => $feature->quantity , 'class' => 'option-input-multiple-clear form-control stock-quantity-input stock-quantity-input-'.$feature->property->colour->id, 'required' => 'true', 'label' => false]) ?>
                                            </td>
                                            <td>                                        
                                                <?= $this->Form->input('features['.$cont.'][price]', ['type' => 'number', 'value' => $feature->price , 'step' =>'any', 'class' => 'option-input-multiple-clear form-control price-no-zero stock-price-input stock-price-input-'.$feature->property->colour->id, 'required' => 'true', 'label' => false]) ?>
                                            </td>
                                        </tr>
                                    


                                <?php $cont ++; ?>
                            <?php endforeach; ?>
                   
                        </tbody>
                    </table>
                                        
                    
                </div>                                
            </fieldset>
            <div class="col-md-12">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <?= $this->Form->submit(__('Guardar'), ['class' => 'btn btn-gold pull-right']); ?>
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>        
    </div>
</div>