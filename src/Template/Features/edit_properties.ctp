<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Busines $busines
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="product-to-edit-title-cstm">
                <ol class="breadcrumb">
                    <li><?= $this->Html->link('Productos',['controller'=> 'Products', 'action' => 'productsList']); ?>  </li>
                    <li><a href="<?= $this->Url->build( $this->request->getRequestTarget(), true); ?>"><?= $product->name ?> - Edicion colores</a> </li>
                  </ol>
            </div>
        </div>
        
        <div class="col-md-12">
            <?= $this->Form->create(null,['enctype' => 'multipart/form-data', 'class'=>'almost-one-element-form']) ?>
            <fieldset>
                <legend><?= __('Seleccionar colores') ?></legend>
                          
                   
                    <?php $count = 0; ?>
                    <?php foreach($colours as $colour): ?>
                        <div class="row property-color-element" id="property-color-<?= $colour->id ?>" >  
                                 
                                <?php
                                    $checked = false;
                                    $img_src = '';
                                    $property_id = null;
                                    $start_state = 0;
                                    $initial_text_action = 'Sin acción';
                                    $css_class_action = 'grey-msg';
                                    foreach($product->properties as $property){
                                        if($property->colour_id == $colour->id){
                                            $property_id = $property->id;
                                            $checked = true;
                                            $img_src = $property->image;
                                            $start_state = 1;
                                            $initial_text_action = 'No modificar';
                                            $css_class_action = 'grey-msg';
                                        }
                                    }
                                ?>
                            
                            <?= $this->Form->input('properties['.$count.'][id]', [ 'type' => 'hidden' , 'value' => $property_id]); ?> 
                            <div class="col-md-1">     
                                <label class="checkbox-custom">
                                    <?= $this->Form->checkbox('properties['.$count.'][colour_id]', [ 'value' => $colour->id, 'start-state' => $start_state, 'middle-state' => 0, 'checked' => $checked, 'class' => 'edit-properties-form-element-check' ,'id' => 'colour_id_'.$colour->id ,'hiddenField' => false]); ?> 
                                    <span class="checkmark"></span>
                                </label>
                                </div> 
                            <div class="col-md-2">    
                                <label class="label-for-colour" for="colour_id_<?= $colour->id ?>"><img src="<?= $this->Url->image('colours/'.$colour->icon);  ?>" /><span><?= $colour->name ?></span></label>
                            </div> 
                            <div class="col-md-4">    
                                <div class="upload-btn-wrapper">
                                    <button class="btn">Seleccionar archivo</button>
                                    <?= $this->Form->file('properties['.$count.'][image]', ['type' => 'file', 'class' => 'edit-properties-form-element-image' ]); ?>
                                </div>
                                <div class="edit-properties-form-name-file-new-image"><span class="image-name">Ninguna imagen seleccionada</span> <span class="edit-properties-form-clear-new-image"><i class="fa fa-times"></i></span></div>
                                
                            </div> 
                            <div class="col-md-3">    
                                <img class="image-preview-edit-property" src="<?= $this->Url->image('properties/'.$img_src);  ?>" /> 
                            </div> 
                            <div class="col-md-2">    
                                <div class="edit-properties-form-element-msg <?= $css_class_action ?>" cstm-id="<?= $colour->id ?>"><?= $initial_text_action ?></div>
                            </div>  
                        </div>  
                    
                        <?php $count ++; ?>
                    <?php endforeach; ?>
                                        
                    <div class="col-md-12">
                        <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                        <?= $this->Form->submit(__('Siguiente'), ['class' => 'btn btn-gold pull-right']); ?>
                        <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                    </div>
                                            
            </fieldset>
            
            <?= $this->Form->end() ?>
        </div>        
    </div>
</div>