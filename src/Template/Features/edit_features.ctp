<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Busines $busines
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="product-to-edit-title-cstm">
                <ol class="breadcrumb">
                    <li><?= $this->Html->link('Productos',['controller'=> 'Products', 'action' => 'productsList']); ?>  </li>
                    <li><a href="<?= $this->Url->build( $this->request->getRequestTarget(), true); ?>"><?= $product->name ?> - Edicion tallas</a> </li>
                  </ol>
            </div>
        </div>
        
        <div class="col-md-12">
            <?= $this->Form->create(null,['enctype' => 'multipart/form-data', 'class'=>'almost-one-element-form']) ?>
            <fieldset>
                <legend><?= __('Seleccionar Tallas') ?></legend>
                
                    <div class="multiple-selection-edit-wrap">
                        <div class="msew-main-title">Edición múltiple</div>
                        <div class="msew-title">
                            <?= __('Colores') ?> 
                            [ <a class="edit-multiple-select-all" cstm-id="edit-feautes-edit-multiple-colors-content">Todos</a> | <a class="edit-multiple-select-none" cstm-id="edit-feautes-edit-multiple-colors-content">Ninguno</a> ]
                        </div>
                        <div class="msew-content" id="edit-feautes-edit-multiple-colors-content">
                            <?php $colour_ids_wrotte = []; ?>
                            <?php foreach($properties as $property): ?>
                                <?php if(!in_array($property->colour->id, $colour_ids_wrotte)): ?>
                                    <?php $colour_ids_wrotte[] = $property->colour->id; ?>
                                    <label for="msew-item-colour-<?= $property->colour->id ?>">
                                        <?= $this->Form->checkbox('multiple[]', [ 'value' => $property->colour->id, 'class' => 'edit-feautes-edit-multiple-color-checkbox' ,'id' => 'msew-item-colour-'.$property->colour->id ,'hiddenField' => false]); ?> 
                                        <span><?= $property->colour->name; ?></span>
                                    </label>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <div class="msew-title">
                            <?= __('Tallas') ?>
                            [ <a class="edit-multiple-select-all" cstm-id="edit-feautes-edit-multiple-inputs-content">Todos</a> | <a class="edit-multiple-select-none" cstm-id="edit-feautes-edit-multiple-inputs-content">Ninguno</a> ]
                        </div>
                        <div class="msew-content" id="edit-feautes-edit-multiple-inputs-content">
                            <?php $sizes_ids_wrotte = []; ?>
                            <?php foreach($sizes as $size): ?>
                                <?php if(!in_array($size->id, $sizes_ids_wrotte)): ?>
                                    <?php $sizes_ids_wrotte[] = $size->id; ?>
                                    
                                    <label for="msew-item-size-<?= $size->id ?>">
                                        <?= $this->Form->checkbox('multiple_sizes[]', [ 'value' => $size->id, 'class' => 'edit-feautes-multiple-size-checkbox' ,'id' => 'msew-item-size-'.$size->id ,'hiddenField' => false]); ?> 
                                        <span><?= $size->name; ?></span>
                                    </label>
                                    <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <div class="msew-submit">
                            <div class="btn btn-gold" id="edit-feautes-edit-multiple-input-btn"><i class="fa fa-check-square-o" aria-hidden="true"></i> Aplicar</div> 
                            <div class="btn btn-gold multiple-edit-options-clear-btn"><i class="fa fa-trash" aria-hidden="true"></i> Desmarcar todas las opciones</div> 
                            <a class="btn btn-gold" href="<?= $this->Url->build( $this->request->getRequestTarget(), true); ?>"><i class="fa fa-refresh" aria-hidden="true"></i> Recargar página</a>
                        </div>
                    </div>
                   
                    
                
                    <?php $cont = 0; ?>
                    <?php foreach($properties as $property): ?>
                        <div class="col-md-12">
                            <div class="edit-feature-color-title"><span><?= $property->colour->name ?></span><img src="<?= $this->Url->image('colours/'.$property->colour->icon);  ?>" /></div>


                            <?php foreach($sizes as $size): ?>
                                <?php
                                    $checked = false;
                                    $feature_id = null;

                                    foreach ($features as $current_feature){
                                        if($current_feature->property->colour_id == $property->colour_id && $current_feature->size_id == $size->id){
                                            $checked = true;
                                            $feature_id = $current_feature->id;
                                        }
                                    }

                                ?>
                                <div class="col-md-3">
                                    <label class="edit-feature-size-element" for="size_id_<?= $size->id ?>_<?= $property->id ?>">
                                        <?= $this->Form->input('features['.$cont.'][id]', [ 'type' => 'hidden' , 'value' => $feature_id]); ?> 
                                        <?= $this->Form->input('features['.$cont.'][product_id]', [ 'type' => 'hidden' , 'value' => $product->id]); ?> 
                                        <?= $this->Form->input('features['.$cont.'][property_id]', [ 'type' => 'hidden' , 'value' => $property->id]); ?> 
                                        <div class="col-md-2 checkbox-size">
                                            <label class="checkbox-custom">
                                                <?= $this->Form->checkbox('features['.$cont.'][size_id]', [ 'value' => $size->id, 'id' => 'size_id_'.$size->id.'_'.$property->id, 'class' => 'option-multiple-clear edit-feature-input-size-'.$size->id.' edit-feature-input-color-'.$property->colour_id , 'checked' => $checked,'hiddenField' => false]); ?> 
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-3 name-size">
                                            <?= $size->name ?>
                                        </div>
                                        <div class="col-md-7 img-size">
                                            <img src="<?= $this->Url->image('sizes/'.$size->icon);  ?>" />
                                        </div>
                                    </label>
                                </div>
                        
                            <?php $cont ++; ?>
                        <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                                        
                                                   
            </fieldset>
            <div class="col-md-12">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <?= $this->Form->submit(__('Siguiente'), ['class' => 'btn btn-gold pull-right']); ?>
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>        
    </div>
</div>