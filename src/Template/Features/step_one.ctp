<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Busines $busines
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->Form->create(null,['enctype' => 'multipart/form-data','class'=>'almost-one-element-form']) ?>
            <fieldset>
                <legend><?= __('Seleccionar colores') ?></legend>
                <div class="col-md-12">                        
                    <span><?= __('colores') ?></span>
                   
                    <?php $count = 0; ?>
                    <?php foreach($colours as $colour): ?>
                    
                    
                        <div class="row property-color-element" id="property-color-<?= $colour->id ?>" >  
                           
                            <div class="col-md-1">     
                                <label class="checkbox-custom">
                                    <?= $this->Form->checkbox('properties['.$count.'][colour_id]', [ 'value' => $colour->id, 'id' => 'colour_id_'.$colour->id , 'class' => 'edit-properties-form-element-check', 'hiddenField' => false]); ?> 
                                    <span class="checkmark"></span>
                                </label>
                                </div> 
                            <div class="col-md-2">    
                                <label class="label-for-colour" for="colour_id_<?= $colour->id ?>"><img src="<?= $this->Url->image('colours/'.$colour->icon);  ?>" /><span><?= $colour->name ?></span></label>
                            </div> 
                            <div class="col-md-4">    
                                <div class="upload-btn-wrapper">
                                    <button class="btn">Seleccionar archivo</button>
                                    <?= $this->Form->file('properties['.$count.'][image]', ['type' => 'file', 'class' => 'edit-properties-form-element-image' ]); ?>                                    
                                </div>
                                <div class="edit-properties-form-name-file-new-image"><span class="image-name">Ninguna imagen seleccionada</span> <span class="edit-properties-form-clear-new-image"><i class="fa fa-times"></i></span></div>
                                
                            </div> 
                                                         
                        </div>  
                    
                        <?php $count ++; ?>
                    <?php endforeach; ?>
                                        
                    
                </div>                                
            </fieldset>
            <div class="col-md-12">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <?= $this->Form->submit(__('Siguiente'), ['class' => 'btn btn-primary pull-right']); ?>
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>        
    </div>
</div>