<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
            <?= $this->Form->create($dye, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Editar color de vinilo') ?></legend>            
                <div class="col-md-12">                        
                    <span><?= __('Nombre del color de vinilo') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'label' => false]) ?>
                </div>                
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Icono del color de vinilo') ?></span>
                    <p class="text-left"><?= $this->Html->image('dyes/' . $dye->icon, ['alt' => 'Icono del color de vinilo correspondiente']); ?></p>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Nueva imagen del color') ?></span>
                    <?= $this->Form->input('new_image', ['type' => 'file', 'class' => 'form-control', 'label' => false]) ?>
                </div>
            </fieldset>
            <div class="col-md-12">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
                <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver',['action' => 'dyes_list'],['class'=>'btn btn-primary','escape'=> false]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>