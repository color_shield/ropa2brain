<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <title><?= $this->fetch('title') ?></title>
</head>
<body>
    <div class="container">
        <div class="row">
            <h2 class="text-center">Correo informativo desde ropa.2brain.com</h2>
            <hr>
            <br/>
            <p>Estimado cliente, usted ha realizado un nuevo pedido en Ropa.2Brain.es con identicador: <?= $order_id ?></p>
            <p>Su pedido con identificador Nº<?= $order_id ?> está siendo procesado en estos momentos. En breve nos pondremos en contacto con usted.</p>
            <p>En caso de que necesite contactar con nosotros, utilice el formulario que tiene a su disposición en esta página: </p>
            <p><a href="https://ropa.2brain.es/store/contact-us">Formulario de contacto de ropa.2brain.es</a></p>
            <p>Si su consulta es referente al pedido, no olvide indicar el identificador del mismo en el mensaje del formulario.</p>
            <p>Esperamos que tenga un buen día y que lo disfrute.</p>
            <hr>
            <br/>
            <br/>
        </div>
    </div>
    <?= $this->fetch('content') ?>
    <p style="text-align: center;"><img alt="Stamping 2Brain logo" title="Stamping 2Brain logo" src="https://ropa.2brain.es/img/logo_horizontal_xs.png"/></p>
</body>
</html>