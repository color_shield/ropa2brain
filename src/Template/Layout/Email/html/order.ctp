<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <title><?= $this->fetch('title') ?></title>
</head>
<body>
    <div class="container">
        <div class="row">
            <h2 class="text-center">Correo de aviso de pedido desde ropa.2brain.com</h2>
            <hr>
            <br/>
            <p>Se ha realizado un nuevo pedido en Ropa.2Brain.es con identicador: <?= $order_id ?></p>
            <p>Vaya a web de administración para gestionar el pedido y sus detalles.</p>
            <hr>
            <br/>
            <br/>
        </div>
    </div>
    <?= $this->fetch('content') ?>
    <p style="text-align: center;"><img alt="Stamping 2Brain logo" title="Stamping 2Brain logo" src="https://ropa.2brain.es/img/logo_horizontal_xs.png"/></p>
</body>
</html>