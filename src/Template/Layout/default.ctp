<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>
        
        <?= $this->Html->script('global_vars.js'); ?>

        <!-- JQuery -->
        <?= $this->Html->script('jquery.js'); ?>
        <!-- Fin JQuery -->

        <!-- JQueryUI -->
        <!--<?= $this->Html->script('/webroot/plugins/jqueryui/jquery-ui.min.js'); ?>-->
        <!--<?= $this->Html->css('../plugins/jqueryui/jquery-ui.min.css'); ?>-->
        <!--<?= $this->Html->css('../plugins/jqueryui/jquery-ui.structure.min.css'); ?>-->
        <!-- Fin JQueryUI -->

        <!-- BootStrap -->
        <?= $this->Html->css('../plugins/bootstrap/css/bootstrap.min.css'); ?>
        <?= $this->Html->script('/webroot/plugins/bootstrap/js/bootstrap.min.js'); ?>
        <!-- Fin BootStrap -->

        <!-- FontAwesome -->
        <?= $this->Html->css('/plugins/font-awesome/css/font-awesome.min.css'); ?>
        <!-- Fin FontAwesome -->
        
        
        <!-- colorpicker -->
        <?= $this->Html->css('/plugins/colorpicker/css/colorpicker.css'); ?>
        <?= $this->Html->script('/webroot/plugins/colorpicker/js/colorpicker.js'); ?>
        <!-- Fin colorpicker -->
        
        <!-- JavaScripts propios -->
        <?= $this->Html->script('jjscript.js'); ?>
        <?= $this->Html->script('mmscript.js'); ?>
        <!-- Fin Estilos propios -->


        <?= $this->Html->css('Main.css') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <?= $this->Flash->render() ?>
        <?= $this->element('ElmAdminMainMenu', []) ?>
        <div class="main-content">
        <?= $this->fetch('content') ?>
        </div>
        <footer>
            
        </footer>
    </body>
</html>
