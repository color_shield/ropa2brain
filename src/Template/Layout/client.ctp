<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="La mejor web de estampación. Crea y personaliza tus camisetas, polos, sudaderas y casi cualquier prenda que puedas imaginar en ropa.2brain.es.">
        <meta name="keywords" content="Estampación, estampar, personalizar, camisetas, sudaderas, polos, mochilas, totebags, polares, gorras, delantales, sudaderas, pañuelos, bodys, vinilo, metalizado, glitter, textura, flúor, deformable">
        <meta name="robots" content="index,follow">
        <meta name="generator" content="CakePHP 3.x">
        <meta name="google-site-verification" content="fbgKT7rg3WBGcMsNPOpDAKKFxUo2z9T-60ZW1fZWxN8" />
        <meta name="author" content="Webcoden">
        <meta name="DC.title" content="ropa.2brain.es estampación para cualquier prenda y totalmente personalizado.">
        <meta name="DC.creator" content="Webcoden">
        <meta name="DC.description" content="La mejor web de estampación. Crea y personaliza tus prendas en ropa.2brain.es.">
        <meta property="og:title" content="ropa.2brain.es estampación para cualquier prenda y totalmente personalizado.">
        <meta property="og:description" content="La mejor web de estampación. Crea y personaliza tus camisetas, polos, sudaderas y casi cualquier prenda que puedas imaginar en ropa.2brain.es.">
        <meta property="og:type" content="website">
        <meta property="og:url" content="https://ropa.2brain.es/">
        <meta property="og:image" content="https://ropa.2brain.es/img/logo_horizontal_xs.png">
        <meta property="og:site_name" content="ropa.2brain.es">
        <title>Ropa.2Brain.es estampación para cualquier prenda y totalmente personalizado.</title>
                
        <?= $this->Html->meta('icon') ?>
        
        <?= $this->Html->script('global_vars.js'); ?>

        <!-- JQuery -->
        <?= $this->Html->script('jquery.js'); ?>
        <!-- Fin JQuery -->

        <!-- JQueryUI -->
        <?= $this->Html->script('/webroot/plugins/jqueryui/jquery-ui.min.js'); ?>
        <?= $this->Html->css('../plugins/jqueryui/jquery-ui.min.css'); ?>
        <!--<?= $this->Html->css('../plugins/jqueryui/jquery-ui.structure.min.css'); ?>-->
        <!-- Fin JQueryUI -->

        <!-- BootStrap -->
        <?= $this->Html->css('../plugins/bootstrap/css/bootstrap.min.css'); ?>
        <?= $this->Html->script('/webroot/plugins/bootstrap/js/bootstrap.min.js'); ?>
        <!-- Fin BootStrap -->

        <!-- FontAwesome -->
        <?= $this->Html->css('/plugins/font-awesome/css/font-awesome.min.css'); ?>
        <!-- Fin FontAwesome -->
        
        
        <!-- colorpicker -->
        <?= $this->Html->css('/plugins/colorpicker/css/colorpicker.css'); ?>
        <?= $this->Html->script('/webroot/plugins/colorpicker/js/colorpicker.js'); ?>
        <!-- Fin colorpicker -->
        
        <!-- JavaScripts propios -->
        <?= $this->Html->script('jjscript.js'); ?>
        <?= $this->Html->script('mmscript.js'); ?>
        <!-- Fin Estilos propios -->


        <?= $this->Html->css('Main.css') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <?= $this->Flash->render() ?>
        <?= $this->element('ElmClientMainMenu', []) ?>
        <div class="main-content">
        <?= $this->fetch('content') ?>
        </div>
        <footer>
            <?= $this->element('ElmBasicFooter', []) ?>
        </footer>
    </body>
</html>
