<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Vinilos Básicos</h2>
                <p class="text-center"><?= $this->Html->image('Separador.png', ['alt' => 'Separador de textos', 'id' => '', 'class' => 'img-responsive']); ?></p>
                <p><?= $this->Html->image('vinyls_type/Vinilos basicos.jpg', ['alt' => 'Nuestros vinilos de tipo básico', 'title'=>'Nuestros vinilos de tipo básico', 'id' => '', 'class' => 'img-responsive']); ?></p>
                <p class="text-justify font_18_px">Esta gama, más económica de Vinilo de Corte Textil, BÁSICO, se caracteriza especialmente por disponer de más de 50 colores.</p>
                <p class="text-justify font_18_px">Recomendamos este tipo de vinilo, por su amplia variedad de colores y precio, para prendas más económicas; despedidas de soltero o soltera, ropa para peñas, prendas para colegios, ropa laboral,...</p>
                <p class="text-justify font_18_px">Está compuesta de poliuretano ecológico fundido y todos sus colores tienen una excelente opacidad y superficie mate sin reflejos. Es adecuado para la transferencia térmica sobre textiles de algodón, mezclas poliéster/algodón y acrílicos.</p>
                <p class="text-justify font_18_px">Usamos este tipo de vinilo por su elasticidad, tacto agradable y suave. Lo aplicamos sobre las prendas por medio de calor con plancha térmica, de esta forma nos aseguramos la perduración del diseño en el tiempo.</p>
                <p class="text-justify font_18_px">Recomendamos lavar la prenda del revés en frío. No planchar directamente; sino planchar del revés o con un paño por encima (no poner la plancha en contacto directo con el estampado).</p>
            </div>
        </div>
    </div>
</section>