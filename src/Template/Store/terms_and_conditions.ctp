<section>
    <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-justify">Términos y condiciones</h2>
                <hr>
                <h3 class="text-justify">Política de devoluciones</h3>
                <h4 class="text-justify">GARANTÍA TOTAL en vuestras devoluciones</h4>
                <p class="text-justify font_18_px"><strong>Dispone de 14 días</strong>, desde la recepción del pedido para cambiar o devolver cualquier artículo en mal estado o deteriorado que no haya sido estampado o personalizado con algún error cometido por nuestra empresa a la hora de elaboración de diseños, fotolitos o pantallas.</p>
                <p class="text-justify font_18_px">¡Usted elige! se lo podemos cambiar o si lo prefiere le devolvemos el dinero incluidos los gastos de transporte, ¡sin problemas!</p>
                <p class="text-justify font_18_px">Salvo prueba de lo contrario, se entenderá que los productos sean conformes con el presupuesto y factura remitidos vía email siempre que cumplan todos los requisitos que se expresan a continuación:</p>
                <ul style="text-align:justify;list-style-type:circle;">
                    <li class="text-justify font_18_px">Se ajusten a la descripción realizada por nuestra empresa y posean las cualidades del producto que le hemos vendido en forma de muestra o modelo (previa solicitud de muestras vía email).</li>
                    <li class="text-justify font_18_px">Sean aptos para los usos a que ordinariamente se destinen los productos del mismo tipo.</li>
                    <li class="text-justify font_18_px">Presenten la calidad y prestaciones habituales en un producto del mismo tipo que el cliente pueda fundamentalmente esperar, habida cuenta de la naturaleza del producto dentro de la categoría de textil y regalo publicitario.</li>
                </ul>
                <h3 class="text-justify">CAMBIOS EN PEDIDOS</h3>
                <p class="text-justify font_18_px"><strong>Una vez realizado el pago de su pedido supone la confirmación del mismo y no podrá ser modificado en cuanto a prendas, colores, cantidad, estampación.</strong></p>
                <p class="text-justify font_18_px">Si precisa realizar alguna modificación en el presupuesto indíquelo por favor, antes de su aceptación mediante un <a title="Cambios en el pedido" href="mailto:info@2brain.es" target="_blank" class="button_2brain_style">email</a> indicando la referencia o código del documento remitido.</p>
                <h3 class="text-justify">NO SE ACEPTARÁN PRENDAS SUMINISTRADAS POR CLIENTES</h3>
                <p class="text-justify font_18_px">No admitiremos prendas proporcionadas por los clientes, debido a que si se estropea alguna, no podrá realizarse reposición de la misma. Además el objetivo de la empresa, es la venta de nuestras prendas personalizadas.</p>
                <h3 class="text-justify">NO SE ACEPTARÁN DEVOLUCIONES DE</h3>
                <ul style="text-align:justify;list-style-type:circle;">
                    <li class="text-justify font_18_px">Prendas o artículos personalizados o estampados.</li>
                    <li class="text-justify font_18_px">Prendas o artículos manchados.</li>
                    <li class="text-justify font_18_px">Prendas devueltas sin su embalaje original, podrán sufrir una depreciación de su valor real.</li>
                    <li class="text-justify font_18_px">Pedidos procesados y remitidos por agencia de transportes, si desea comprobación de tallas, colores o calidades solicite muestras o póngase en contacto con nuestros comerciales para que puedan asesorarle.</li>
                </ul>
            </div>
        </div>
    </div>
    <?= $this->element('ElmEmptySpace', ['altura' => 150]) ?>
</section>
