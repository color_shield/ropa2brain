<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Vinilos con Motivos</h2>
                <p class="text-center"><?= $this->Html->image('Separador.png', ['alt' => 'Separador de textos', 'id' => '', 'class' => 'img-responsive']); ?></p>
                <p><?= $this->Html->image('vinyls_type/Vinilos motivos.jpg', ['alt' => 'Nuestros vinilos de tipo motivo', 'title'=>'Nuestros vinils de tipo motivo', 'id' => '', 'class' => 'img-responsive']); ?></p>
                <p class="text-justify font_18_px"><em>Sección en desarrollo</em></p>
                <p class="text-justify font_18_px">Finalizaremos esta sección lo antes posible, lamentamos las molestias.</p>
                <p class="text-justify font_18_px"></p>
                <p class="text-justify font_18_px"></p>
                <p class="text-justify font_18_px"></p>
            </div>
        </div>
    </div>
</section>