<section id="store_vinyls_type">
    <div class="container">
        <?= $this->element('ElmEmptySpace', ['altura' => 70]) ?>
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Tipos de vinilo</h2>
                <hr>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="vinyl_container">
                        <a href="<?= $this->Url->build(["controller" => "Store","action" => "vinyl_basic"]); ?>">
                           <p class="text-center vinyls_container_title_basicos">Básicos</p>

                            <?= $this->Html->image('vinyls_type/Vinilos basicos.jpg', ['alt' => 'Vinilos básicos', 'title'=>'Vinilos básicos', 'id' => '', 'class' => 'img-responsive']); ?>
                        </a>        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="vinyl_container">
                        <a href="<?= $this->Url->build(["controller" => "Store","action" => "vinyl_deformable"]); ?>">
                           <p class="text-center vinyls_container_title">Deformables</p>

                            <?= $this->Html->image('vinyls_type/Vinilos deformables.jpg', ['alt' => 'Vinilos deformables', 'title'=>'Vinilos deformables', 'id' => '', 'class' => 'img-responsive']); ?>
                        </a>        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="vinyl_container">
                        <a href="<?= $this->Url->build(["controller" => "Store","action" => "vinyl_special"]); ?>">
                           <p class="text-center vinyls_container_title">Especiales</p>

                            <?= $this->Html->image('vinyls_type/Vinilos especiales.jpg', ['alt' => 'Vinilos especiales', 'title'=>'Vinilos especiales', 'id' => '', 'class' => 'img-responsive']); ?>
                        </a>        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="vinyl_container">
                        <a href="<?= $this->Url->build(["controller" => "Store","action" => "vinyl_fluor"]); ?>">
                           <p class="text-center vinyls_container_title">Flúor</p>

                            <?= $this->Html->image('vinyls_type/Vinilos fluor.jpg', ['alt' => 'Vinilos flúor', 'title'=>'Vinilos flúor', 'id' => '', 'class' => 'img-responsive']); ?>
                        </a>        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="vinyl_container">
                        <a href="<?= $this->Url->build(["controller" => "Store","action" => "vinyl_glitter"]); ?>">
                           <p class="text-center vinyls_container_title">Glitter</p>

                            <?= $this->Html->image('vinyls_type/Vinilos glitter.jpg', ['alt' => 'Vinilos glitter', 'title'=>'Vinilos glitter', 'id' => '', 'class' => 'img-responsive']); ?>
                        </a>        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="vinyl_container">
                        <a href="<?= $this->Url->build(["controller" => "Store","action" => "vinyl_metalized"]); ?>">
                           <p class="text-center vinyls_container_title">Metalizados</p>

                            <?= $this->Html->image('vinyls_type/Vinilos metalizados.jpg', ['alt' => 'Vinilos metalizados', 'title'=>'Vinilos metalizados', 'id' => '', 'class' => 'img-responsive']); ?>
                        </a>        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="vinyl_container">
                        <a href="<?= $this->Url->build(["controller" => "Store","action" => "vinyl_motive"]); ?>">
                           <p class="text-center vinyls_container_title">Motivos</p>

                            <?= $this->Html->image('vinyls_type/Vinilos motivos.jpg', ['alt' => 'Vinilos de motivos', 'title'=>'Vinilos de motivos', 'id' => '', 'class' => 'img-responsive']); ?>
                        </a>        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="vinyl_container">
                        <a href="<?= $this->Url->build(["controller" => "Store","action" => "vinyl_textured"]); ?>">
                           <p class="text-center vinyls_container_title">Texturas</p>

                            <?= $this->Html->image('vinyls_type/Vinilos texturas.jpg', ['alt' => 'Vinilos con textura', 'title'=>'Vinilos con textura', 'id' => '', 'class' => 'img-responsive']); ?>
                        </a>        
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <p class="text-justify"></p>
            </div>
        </div>
    </div>
</section>