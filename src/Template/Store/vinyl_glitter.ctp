<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Vinilos Glitter</h2>
                <p class="text-center"><?= $this->Html->image('Separador.png', ['alt' => 'Separador de textos', 'id' => '', 'class' => 'img-responsive']); ?></p>
                <p><?= $this->Html->image('vinyls_type/Vinilos glitter.jpg', ['alt' => 'Nuestros vinilos de tipo glitter', 'title'=>'Nuestros vinilos de tipo glitter', 'id' => '', 'class' => 'img-responsive']); ?></p>
                <p class="text-justify font_18_px">Vinilo textil termo adhesivo con partículas brillantes, éste tiene un recubrimiento transparente para evitar el desgaste por roce o lavados, y a su vez dotar al mismo de un tacto final agradable y suave. Recomendamos el uso de este tipo de vinilo para prendas que requieran de algo más de sofisticación; camisetas promocionales, prendas para empresas, ropa laboral, eventos especiales,...</p>
                <p class="text-justify font_18_px">Apto para uso general, prendas y accesorios con acabado textil con composición de algodón, poliéster, algodón-poliéster o poliéster-acrílico como camisetas, sudaderas, polos, gorras, bolsos, prendas deportivas, etc. Libre de PVC, grupos nítricos y metales pesados cumplimentando así las normas de protección medioambiental y de la salud.</p>
                <p class="text-justify font_18_px">Recomendamos lavar la prenda del revés en frío. No planchar directamente; sino planchar del revés o con un paño por encima (no poner la plancha en contacto directo con el estampado).</p>                
            </div>
        </div>
    </div>
</section>