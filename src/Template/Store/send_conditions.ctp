<section>
    <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-justify">Envíos y devoluciones</h2>
                <hr>
                <h3 class="text-justify">Envío del paquete</h3>
                <p class="text-justify font_18_px">Como norma general, los paquetes se envían dentro de las 48 horas siguientes a la recepción del pago, través de UPS con número de seguimiento y entrega sin firma. Si prefieres el envío certificado mediante UPS Extra, se aplicará un cargo adicional. Ponte en contacto con nosotros antes de solicitar esta opción. Sea cual sea la forma de envío que elijas, te proporcionaremos un enlace para que puedas seguir tu pedido en línea.</p>
                <p class="text-justify font_18_px">Los gastos de envío incluyen los gastos de manipulación y empaquetado, así como los gastos postales. Los gastos de manipulación tienen un precio fijo, mientras que los gastos de transporte pueden variar según el peso total del paquete. Te aconsejamos que agrupes todos tus artículos en un mismo pedido. No podemos combinar dos pedidos diferentes, y los gastos de envío se aplicarán para cada uno de manera individual. No nos hacemos responsables de los daños que pueda sufrir tu paquete tras el envío, pero hacemos todo lo posible para proteger todos los artículos frágiles.</p>
                <p class="text-justify font_18_px">Las cajas son grandes y tus artículos estarán bien protegidos.</p>
            </div>
        </div>
    </div>
    <?= $this->element('ElmEmptySpace', ['altura' => 150]) ?>
</section>