<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Vinilos Deformables</h2>
                <p class="text-center"><?= $this->Html->image('Separador.png', ['alt' => 'Separador de textos', 'id' => '', 'class' => 'img-responsive']); ?></p>
                <p><?= $this->Html->image('vinyls_type/Vinilos deformables.jpg', ['alt' => 'Nuestros vinilos de tipo deformable', 'title'=>'Nuestros vinilos de tipo deformable', 'id' => '', 'class' => 'img-responsive']); ?></p>
                <p class="text-justify font_18_px">Vinilo textil termoadhesivo especialmente indicado para tejidos difíciles, como telas elásticas. Nylon, tejidos impermeables o tejidos hidrófugos (altamente repelentes al agua).</p>
                <p class="text-justify font_18_px"><em>Recomendamos el uso de este tipo de vinilo para prendas que vayan a someterse a más procesos de elasticidad; ropa deportiva, bañadores, camisetas running, ropa de competición...</em></p>
                <p class="text-justify font_18_px">Apto para uso general, prendas y accesorios con acabado textil con composición de algodón, poliéster, algodón-poliéster o poliéster-acrílico como camisetas, sudaderas, polos, gorras, bolsos, prendas deportivas, etc. Libre de PVC, grupos nítricos y metales pesados cumplimentando así las normas de protección medioambiental y de la salud.</p>
                <p class="text-justify font_18_px"><em>Recomendamos lavar la prenda del revés en frío. No planchar directamente; sino planchar del revés o con un paño por encima (no poner la plancha en contacto directo con el estampado).</em></p>                
            </div>
        </div>
    </div>
</section>