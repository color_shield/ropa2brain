<section>
    <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3" id="contact_us_store_info_container">
                <h3 class="text-uppercase">información de la tienda</h3>
                <p class="text-center"><i class="fa fa-map-marker contact_us_icon"></i></p>
                <p class="text-center font_18_px">Stamping por 2brain.es</p>
                <p class="text-center font_18_px">c/ El Amacal 7</p>
                <p class="text-center font_18_px">34419 Villalobón</p>
                <p class="text-center font_18_px">Palencia</p>
                <p class="text-center font_18_px">España</p>
                <p class="text-center"><i class="fa fa-phone contact_us_icon"></i></p>
                <p class="text-center"><a href="tel:619914524">619914524</a></p>
                <p class="text-center"><i class="fa fa-envelope contact_us_icon"></i></p>
                <p class="text-center"><a href="mailto:ropa@2brain.es">ropa@2brain.es</a></p>                
            </div>            
            <div class="col-xs-12 col-sm-8 col-md-8" id="contact_us_form">
                <?= $this->Form->create(null, ['url' => ['controller'=>'store','action' => 'contact_us'], 'enctype' => 'multipart/form-data', 'type' => 'post', 'id' => '', 'class' => '']) ?>
                <?= $this->Form->input('role_form', ['type' => 'hidden', 'value' => "contact_us", 'label' => false]) ?>
                    <section class="form-fields">
                        <div class="form-group row">
                            <div class="col-md-9 col-md-offset-3">
                                <h3>Contáctenos</h3>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Asunto</label>
                            <div class="col-md-6">
                                <select name="contact_destination" class="form-control form-control-select">
                                    <option value="Consulta">Consulta</option>
                                    <option value="ServicioCliente">Servicio al cliente</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Nombre</label>
                            <div class="col-md-6">
                                <input class="form-control" name="contact_name" type="text" value="" placeholder="Escriba su nombre..." required="true">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Dirección email</label>
                            <div class="col-md-6">
                                <input class="form-control" name="from" type="email" value="" placeholder="tu@email.com" required="true">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Mensaje</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="message" placeholder="Cómo podemos ayudarte?" rows="3" required="true"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Marque el captcha</label>
                            <div class="col-md-9">
                                <?= $this->Recaptcha->display()?>
                            </div>
                        </div>                        
                    </section>                    
                    <div class="form-footer text-xs-right">
                        <p class="text-center"><input class="" type="submit" name="submitMessage" value="Enviar" id="contact_us_submit_button"></p>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
    <?= $this->element('ElmEmptySpace', ['altura' => 150]) ?>
</section>
