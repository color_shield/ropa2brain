<section>
    <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-justify">POLÍTICA DE PRIVACIDAD Y AVISO LEGAL</h2>
                <hr>
                <h3 class="text-justify">Política de privacidad</h3>
                <p class="text-justify font_18_px">En cumplimiento de lo previsto por el artículo 5 de la LOPD, se informa que los datos de carácter personal que se recaban directamente del usuario a través de los distintos recursos disponibles en el portal, serán tratados de forma confidencial y quedarán incorporados a un fichero del que es responsable 2brain.es. Mediante dicho uso, el cliente y/o usuario autoriza expresamente a 2brain.es el envío de comunicaciones comerciales por correo, así como al contacto telefónico.</p>
                <p class="text-justify font_18_px"><span>La información recibida será utilizada exclusivamente para propósitos inherentes a cada servicio y no será comunicada a terceros.</span></p>
                <p class="text-justify font_18_px"><span>Los clientes y/o usuarios de 2brain.es p</span><span>ueden, en todo momento, ejercitar los derechos de acceso, rectificación, cancelación u oposición sobre sus datos personales comunicándolo por escrito a 2brain.es, c/ El Amacal 7, 34419 Villalobón (Palencia) o bien enviando un email a:</span></p>
                <p class="text-center"><a title="Actualiza, cambia o modifica tus datos enviandonos un correo a esta dirección" href="mailto:info@2brain.es" target="_blank" class="button_2brain_style">info@2brain.es</a></p>
                <h3 class="text-justify">Aviso Legal</h3>
                <p class="text-justify font_18_px"><span>Este portal, cuyo titular es Idoia Vélez Miguel, con NIF 12771602R, domicilio en la calle El Amacal, nº7, 34419 Villalobón (Palencia), y teléfono 619 914 524, está constituido por los sitios web asociados a los dominios 2brain.es; bodas.2brain.es.</span></p>                
            </div>
        </div>
    </div>
    <?= $this->element('ElmEmptySpace', ['altura' => 150]) ?>
</section>