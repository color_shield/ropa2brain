<section>
    <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-justify">Pago seguro</h2>
                <hr>                
                <h3 class="text-justify">Nuestro pago seguro</h3>
                <p class="text-justify font_18_px">Con SSL</p>
                <h3 class="text-justify">Utilizando Visa/Mastercard/Paypal</h3>
                <p class="text-justify font_18_px">Sobre este servicio</p>
            </div>
        </div>
    </div>
    <?= $this->element('ElmEmptySpace', ['altura' => 200]) ?>
</section>
