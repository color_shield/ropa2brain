<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Vinilos Flúor</h2>
                <p class="text-center"><?= $this->Html->image('Separador.png', ['alt' => 'Separador de textos', 'id' => '', 'class' => 'img-responsive']); ?></p>
                <p><?= $this->Html->image('vinyls_type/Vinilos fluor.jpg', ['alt' => 'Nuestros vinilos de tipo fluor', 'title'=>'Nuestros vinilos de tipo flúor', 'id' => '', 'class' => 'img-responsive']); ?></p>
                <p class="text-justify font_18_px">El vinilo de corte FLÚOR, se caracteriza especialmente por disponer de una gama reducida de colores tipo flúor. Recomendamos este tipo de vinilo, por sus llamativos y vibrantes colores para; ropa para peñas, despedidas de soltero o soltera, ropa laboral, ropa para el AMPA, ropa para ayuntamientos...</p>
                <p class="text-justify font_18_px">Está compuesta de poliuretano ecológico fundido y todos sus colores tienen una excelente opacidad y superficie mate sin reflejos. Es adecuado para la transferencia térmica sobre textiles de algodón, mezclas poliéster/algodón y acrílicos.</p>
                <p class="text-justify font_18_px">Usamos este tipo de vinilo por su elasticidad, tacto agradable y suave. Lo aplicamos sobre las prendas por medio de calor con plancha térmica, de esta forma nos aseguramos la perduración del diseño en el tiempo. Recomendamos lavar la prenda del revés en frío. No planchar directamente; sino planchar del revés o con un paño por encima (no poner la plancha en contacto directo con el estampado).</p>                
            </div>
        </div>
    </div>
</section>