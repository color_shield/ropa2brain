<section>


    <?= $this->element('ElmEmptySpace', ['altura' => 0]) ?>

    <div class="row">                
        <div class="col-md-12"> 

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <div class="item active">
                        <?= $this->Html->image('index_slider/camisetas-anuncio.jpg', ['class' => 'index-main-img']) ?>
                        <div class="carousel-caption">
                            <div class="slider-content">
                                <div class="text1">Camiseta básica</div>
                                <div class="text2">con una estampación delantera</div>
                                <p class="text-center"><a title="Ir al catálogo de camisetas" href="<?= $this->Url->build(["controller" => "Filter", "action" => "catalogue", 9]); ?>" class="button_2brain_style">PROBAR AHORA</a></p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <?= $this->Html->image('index_slider/Vinilos y sus tipos.jpg', ['class' => 'index-main-img']) ?>
                        <div class="carousel-caption">
                            <div class="slider-content">
                                <div class="text1">Escoge el tipo de vinilo</div>
                                <div class="text2">que necesitas en tus prendas</div>
                                <p class="text-center"><a title="Ver los tipos de vinilos que utilizamos" href="<?= $this->Url->build(["controller" => "Store", "action" => "vinyls_type"]); ?>" class="button_2brain_style">VER VINILOS</a></p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <?= $this->Html->image('index_slider/Imagen de camisetas tecnicas.jpg', ['class' => 'index-main-img']) ?>
                        <div class="carousel-caption">
                            <div class="slider-content">
                                <div class="text1">Camisetas técnicas</div>
                                <div class="text2">tu mejor opción para el deporte</div>
                                <p class="text-center"><a title="Ir al catálogo de camisetas técnicas" href="<?= $this->Url->build(["controller" => "Filter", "action" => "catalogue", 12]); ?>" class="button_2brain_style">PROBAR AHORA</a></p>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Siguiente</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Anterior</span>
                </a>
            </div>




        </div>
    </div>


</section>



<section>
    <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
    <div class="container">
        <div class="row">           
            <div class="col-md-12">  
                <h2 class="text-center title_custom_2brain">Categorías</h2>
                <p class="text-center"><?= $this->Html->image('Separador_hojas.png', ['alt' => 'Separador de sección', 'title'=>'Separador de sección']); ?></p>
            </div>
            <div class="col-md-12">  
                <?php foreach ($list_categories as $category): ?>
                <a title="Ir a la categoría <?= $category->name ?>" href="<?= $this->Url->build(["controller" => "Filter", "action" => "catalogue", $category->id]); ?>" class="category-element">
                        <div class="col-md-3">
                            <?= $this->Html->image('categories/' . $category->image, []) ?>
                            <h3><?= $category->name ?></h3>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>

<section>

    <div class="container">
        <div class="row">           
            <div class="col-md-12">  
                <h2 class="text-center title_custom_2brain">Productos Nuevos</h2>
                <p class="text-center"><?= $this->Html->image('Separador_hojas.png', ['alt' => 'Separador de sección', 'title'=>'Separador de sección']); ?></p>
            </div>
            <div class="col-md-12">  
                <?php foreach ($list_new_products as $product): ?>
                    <div class="col-md-3 product_element_min_size">
                        <?= $this->Element('ElmProductElementShow', ['product' => $product]) ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

</section>

<?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>

<section>
    <div class="parallax" style="background-image: url('<?= $this->Url->image('parallax/parallax_bg_01.jpg'); ?>');">
        <div class="container">
            <div class="parallax-wrapper">
                <div class="parallax-text">
                    <div class="text1">Stamping</div>
                    <div class="text2">Diseña tu propia ropa entre cientos de combinaciones...</div>
                    <a href="<?= $this->Url->build(["controller" => "Filter", "action" => "catalogue"]); ?>" class="button">¡¡¡PROBAR AHORA!!!</a></div>
            </div>
        </div>
    </div>
</section>


<?= $this->element('ElmEmptySpace', ['altura' => 100]) ?>

<section class="policy-section">

    <div class="container"> 
        <div class="row"> 
            <div class="col-lg-12 col-sm-12 col-xs-12"> 
                <div class="col-lg-3 col-sm-3 col-xs-12 policy-element"> 
                    <div class="icons-box"> 
                        <div class="icon"><span class="fa fa-plane">&nbsp;</span></div> 
                        <div class="text-box"> 
                            <h3>Envío gratis</h3> 
                            <p>Todos los envíos para España son gratis si el pedido es superior a 130€</p> 
                        </div> 
                    </div> 
                </div> 
                <div class="col-lg-3 col-sm-3 col-xs-12 policy-element"> 
                    <div class="icons-box"> 
                        <div class="icon">
                            <span class="fa fa-money">&nbsp;</span>
                        </div> 
                        <div class="text-box"> 
                            <h3>Facilidad de Pago</h3> 
                            <p>Eliges cómo quieres pagar: Paypal, tarjeta o transferencia. Todo con la mayor seguridad.</p> 
                        </div> 
                    </div> 
                </div> 
                <div class="col-lg-3 col-sm-3 col-xs-12 policy-element"> 
                    <div class="icons-box"> 
                        <div class="icon">
                            <span class="fa fa-clock-o">&nbsp;</span>
                        </div> 
                        <div class="text-box"> 
                            <h3>Horario de atención al cliente</h3> 
                            <span class="text">De lunes a viernes.</span> 
                            <p>Mañanas de 9:00 a 14:00<br>Tardes de 17:00 a 20:00</p> 
                        </div> 
                    </div> 
                </div> 
                <div class="col-lg-3 col-sm-3 col-xs-12 policy-element"> 
                    <div class="icons-box"> 
                        <div class="icon">
                            <span class="fa fa-calendar">&nbsp;</span>
                        </div> <div class="text-box"> 
                            <h3>Abierto&nbsp;24/7</h3> 
                            <span class="text">La tienda ¡no cierra!</span> 
                            <p>Puedes comprar a cualquier hora.</p> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </div> 

</section>
<?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">  
                    <h2 class="text-center title_custom_2brain">Algunos de nuestros vinilos</h2>
                    <p class="text-center"><?= $this->Html->image('Separador_hojas.png', ['alt' => 'Separador de sección', 'title'=>'Separador de sección']); ?></p>
                </div>
                <div class="col-md-12">  
                    <a title="Ir a la sección vinilos básicos" href="<?= $this->Url->build(["controller" => "Store", "action" => "vinyl_basic"]); ?>" class="type-vinyle-element">
                        <div class="col-md-4">
                            <?= $this->Html->image('vinyls_type/Vinilos basicos.jpg', []) ?>
                            <h3>Vinilos Básicos</h3>
                        </div>
                    </a>
                    <a title="Ir a la sección vinilos metalizados" href="<?= $this->Url->build(["controller" => "Store", "action" => "vinyl_metalized"]); ?>" class="type-vinyle-element">
                        <div class="col-md-4">
                            <?= $this->Html->image('vinyls_type/Vinilos metalizados.jpg', []) ?>
                            <h3>Vinilos Metalizados</h3>
                        </div>
                    </a>
                    <a title="Ir a la sección vinilos flúor" href="<?= $this->Url->build(["controller" => "Store", "action" => "vinyl_fluor"]); ?>" class="type-vinyle-element">
                        <div class="col-md-4">
                            <?= $this->Html->image('vinyls_type/Vinilos fluor.jpg', []) ?>
                            <h3>Vinilos Flúor</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
