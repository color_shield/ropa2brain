<section>
    <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-justify">SOBRE NOSOTROS</h2>
                <hr>                
                <h3 class="text-justify">Nuestro equipo, 2brain.es</h3>
                <p class="text-justify font_18_px">2brain.es es un equipo de expertos del diseño que comenzamos nuestras carreras, tanto profesionales como creativas, por diversos campos publicitarios. Debido a la situación actual de crisis y motivación personal, hemos decidido unirnos y hacer lo que nos gusta. ¡CREAR!</p>
                <p class="text-justify font_18_px">¿Y para conseguirlo con éxito? Hemos decidido; ser marca de CONFIANZA, que nuestros clientes; SIEMPRE acaben satisfechos, que nuestros materiales; sean de LA MEJOR CALIDAD, que nuestros colaboradores; estén tan o más implicados que nosotros, en dar EL MEJOR SERVICIO.</p>
                <p class="text-justify font_18_px">Así que, PROMETEMOS:</p>
                <ul style="text-align:justify;list-style-type:circle;">
                    <li class="text-justify font_18_px"><em>Cuidar cada uno de nuestros trabajos:</em> primero OS ESCUCHAMOS, y después os aportamos todos nuestros conocimientos. Intentamos que cada trabajo que salga de nuestro estudio tenga su propio carácter y os aporte aquello que buscáis en el resultado final.</li>
                    <li class="text-justify font_18_px"><em>Tener el mejor ambiente laboral:</em> y que gracias al cual, llegamos a nuestros clientes, ofreciéndo CONFIANZA. No hay nada como trabajar en lo que a uno le gusta, y eso se desprende en nuestro trabajo.</li>
                    <li class="text-justify font_18_px"><em>Productos de calidad:</em> desde los que puedes personalizar, hasta los que están diseñados por nuestros colaboradores o dentro del propio estudio. TODOS están hechos con el mayor cariño y cortados, impresos y estampados a mano. Así que podemos decir que te llevas un producto único a tu casa.</li>
                    <li class="text-justify font_18_px"><em>Diseños personalizados:</em> todos nuestros productos estampados, o de la sección “personalizados”, son elaborados a mano, por lo que SIEMPRE, podemos darle un toque personal, añadiendo tu marca, tu nombre, o una frase divertida (se nos ocurre).</li>
                </ul></div>
        </div>
    </div>
    <?= $this->element('ElmEmptySpace', ['altura' => 150]) ?>
</section>