<section>
    <div class="container">
    <h2 class="text-center text-uppercase">preguntas frecuentes</h2>
    <hr>
    <p class="text-center">
        <?= $this->Html->image('faq/Cabecera preguntas y respuestas.jpg', ['alt' => 'Cabecera de la sección Preguntas y Respuestas', 'title'=>'Cabecera de la sección Preguntas y Respuestas']); ?>
    </p>
    <p class="text-center">
        <?= $this->Html->image('Separador.png', ['alt' => 'Separador de sección', 'title'=>'Separador de sección']); ?>
    </p>    
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="Cuidados">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#CuidadosEspeciales" aria-expanded="true" aria-controls="CuidadosEspeciales">
                            ¿Como cuido mi ropa?
                        </a>
                    </h4>
                </div>
                <div id="CuidadosEspeciales" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Cuidados">
                    <div class="panel-body">
                        <p class="faq_inherit_font_size">Cuidados especiales de las prendas:</p>
                        <ul>
                            <li class="faq_inherit_font_size">No lavar en seco</li>
                            <li class="faq_inherit_font_size">Lavar en frío del revés</li>
                            <li class="faq_inherit_font_size">No usar detergentes fuertes ni lejía</li>
                            <li class="faq_inherit_font_size">No usar secadora</li>
                            <li class="faq_inherit_font_size">No planchar directamente. Planchar por el revés o con un paño por encima (no poner la plancha en contacto directo con el estampado)</li>
                        </ul>
                        <p class="text-center">
                            <?= $this->Html->image('faq/Cuidados de la ropa.png', ['alt' => 'Cuidados de la ropa', 'title'=>'Cuidados de la ropa']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="Adecuar">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#AdecuarEstampacion" aria-expanded="false" aria-controls="AdecuarEstampacion">
                            ¿Qué significa, adecuar la estampación al tamaño de la ropa?
                        </a>
                    </h4>
                </div>
                <div id="AdecuarEstampacion" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Adecuar">
                    <div class="panel-body">
                        <p class="faq_inherit_font_size">Significa, que en STAMPING por 2brain.es, proporcionamos el dibujo al tamaño de tu ropa. Si tu camiseta es una XL, y el dibujo tiene un ancho de 30cm, a tu hijo con talla S, le hacemos un ancho de 20cm. De esta forma conseguiremos tener las mismas proporciones de tela y vinilo, visualmente, queda mucho mejor, y en la práctica, es más cómodo para ambos.</p>
                        <p class="text-center">
                            <?= $this->Html->image('faq/Adecuar estampacion al tamaño de la ropa.jpg', ['alt' => 'Adecuar estampacion al tamaño de la ropa', 'title'=>'Adecuar estampación al tamaño de la ropa']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="Duracion">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#DuracionEstampacion" aria-expanded="false" aria-controls="DuracionEstampacion">
                            ¿Cuánto dura la estampación?
                        </a>
                    </h4>
                </div>
                <div id="DuracionEstampacion" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Duracion">
                    <div class="panel-body">
                        <p class="faq_inherit_font_size">Siguiendo las instrucciones de lavado y planchado, el vinilo durará más de 4 años estampado en tu prenda.</p>
                        <p class="text-center">
                            <?= $this->Html->image('faq/Duracion de la estampacion.jpg', ['alt' => 'Duracion de la estampacion', 'title'=>'Duración de la estampación']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="Archivo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ArchivoEstampacion" aria-expanded="false" aria-controls="ArchivoEstampacion">
                            ¿Cómo necesitamos tu archivo para poder estampar?
                        </a>
                    </h4>
                </div>
                <div id="ArchivoEstampacion" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Archivo">
                    <div class="panel-body">
                        <ul>
                            <li class="faq_inherit_font_size">Lo perfecto sería que nos adjuntes durante el proceso de compra, un archivo en eps o pdf con el dibujo en vectorial, donde se diferencie bien el color de la prenda, con el color de la estampación. Una vez recibido el archivo, lo examinaremos y nos pondremos en contacto si existiera algún problema.</li>
                            <li class="faq_inherit_font_size">Si no lo tienes en vectorial, envíanos un jpg con el dibujo a 1 o 2 tintas, y lo examinaremos. Procederíamos a trazarlo y vectorizalo desde el estudio. Si hubiera algún problema o fuera muy complejo, nos pondríamos en contacto para pasar un presupuesto a medida por este trabajo.</li>
                            <li class="faq_inherit_font_size">En cualquier caso, el dibujo no debe tener líneas muy finas, ya que pueden perderse y deteriorarse con mucha más facilidad y rapidez.</li>
                        </ul>
                        <p class="text-center">
                            <?= $this->Html->image('faq/Tu archivo de imagen para estampar.jpg', ['alt' => 'Tu archivo de imagen para estampar', 'title'=>'Tu archivo de imagen para estampar']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="Recibir">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#RecibirRopa" aria-expanded="false" aria-controls="RecibirRopa">
                            ¿Cuándo recibirás tu ropa?
                        </a>
                    </h4>
                </div>
                <div id="RecibirRopa" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Recibir">
                    <div class="panel-body">
                        <p class="faq_inherit_font_size">Tardamos entre 10 y 15 días en estampar tu ropa y enviártela a casa. Este periodo puede variar en función de las prendas escogidas y la disponibilidad de las mismas.</p>
                        <p class="text-center">
                            <?= $this->Html->image('faq/Cuando recibire mi ropa.jpg', ['alt' => 'Cuando recibiré mi ropa', 'title'=>'Cuando recibiré mi ropa']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="Estampar">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#EstamparVinilo" aria-expanded="false" aria-controls="EstamparVinilo">
                            ¿Por qué estampar con vinilo tu ropa?
                        </a>
                    </h4>
                </div>
                <div id="EstamparVinilo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Estampar">
                    <div class="panel-body">
                        <ul>
                            <li class="faq_inherit_font_size">El vinilo es un material con gran duración, por lo que usarás tu ropa estampada mucho más tiempo.</li>
                            <li class="faq_inherit_font_size">No se daña ni destiñe con el tiempo (siguiendo las indicaciones de planchado y lavado).</li>
                            <li class="faq_inherit_font_size">Se adhiere y adapta a tu ropa perfectamente.</li>
                            <li class="faq_inherit_font_size">Los acabados son perfectos, y los colores mucho más vivos que con otras técnicas de estampación sobre ropa.</li>
                            <li class="faq_inherit_font_size">Además los colores no pierden su color, garantía mínima de 4 años.</li>
                            <li class="faq_inherit_font_size">El vinilo es un material con cierta elasticidad, muy cómodo al llevarlo puesto.</li>
                        </ul>
                        <p class="text-center">
                            <?= $this->Html->image('faq/Ropa estampada con vinilo.jpg', ['alt' => 'Ropa estampada con vinilo', 'title'=>'Ropa estampada con vinilo']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="Imagen">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ImagenColor" aria-expanded="false" aria-controls="ImagenColor">
                            ¿Ponemos imágenes a todo color?
                        </a>
                    </h4>
                </div>
                <div id="ImagenColor" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Imagen">
                    <div class="panel-body">
                        <p class="faq_inherit_font_size">No. Desde stamping, no ofrecemos este servicio por su baja calidad y durabilidad.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>