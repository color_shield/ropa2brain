<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Cómo se crea una plantilla y para que vale.</h2>
            <hr>
            <p>
                Las plantillas se agregan a los productos a la hora de su creación. Cada producto por lo tanto estará asociado a una plantilla. Dicha plantilla nos especificará
             ciertos detalles del producto como sus zonas de estampación y el tipo de vinilo que puede estamparse en esa prenda.
            </p>
            <p>
                Las plantillas pueden ser asignadas a muchos productos una vez se crean, por ejemplo, si creamos una plantilla para una camiseta en la que especificamos que puede 
                estamparse vinilo tanto en la manga como en la espalda, podremos usar esa plantilla para todas las camisetas que tengan la misma zona de estampación y el mismo tipo de vinilo 
                para estampar. Las pantillas se componen de: <strong>Opciones y Tipos de vinilo</strong>. Cada plantilla tendrá una serie de opciones: <strong>Manga, Pecho, Corazón, Espalda</strong> (que corresponderán con las 
                zonas que pueden estamparse en dicha prenda) y unos tipos de vinilos asociados (<strong>flúor, deformable, básico,</strong> etc) que corresponderán con los vinilos que dicha zona puede llevar.
            </p>
            <p>
                Aconsejamos crear las plantillas en base a las categorías, esto significa que si tenemos la familia Camisetas, crearemos por ejemplo: Camiseta estampado manga todos los vinilos, Camiseta 
                estampado espalda todos los vinilos, Camiseta estampado pecho sólo flúor, etc.
            </p>
            <p>
                Para crear una plantilla nueva se recomienda seguir estos pasos:
            <ol>
                <li>Lo primero de todo debe crear los colores que pueden llevar los vinilos con los que trabaje, una vez creados puede empezar a crar vinilos.</li>
                <li>Debe crear los vinilos disponibles para todas las prendas, luego durante el proceso asignará los que correspondan a cada prenda, pero debe tener todos introducidos en el sistema primero.</li>
                <li>Crear las opciones necesarias para dicha prenda junto con los tipos de vinilo que pueda estamparse en ella, por ejemplo: Crear la opción de manga y asignar los vinilos a dicha zona</li>
                <li>Una vez creadas las opciones necesarias para una prenda, ir a la creacion de plantillas y asignar las opciones correspondientes que hemos creado previamente.</li>
                <li>IMPORTANTE: No ir a la sección de crear plantilla sin haber creado antes todos los vinilos y las opciones para las prendas.</li>
            </ol>
            </p>
            
        </div>
        <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
    </div>
</div>
