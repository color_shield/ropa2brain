<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Listado de plantillas</h3>
            <span>
                <?= $this->Html->link('<i class="fa fa-plus-square"></i> Nueva plantilla',['action' => 'add_template'],['class'=>'btn btn-success','escape'=> false]) ?>
            </span>
            <hr>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="row"><?= $this->Paginator->sort('name',"Talla") ?></th>
                        <th scope="row">Opciones asignadas</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($templates as $template): ?>
                    <tr>                        
                        <td><?= $template->name ?></td>
                        <td>
                            <?php foreach($template->options as $option): ?>
                            <?= $this->Html->image('options/'.$option->icon, ['alt' => 'imagen de opcion','data-plugin-tooltip'=>'','data-toggle'=>'tooltip','data-placement'=>'left','data-original-title'=>$option->name]); ?>
                            <?php endforeach; ?>
                        </td>
                        <td>                        
                            <?= $this->Html->link(__('Editar'), ['action' => 'edit_template', $template->id], ['class' => 'btn btn-primary']) ?>
                            <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $template->id], ['class' => 'btn btn-danger', 'confirm' => __('¿Seguro que desea borrar esta plantilla?')]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('Primera')) ?>
                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                <?= $this->Paginator->last(__('Última') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} elemento(s) de un total de {{count}} registros.')]) ?></p>
        </div>
        </div>
        <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
    </div>
</div>


