<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
            <?= $this->Form->create($template, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Añadir plantilla') ?></legend>
                <div class="col-md-5">                        
                    <span><?= __('Nombre') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Opciones de plantilla') ?></span>
                    <hr>
                    <p>Elija las zonas de la prenda que tendrá asociadas esta plantilla:</p>
                    <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                    <div class="form-inline">
                        <div class="row">
                        <?php $cont = 0; ?>
                        <?php foreach ($options as $option): ?>
                                <div class="col-sm-4 col-md-2">
                                    <div class="thumbnail">
                                        <p class="text-center"><label for="option_id_<?= $option->id ?>"><img style="cursor: pointer;" src="<?= $this->Url->image('options/' . $option->icon); ?>" /><br/>  </label></p>
                                        <div class="caption">
                                            <p class="text-center"> Zona: <?= $option->name ?></p>
                                            <p class="text-center"><?= $this->Form->checkbox('options[][id]', ['value' => $option->id, 'id' => 'option_id_' . $option->id, 'hiddenField' => false]); ?></p> 
                                        </div>
                                    </div>
                                </div>
                            <?php $cont++; ?>
                        <?php endforeach; ?>
                        </div>
                    </div>                   
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
            </fieldset>
            <div class="col-md-5">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>                
                <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver', ['action' => 'templates_list'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>        
    </div>
</div>

