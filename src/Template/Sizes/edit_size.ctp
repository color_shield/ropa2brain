<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
            <?= $this->Form->create($size, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Editar Talla') ?></legend>            
                <div class="col-md-12">                        
                    <span><?= __('Nombre de la talla') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'label' => false]) ?>
                </div>                
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Imagen de la talla') ?></span>
                    <p class="text-left"><?= $this->Html->image('sizes/' . $size->icon, ['alt' => 'Imagen de la talla correspondiente']); ?></p>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Nueva imagen de talla') ?></span>
                    <?= $this->Form->input('new_image', ['type' => 'file', 'class' => 'form-control', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Fecha de creación') ?></span>
                    <input name="date_order" type="date" class="form-control" />
                </div>
            </fieldset>
            <div class="col-md-12">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
                <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver',['action' => 'sizes_list'],['class'=>'btn btn-primary','escape'=> false]) ?>                
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>