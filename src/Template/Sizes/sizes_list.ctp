<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Listado de tallas</h3>
            <span>
                <?= $this->Html->link('<i class="fa fa-plus-square"></i> Nueva talla',['action' => 'add_size'],['class'=>'btn btn-success','escape'=> false]) ?>
            </span>
            <hr>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="row"><?= $this->Paginator->sort('name',"Talla") ?></th>

                        <th scope="row"><?= __('Icono') ?></th>
                        
                        <th scope="row"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($sizes as $size): ?>
                    <tr>                        
                        <td><?= $size->name ?></td>
                        <td><?= $this->Html->image('sizes/'.$size->icon, ['alt' => 'imagen de la talla correspondiente']); ?></td>
                        
                        
                        <td>                        
                            <?= $this->Html->link(__('<i class="fa fa-pencil" aria-hidden="true"></i> Editar'), ['action' => 'edit_size', $size->id], ['class' => 'btn btn-warning','escape'=> false]) ?>
                            <?= $this->Form->postLink(__('<i class="fa fa-trash-o" aria-hidden="true"></i> Borrar'), ['action' => 'delete', $size->id], ['class' => 'btn btn-danger','escape'=> false, 'confirm' => __('¿Seguro que desea borrar esta talla?')]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('Primera')) ?>
                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                <?= $this->Paginator->last(__('Última') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} elemento(s) de un total de {{count}} registros.')]) ?></p>
        </div>
        </div>        
    </div>
</div>

