<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
            <?= $this->Form->create($new_category, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Añadir categoría') ?></legend>
                <p>Debe añadir categorías a los productos. Tambien puede crear subcategorías si añade una categoría existente a la categoría que vaya a crear.</p>
                <div class="col-md-12">                        
                    <p><span><?= __('Nombre de la categoría') ?></span></p>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">  
                    <p><span><?= __('Descripción de la categoría') ?></span></p>
                    <?= $this->Form->input('description', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <p><span><?= __('Imagen de categoría') ?></span></p>
                    <?= $this->Form->input('image', ['type' => 'file', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <?php if ($cat_control): ?>
                        <p><span><?= __('Categoría padre:') ?></span></p>
                        <select class="form-control" name="category_id">
                            <option value="0">--- Ninguno ---</option>
                            <?php foreach ($categories_list as $category): ?>
                                <option value="<?= $category->id ?>"><?= $category->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php else: ?>
                        <p><span><?= __('Categoría padre:') ?></span></p>
                        <p><?php echo "No existen categorias creadas aún, debe crear al menos una para poder asignar una categoria padre."; ?></p>
                    <?php endif; ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
        </div>                                
        </fieldset>
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver', ['action' => 'categories_list'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
            <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
        </div>
        <?= $this->Form->end ?>
    </div>
</div>
</div>