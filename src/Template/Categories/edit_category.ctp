<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
            <?= $this->Form->create($category, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Editar Categoría') ?></legend>            
                <div class="col-md-12">                        
                    <span><?= __('Nombre de la categoría') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Descripción de la categoría') ?></span>
                    <?= $this->Form->input('description', ['type' => 'text', 'class' => 'form-control', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <?php if ($cat_control): ?>
                        <p><span><?= __('Categoría padre:') ?></span></p>
                        <select class="form-control" name="category_id">
                            <option value="0" <?php if($category->category_id == 0){ echo 'selected'; } ?> >--- Ninguno ---</option>
                            <?php foreach ($categories_list as $elem_category): ?>
                                <option value="<?= $elem_category->id ?>" <?php if($category->category_id == $elem_category->id){ echo 'selected'; } ?> ><?= $elem_category->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php else: ?>
                        <p><span><?= __('Categoría padre:') ?></span></p>
                        <p><?php echo "No existen categorias creadas aún, debe crear al menos una para poder asignar una categoria padre."; ?></p>
                    <?php endif; ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Imagen de la categoría') ?></span>
                    <p class="text-left"><?= $this->Html->image('categories/' . $category->image, ['alt' => 'Imagen de la categoría correspondiente']); ?></p>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Nueva imagen de categoría') ?></span>
                    <?= $this->Form->input('new_image', ['type' => 'file', 'class' => 'form-control', 'label' => false]) ?>
                </div>
            </fieldset>
            <div class="col-md-12">                
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
                <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver',['action' => 'categories_list'],['class'=>'btn btn-primary','escape'=> false]) ?>            
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>