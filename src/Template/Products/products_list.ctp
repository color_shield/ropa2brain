
<section class="product-list-page">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Listado de Productos</h3>
                <span>
                    <?= $this->Html->link('<i class="fa fa-plus-square"></i> Nuevo producto',['action' => 'addProduct'],['class'=>'btn btn-success','escape'=> false]) ?>
                </span>
                <hr>
                <table class="table table-hover table-product-list">
                    <thead>
                        <tr>
                            <th scope="row"><?= $this->Paginator->sort('name',"Nombre") ?></th>
                            <th scope="row"><?= $this->Paginator->sort('category_id',"Categoría") ?></th>
                            <th scope="row"><?= __('Tallas') ?></th>
                            <th scope="row"><?= __('Colores') ?></th>
                            <th scope="row"><?= __('Precio') ?></th>
                            <th scope="row"><?= __('Acciones') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($products as $product): ?>
                        <tr>                        
                            <td><?= $product->name ?></td>
                            <td><?= $product->category->name ?></td> 

                            <td>
                                <?= $this->Element('ElmProductHelper', ['element_class' => 'deco-element-size', 'action' => 'get-sizes', 'product' => $product]) ?>
                            </td>

                            <td>
                                <?= $this->Element('ElmProductHelper', ['element_class' => 'deco-element-colour', 'action' => 'get-colours', 'product' => $product]) ?>
                            </td> 

                            <td>
                                <?= $this->Element('ElmProductHelper', ['element_class' => 'deco-element-price', 'action' => 'get-prices', 'product' => $product]) ?>
                            </td>

                            <td>                        
                                <?= $this->Html->link(__('<i class="fa fa-database" aria-hidden="true"></i> Stock'), ['controller' => 'Features' ,'action' => 'Stock', $product->id], ['class' => 'btn btn-primary','escape' => false]) ?>
                                <?= $this->Html->link(__('<i class="fa fa-pencil" aria-hidden="true"></i> Editar'), ['action' => 'editProduct', $product->id], ['class' => 'btn btn-warning','escape' => false]) ?>
                                <?= $this->Html->link(__('<i class="fa fa-files-o" aria-hidden="true"></i> Duplicar'), ['action' => 'duplicateProduct', $product->id], ['class' => 'btn btn-info','escape' => false]) ?>
                                <?= $this->Form->postLink(__('<i class="fa fa-trash-o" aria-hidden="true"></i> Borrar'), ['action' => 'delete', $product->id], ['class' => 'btn btn-danger','escape' => false , 'confirm' => __('¿Seguro que desea borrar este producto?')]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('Primera')) ?>
                    <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                    <?= $this->Paginator->last(__('Última') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} elemento(s) de un total de {{count}} registros.')]) ?></p>
            </div>
            </div>        
        </div>
    </div>
    
</section>