<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Busines $busines
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="product-to-edit-title-cstm">
                <ol class="breadcrumb">
                    <li><?= $this->Html->link('Productos',['controller'=> 'Products', 'action' => 'productsList']); ?>  </li>
                    <li><a href="<?= $this->Url->build( $this->request->getRequestTarget(), true); ?>"><?= $product->name ?> - Editar Producto</a> </li>
                  </ol>
            </div>
        </div>
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            <?= $this->Form->create($product, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Editar Producto') ?></legend>
                <div class="col-md-12">                        
                    <span><?= __('Nombre') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Descripción') ?></span>
                    <?= $this->Form->input('description', ['type' => 'textarea', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?>
                </div>                
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                
                <div class="col-md-5">
                    <span><?= __('Imagen actual') ?></span>
                    <p class="text-left"><?= $this->Html->image('products/' . $product->image, ['alt' => 'Imagen producto', 'class'=>'img-responsive']); ?></p>
                </div>
                
                <div class="col-md-5">
                    <span><?= __('Nueva imagen') ?></span>
                    <?= $this->Form->input('new_image', ['type' => 'file', 'class' => 'form-control', 'label' => false]) ?>
                </div>                
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                
                <div class="col-md-5">
                    <span><?= __('Imagen actual de cuidado de la ropa') ?></span>
                    <p class="text-left"><?= $this->Html->image('products/' . $product->care, ['alt' => 'Imagen cuidado del producto', 'class'=>'img-responsive']); ?></p>
                </div>
                
                <div class="col-md-5">
                    <span><?= __('Nueva imagen de cuidado de la ropa') ?></span>
                    <?= $this->Form->input('new_care_image', ['type' => 'file', 'class' => 'form-control', 'label' => false]) ?>
                </div>                
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                
                <div class="col-md-12">
                    <span><?= __('Categoría') ?></span>
                    <select name="category_id" class="form-control" >
                        <?php foreach($categories as $category): ?>
                            <option value="<?= $category->id ?>"  <?php if($category->id == $product->category_id){ echo 'selected'; } ?> ><?= $category->name ?></option>
                            
                            <?php foreach($category->sub_categories as $sub_category): ?>
                                <option value="<?= $sub_category->id ?>" <?php if($sub_category->id == $product->category_id){ echo 'selected'; } ?> >-- <?= $sub_category->name ?></option>

                            <?php endforeach; ?>   
                        <?php endforeach; ?>                        
                    </select>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Plantilla') ?></span>
                    <?= $this->Form->control('template_id', ['options' => $templates, 'class' => 'form-control', 'label' => '']); ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                
                 <div class="col-md-12">
                    <span><?= __('Atributos') ?></span>
                    <?= $this->element('ElmEmptySpace', ['altura' => 15]) ?>
                </div>
                <div class="form-inline">
                    <div class="row">   
                        
                        <div class="col-md-12">                          
                            <?php $attr_count = 0; ?>
                            <?php foreach ($attributes as $attribute): ?>
                                
                                <?php
                                    $checked = false;
                                    foreach ($product->attributes as $p_attr){
                                        if($p_attr->id == $attribute->id){
                                            $checked = true;
                                        }
                                    }

                                ?>

                                <div class="col-sm-4 col-md-2">
                                    <label for="attribute_id_<?= $attribute->id ?>" style="width: 100%; cursor: pointer;">
                                        <div class="thumbnail">

                                            <p class="text-center"><img src="<?= $this->Url->image('attributes/' . $attribute->icon); ?>" class="img-responsive" /><br/>  </p>
                                            <div class="caption">
                                                <p class="text-center"><?= $attribute->name ?></p>
                                                <p class="text-center"><?= $this->Form->checkbox('attributes[][id]', ['value' => $attribute->id, 'checked' => $checked ,'id' => 'attribute_id_' . $attribute->id, 'hiddenField' => false]); ?></p>
                                            </div>

                                        </div>
                                    </label>
                                </div> 
                            
                            <?php endforeach; ?>
                    </div> 
                </div> 
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
        </div>                                
        </fieldset>
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            <?= $this->Form->submit(__('Guardar'), ['class' => 'btn btn-primary pull-right']); ?>
            <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>        
</div>
</div>