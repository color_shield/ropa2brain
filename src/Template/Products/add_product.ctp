<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Busines $busines
 */
?>
<div class="container">
    <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
    <div class="row">
        <div class="col-md-12">
            <div class="product-to-edit-title-cstm">
                <ol class="breadcrumb">
                    <li><?= $this->Html->link('Productos',['controller'=> 'Products', 'action' => 'productsList']); ?>  </li>
                    <li><a href="<?= $this->Url->build( $this->request->getRequestTarget(), true); ?>"><?= $product->name ?> - Editar Producto</a> </li>
                  </ol>
            </div>
        </div>
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            <?= $this->Form->create($product, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Añadir Producto') ?></legend>
                <div class="col-md-12">                        
                    <span><?= __('Nombre') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Descripción') ?></span>
                    <?= $this->Form->input('description', ['type' => 'textarea', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Imagen del producto') ?></span>
                    <?= $this->Form->input('image', ['type' => 'file', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?> 
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Imagen de cuidado de la ropa para este producto') ?></span>
                    <?= $this->Form->input('care', ['type' => 'file', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?> 
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Categoría') ?></span>
                    <select name="category_id" class="form-control">
                        <?php foreach($categories as $category): ?>
                            <option value="<?= $category->id ?>"><?= $category->name ?></option>
                            
                            <?php foreach($category->sub_categories as $sub_category): ?>
                                <option value="<?= $sub_category->id ?>">-- <?= $sub_category->name ?></option>

                            <?php endforeach; ?>   
                        <?php endforeach; ?>                        
                    </select>
                    
                    
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Plantilla') ?></span>
                    <?= $this->Form->control('template_id', ['options' => $templates, 'class' => 'form-control', 'label' => '']); ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                
                <div class="col-md-12">
                    <span><?= __('Atributos') ?></span>
                    <?= $this->element('ElmEmptySpace', ['altura' => 15]) ?>
                </div>
                <div class="form-inline">
                    <div class="row">   
                        
                        <div class="col-md-12">
                            <?php $attr_count = 0; ?>
                            <?php foreach ($attributes as $attribute): ?>
                                <div class="col-sm-4 col-md-2">
                                    <label for="attribute_id_<?= $attribute->id ?>" style="width: 100%; cursor: pointer;">
                                        <div class="thumbnail">

                                            <p class="text-center"><img src="<?= $this->Url->image('attributes/' . $attribute->icon); ?>" class="img-responsive" /><br/>  </p>
                                            <div class="caption">
                                                <p class="text-center"><?= $attribute->name ?></p>
                                                <p class="text-center"><?= $this->Form->checkbox('attributes[][id]', ['value' => $attribute->id, 'id' => 'attribute_id_' . $attribute->id, 'hiddenField' => false]); ?></p>
                                            </div>

                                        </div>
                                    </label>
                                </div> 
                            <?php endforeach; ?>
                        </div>
                        <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                    </div>
                </div>
        </div>                                
        </fieldset>
        <div class="col-md-5">
            <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            <?= $this->Form->submit(__('Guardar'), ['class' => 'btn btn-primary pull-right']); ?>
            <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>        
</div>
</div>