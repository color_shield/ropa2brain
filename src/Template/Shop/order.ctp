

<section class="order-show-client">
    <div class="container">
        
        <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
        <div class="row">
            <div class="col-md-12">
                <h3 class="order-show-client-title">Carrito</h3>
            </div>
        </div>
        
        <?php if($order->suborders == null): ?>
            
        <div class="row">
            <div class="col-md-12">
                <p>Aún no has añadido ningún producto al carrito.</p>
                <p>
                    <?= $this->Html->link('<i class="fa fa-home" aria-hidden="true"></i> Inicio',['controller'=>'Store', 'action'=>'index'], ['class'=>'btn btn-gold', 'title' =>'Inicio ropa.2brain.es', 'escape' => false]) ?>
                    <?= $this->Html->link('<i class="fa fa-shopping-bag" aria-hidden="true"></i> Catálogo de productos',['controller'=>'Filter', 'action'=>'catalogue'], ['class'=>'btn btn-gold', 'title' =>'Catálogo de productos', 'escape' => false]) ?>
                </p>
                <?= $this->element('ElmEmptySpace', ['altura' => 300]) ?>    
            </div>
        </div>
        
        <?php else: ?>
        
        
            <?php foreach($order->suborders as $suborder): ?>

                <?php
                    $product_elem = $suborder->features[0]->product;
                ?>

                <div class="row">



                    <div class="col-md-12">
                        <div class="panel panel-default color-bg-1 shide-show panel-order-show-client-product">
                            <div class="panel-heading">
                                <h3 class="panel-title">Producto: <?= $product_elem->name ?>  <i class="fa fa-caret-down" aria-hidden="true"></i></h3>
                            </div>
                            <div class="panel-body">
                                <?= $this->Form->postLink(__('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar'), ['action' => 'deleteSuborder', $suborder->id], ['class' => 'btn btn-danger pull-right','escape'=> false, 'confirm' => __('¿Seguro que desea eliminar el producto del carrito?')]) ?>

                                <h3>Producto</h3>
                                <table class="table">
                                    <thead>
                                        <tr><th>ID</th><th>Nombre</th><th>Categoría</th><th>Imagen</th></tr>                                    
                                    </thead>
                                    <tbody>
                                        <tr><td>#<?= $product_elem->id ?></td><td><?= $product_elem->name ?></td><td><?php if($product_elem->category->parent_category != null) { echo $product_elem->category->parent_category->name.' / '; } ?> <?= $product_elem->category->name ?></td><td><?= $this->Html->image('products/'.$product_elem->image,[]) ?></td></tr>                                    
                                    </tbody>            
                                </table>

                                <h3>Tallas, cantidades y colores</h3>
                                <table class="table">
                                    <thead>
                                        <tr><th>Talla</th><th>Color</th><!--<th>Imagen</th>--><th>Precio unidad</th><th>Cantidad</th><th>Total fila</th></tr>                                    
                                    </thead>
                                    <tbody>
                                        <?php $total_price_features = 0; ?>
                                        <?php $total_quantity_features = 0; ?>
                                        <?php foreach($suborder->features as $feature): ?>
                                            <tr>
                                                <td><?= $this->Html->image('sizes/'.$feature->size->icon,[]) ?> <?= $feature->size->name ?></td>
                                                <td><?= $this->Html->image('colours/'.$feature->property->colour->icon,[]) ?> <?= $feature->property->colour->name ?></td>
                                                <!--<td><?= $this->Html->image('properties/'.$feature->property->image,[]) ?></td>-->
                                                <td><?= number_format($feature->_joinData->unit_price, 2, ',', '.') ?>€</td>
                                                <td><?= $feature->_joinData->quantity ?></td>
                                                <td><?= number_format( ($feature->_joinData->unit_price * $feature->_joinData->quantity) , 2, ',', '.') ?>€</td>                                           
                                            </tr>                                    

                                            <?php
                                                $total_quantity_features += $feature->_joinData->quantity;
                                                $total_price_features += ($feature->_joinData->unit_price * $feature->_joinData->quantity);    
                                            ?>                                        
                                        <?php endforeach; ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>TOTAL PRODUCTOS</td>
                                            <td><b><?= $total_quantity_features ?></b></td>
                                            <td><b><?= number_format( ($total_price_features) , 2, ',', '.') ?>€</b></td>
                                        </tr> 
                                    </tbody>            
                                </table>

                                <h3>Diseño</h3>
                                <table class="table">
                                    <thead>
                                        <tr><th>Opción vinilo</th><th>Color vinilo</th><th>Tipo vinilo</th><th>Imagen cliente</th><th>Precio unidad</th><th>Cantidad</th><th>Total fila</th></tr>                                    
                                    </thead>
                                    <tbody>
                                        <?php $total_price_options = 0; ?>
                                        <?php foreach($suborder->options_suborders as $option_suborder): ?>
                                            <tr>
                                                <td><?= $this->Html->image('options/'.$option_suborder->option->icon,[]) ?> <?= $option_suborder->option->name ?> </td>
                                                <td><?= $this->Html->image('dyes/'.$option_suborder->dye->icon,[]) ?> <?= $option_suborder->dye->name ?> </td>
                                                <td><?= $this->Html->image('vinyls/'.$option_suborder->vinyl->icon,[]) ?> <?= $option_suborder->vinyl->name ?> </td>
                                                <td><?= $this->Html->image('client_file/'.$option_suborder->client_file,[]) ?></td>
                                                <td><?= number_format($option_suborder->stamp_price, 2, ',', '.') ?>€</td>
                                                <td><?= $total_quantity_features ?></td>
                                                <td><?= number_format( ($option_suborder->stamp_price * $total_quantity_features) , 2, ',', '.') ?>€</td>
                                            </tr>        

                                            <?php
                                                $total_price_options += ($option_suborder->stamp_price * $total_quantity_features);
                                            ?>

                                        <?php endforeach; ?>
                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>TOTAL DISEÑO</td>
                                            <td><b><?= $total_quantity_features ?></b></td>
                                            <td><b><?= number_format( ($total_price_options) , 2, ',', '.') ?>€</b></td>
                                        </tr> 
                                    </tbody>            
                                </table>


                            </div>
                        </div>
                    </div>  

                </div>





            <?php endforeach; ?>
        
        
            <div class="row">
                <?= $this->Form->create($order,['url' => ['action' => 'complete', $order->id], 'enctype' => 'multipart/form-data', 'type' => 'post', 'id' => '', 'class' => 'product-shop-order-form']) ?>
                <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>    
                <div class="col-md-6">
                    <div class="panel panel-default color-bg-2">
                            <div class="panel-heading">
                                <h3 class="panel-title">Información de contacto</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <?= $this->Form->input('first_name', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Nombre']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('last_name', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Apellidos']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('email', ['type' => 'email', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Email']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('phone', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Teléfono']) ?>
                                </div>
                                <div class="col-md-12">
                                    <br />
                                </div>

                            </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default color-bg-3">
                        <div class="panel-heading">
                            <h3 class="panel-title">Precio</h3>
                        </div>
                        <div class="panel-body">

                            <table class="table">
                                <tbody>                                
                                    <tr>                                   
                                        <td>Productos</td>
                                        <td><?= number_format( ($total_price_features) , 2, ',', '.') ?>€</td>
                                    </tr> 
                                    <tr>                                   
                                        <td>Diseño</td>
                                        <td><?= number_format( ($total_price_options) , 2, ',', '.') ?>€</td>
                                    </tr> 
                                    <tr>                                   
                                        <td>Subtotal</td>
                                        <td><?= number_format( ($order->subtotal) , 2, ',', '.') ?>€</td>
                                    </tr> 
                                    <tr>                                   
                                        <td>Envío</td>
                                        <td><?= number_format( ($order->shipping) , 2, ',', '.') ?>€</td>
                                    </tr>                                                                
                                    <tr>                                   
                                        <td><b>Descuento <?= ($order->discount_percent) ?>% </b></td>
                                        <td><b>-<?= number_format( ($order->discount_value) , 2, ',', '.') ?>€</b></td>
                                    </tr> 
                                    <tr>                                   
                                        <td><b>TOTAL (IVA incl.)</b></td>
                                        <td><b><?= number_format( ($order->total) , 2, ',', '.') ?>€</b></td>
                                    </tr> 


                                </tbody>            
                            </table>
                        </div>
                    </div>
                </div>  

                <div class="col-md-6">
                    <div class="panel panel-default color-bg-2">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos de envío</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <?= $this->Form->input('shipping_address', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Dirección']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('shipping_address2', ['type' => 'text', 'class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Dirección. Línea 2.']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('shipping_location', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Población']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('shipping_province', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Provincia']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('shipping_zip', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Código postal']) ?>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default color-bg-2">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos de facturación</h3>
                            </div>
                            <div class="panel-body">
                                 <div class="col-md-12">
                                     <input type="checkbox" name="shipping_to_billing" class="shipping_to_billing" id="shipping_to_billing_id_checkbox" /> <label for="shipping_to_billing_id_checkbox">Los datos de facturación coinciden con los datos de envío.</label>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('billing_address', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Dirección']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('billing_address2', ['type' => 'text', 'class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Dirección. Línea 2.']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('billing_location', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Población']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('billing_province', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Provincia']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $this->Form->input('billing_zip', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false, 'placeholder' => 'Código postal']) ?>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-default color-bg-2">
                            <div class="panel-heading">
                                <h3 class="panel-title">Información adicional</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <?= $this->Form->input('comment', ['type' => 'textarea', 'class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Nota...']) ?>
                                </div>                            
                            </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-default color-bg-2">                        
                        <div class="panel-body">
                            <div class="col-md-6">                            
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label">Marque el captcha</label>
                                    <div class="col-md-9">
                                        <?= $this->Recaptcha->display()?>
                                    </div>
                                </div>   
                            </div> 
                            <div class="col-md-4">                            
                                <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500</p>
                            </div>   
                            <div class="col-md-2">
                                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                                <?= $this->Form->submit(__('Comprar'), ['class' => 'btn btn-gold pull-right']); ?>
                                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?= $this->Form->end() ?>
            </div>
        <?php endif; ?>
    </div>
</section>