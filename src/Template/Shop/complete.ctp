<section class="section-shop-complete">
    <div class="container">
        <div class="row">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>    
            <div class="col-md-6">
                <h1>Gracias por su compra</h1>
                <h3>Pedido: #<?= $order->id ?></h3>
                <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500.</p>
            </div>
            <div class="col-md-6">
                <?= $this->Html->image('Stamping_logo_horizontal.png', ['alt' => 'Stamping 2Brain logo', 'class'=>'img-responsive logo-complete']) ?>
            </div>  
            
        </div>
    </div>
</section>
               
