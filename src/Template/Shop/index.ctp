<?php
    $property_show = null;
    $price_show = "--,--";
    $isFirst = true;
    
    if($product->properties != null){
        foreach($product->properties as $tmp_prop){
            if($isFirst){
                $property_show = $tmp_prop;
                $price_show = $tmp_prop->_joinData->price;
                
                $isFirst = false;
            }
            else{
                if($tmp_prop->_joinData->price < $price_show){
                    $price_show = $tmp_prop->_joinData->price;
                }
            }
        
        }
    }
?>



<section>
    <div class="container">        
        <div class="row product-shop-wrap">
            
            <?php if($property_show != null): ?>
            
                <?= $this->Form->create(null,['url' => ['action' => 'order', $product->id], 'enctype' => 'multipart/form-data', 'type' => 'post', 'id' => '', 'class' => 'product-shop-full-form']) ?>
                    
                    <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>    
                
                    <div class="col-md-6">
                        
                        <div id="product-images-slider" class="" >
                            <?= $this->Html->image('products/'.$product->image,['alt'=> $product->name, 'title'=> $product->name, 'class' =>'img-responsive']) ?>
                            
                            <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?> 
                            
                            <div class="panel panel-default color-bg-1">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Descuento por cantidad</h3>
                                </div>
                                <div class="panel-body">
                                    
                                    <table class="table discount-product-table">
                                        <thead>
                                            <tr>
                                                <th>Nº de productos</th><th>Descuento</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>10 productos</td><td>15% dto.</td>
                                            </tr>
                                            <tr>
                                                <td>25 productos</td><td>24% dto.</td>
                                            </tr>
                                            <tr>
                                                <td>50 productos</td><td>27% dto.</td>
                                            </tr>
                                            <tr>
                                                <td>100 productos</td><td>30% dto.</td>
                                            </tr>                                       
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            
                            
                            
                            
                            <!-- Wrapper for slides -->
                            <!--<div class="carousel-inner" role="listbox">                                
                                <?php $count_images_slider = 0; ?>
                                <?php $arr_colour_ids = []; ?>
                                <?php foreach($product->properties as $element_property): ?>
                                    <?php if( !in_array($element_property->colour->id, $arr_colour_ids) ): ?>
                                        <?php $arr_colour_ids[] = $element_property->colour->id; ?>
                                        <div class="item <?php if($count_images_slider == 0) {echo 'active';} ?>">
                                            <?= $this->Html->image('properties/'.$element_property->image,['alt'=> $product->name, 'class' =>'']) ?>                                        
                                        </div>
                                        <?php $count_images_slider ++; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>                              
                            </div>-->

                            <!-- Controls -->
                          <!--   <a class="left carousel-control" href="#product-images-slider" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Siguiente</span>
                            </a>
                            <a class="right carousel-control" href="#product-images-slider" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Anterior</span>
                            </a>
                        </div>-->
                        
                        
                        
                        <!-- <div class="product-shop-images-thumbnails">         
                            
                                <?php $count_images_thumbnails = 0; ?>
                                <?php $arr_thumb_colour_ids = []; ?>
                                <?php foreach($product->properties as $element_property): ?>
                                    <?php if( !in_array($element_property->colour->id, $arr_thumb_colour_ids) ): ?>
                                        <?php $arr_thumb_colour_ids[] = $element_property->colour->id; ?>
                                        <div class="slider-thumbnail <?php if($count_images_thumbnails==0) {echo 'active';} ?>" cstm-frame-to="<?= $count_images_thumbnails ?>"   >
                                            <?= $this->Html->image('properties/'.$element_property->image,['alt'=> $product->name, 'class' =>'product-shop-image-small']) ?>
                                        </div>
                                        <?php $count_images_thumbnails ++; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>                                               
                        </div>-->
                        
                        
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="product-shop-title"><?= $product->name ?></div>
                        <div class="product-shop-attributes">
                            <?php
                                foreach($product->attributes as $attribute){
                                    echo $this->Html->image('attributes/'.$attribute->icon,['alt'=>$attribute->name, 'title' => $attribute->name, 'data-toggle' => 'tooltip', 'data-placement' => 'top']);
                                }        
                            ?>
                        </div>
                        <div class="product-shop-price">Desde <span><?= number_format($price_show, 2, ',', '.'); ?>€ </span>(IVA incl.)</div>
                        <div class="product-shop-desc"><?= $product->description ?></div>
                        
                        <div class="product-shop-image-instructions">
                            <?= $this->Html->image('products/'.$product->care,['alt'=> $product->name.' - cuidado', 'class' =>'product-shop-image-care']) ?>                           
                        </div>
                        <div class="product-shop-image-tax-text">
                            <h3>Calcula tu presupuesto</h3>
                                        
                        </div>
                        <div>
                            <p>Una vez hayas seguido todos los pasos para confeccionas tus prendas, pulsa el botón PRESUPUESTAR PEDIDO, y podrás ver el precio total de tu pedido. No realizaremos tu pedido hasta que nos des el OK del diseño digital, siendo el momento de realizar el pago.</p>
                            <p>La cantidad mínima total debe ser de 5 unidades.</p>
                            <p>Todos los precios incluyen el IVA del 21%.</p>           
                        </div>
                        
                        <div class="product-shop-features-colours">
                            
                            <div class="panel panel-default color-bg-1">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span class="circle-number">1</span>Elige el color</h3>
                                </div>
                                <div class="panel-body">
                                    
                                    <?php $arr_colour_ids_show = []; ?>
                                    <?php $count_frame_colour = 0; ?>
                                    <?php foreach($features as $feature): ?>
                                        <?php if(!in_array($feature->property->colour->id,$arr_colour_ids_show)): ?>
                                            <?php $arr_colour_ids_show[] = $feature->property->colour->id; ?>
                                            <div class="slider-thumbnail colour-activation-show-sizes-thumbnail" cstm-frame-to="<?= $count_frame_colour ?>">
                                                <?= $this->Html->image('colours/'.$feature->property->colour->icon,['class'=>'colour-activation-show-sizes product-shop-image-small','for-colour-id'=>$feature->property->colour->id,'alt'=>$feature->property->colour->name, 'title' => $feature->property->colour->name, 'data-toggle' => 'tooltip', 'data-placement' => 'top']); ?> 
                                                <span class="badge" id="badge-colour-<?= $feature->property->colour->id ?>">0</span>
                                            </div>
                                            <?php $count_frame_colour ++; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>

                                </div>
                            </div>

                        </div>
                        <div class="product-shop-features">
                            
                            <div class="panel panel-default shide-show panel-features color-bg-1">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span class="circle-number">2</span>Elige las cantidades <i class="fa fa-caret-down" aria-hidden="true"></i></h3>
                                </div>
                                <div class="panel-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Color</th><th>Talla</th><!--<th>Imagen</th>--><th>Cantidad</th><th>Precio/unidad</th><th>Sumatorio</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $cont = 0; ?>
                                            <?php $last_colour_id = 0; ?>
                                            <?php $count_images_thumbnails_td = -1; ?>
                                            <?php foreach($features as $feature): ?>
                                                <?php
                                                    if($last_colour_id != $feature->property->colour->id){
                                                        $last_colour_id = $feature->property->colour->id;
                                                        if($last_colour_id >= 0){
                                                            $count_images_thumbnails_td ++;                                                
                                                        }
                                                    }

                                                ?>

                                                <tr class="table-colour-tr table-colour-tr-<?= $feature->property->colour->id ?>">
                                                    <td>
                                                        <?= $this->Form->input('features['.$cont.'][id]', [ 'type' => 'hidden' , 'value' => $feature->id]); ?> 

                                                        <?= $this->Html->image('colours/'.$feature->property->colour->icon,['alt'=>$feature->property->colour->name, 'title' => $feature->property->colour->name, 'data-toggle' => 'tooltip', 'data-placement' => 'top']); ?> 

                                                    </td>
                                                    <td>
                                                        <?= $this->Html->image('sizes/'.$feature->size->icon,['alt'=>$feature->size->name, 'title' => $feature->size->name, 'data-toggle' => 'tooltip', 'data-placement' => 'top']); ?> 

                                                    </td>
                                                    <!--<td>
                                                        <div class="slider-thumbnail <?php if($count_images_thumbnails_td==0) {echo 'active';} ?>" cstm-frame-to="<?= $count_images_thumbnails_td ?>"   >
                                                            <?= $this->Html->image('properties/'.$feature->property->image,['alt'=> $product->name, 'class' =>'product-shop-image-small']) ?>
                                                        </div>
                                                    </td> -->
                                                    <td>                                        
                                                        <?= $this->Form->input('features['.$cont.'][quantity]', ['type' => 'number', 'value' => 0 , 'min'=>"0", 'class' => 'form-control shop-product-quantity-input', 'cstm-to-badge' => 'badge-colour-'.$feature->property->colour->id, 'cstm-to-price' => 'feature-total-price-'.$feature->id , 'cstm-unit-price' => $feature->price  , 'required' => 'false', 'label' => false]) ?>
                                                    </td>   
                                                    <td>                                        
                                                        <?= number_format($feature->price, 2, ',', '.'); ?>€
                                                    </td>  
                                                    <td>                                        
                                                        <span id="feature-total-price-<?= $feature->id ?>">0€</span>
                                                    </td> 
                                                </tr>

                                                <?php $cont ++; ?>
                                            <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        
                    </div>
                    
                    <div class="col-md-12">
                        <div class="product-shop-design">
                            
                            <div class="panel panel-default shide-show color-bg-1">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span class="circle-number">3</span>¿Dónde quieres tu estampación? <i class="fa fa-caret-down" aria-hidden="true"></i></h3>
                                </div>
                                <div class="panel-body">
                                    <p>En función del tipo de vinilo y lugar escogido de tu prenda, el precio puede variar. Lo podrás ver al final del proceso, pulsado el botón PRESUPUESTAR PEDIDO.</p>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th> </th><th>Vinilo</th><th>Tipo</th><th>Color </th><th>Tu imagen</th><th>Precio/Unidad</th><th>Precio fila</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $opt_cont = 0; ?>
                                            <?php foreach($product->template->options as $option): ?>
                                                <tr>
                                                    <td>
                                                        <?= $this->Form->input('options['.$opt_cont.'][id]', [ 'type' => 'hidden' , 'value' => $option->id]); ?> 
                                                        <?= $this->Html->image('options/'.$option->icon,['alt'=> $option->name, 'title' => $option->name, 'data-toggle' => 'tooltip', 'data-placement' => 'top']); ?> 
                                                    </td>
                                                    <td>
                                                        <?= $option->name ?>
                                                    </td>
                                                    <td>
                                                        <select name="options[<?= $opt_cont ?>][vinyl_id]" cstm-option-id="<?= $option->id ?>" class="form-control shop-product-design-select shop-product-design-select-option-<?= $option->id ?>">
                                                            <option value="0">No incluir</option>
                                                            <?php foreach($option->vinyls as $vinyle): ?>
                                                                <option value="<?= $vinyle->id ?>" cstm-price="<?= $vinyle->_joinData->price ?>"><?= $vinyle->name ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="options[<?= $opt_cont ?>][dye_id]" class="form-control order_color_chooser order_color_chooser-<?= $option->id ?>">
                                                            <?php foreach($dyes as $dye): ?>
                                                                <?php 
                                                                    $list_class_for_vinyl = '';
                                                                    foreach($option->vinyls as $vinyl_elem){
                                                                        foreach($vinyl_elem->dyes as $v_dye){
                                                                            if($v_dye->id == $dye->id){
                                                                                $list_class_for_vinyl .= ' for-vinyle-'.$v_dye->_joinData->vinyl_id;
                                                                            }
                                                                        }
                                                                    }
                                                                
                                                                ?>
                                                            
                                                                <option value="<?= $dye->id ?>" class="dye-option-for-option-select dye-option-for-option-select-<?= $option->id ?> <?= $list_class_for_vinyl ?>" data-class="avatar" data-style="background-image: url('/webroot/img/dyes/<?= $dye->icon ?>');"><?= $dye->name ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                    <td>                               

                                                        <div class="upload-btn-wrapper">
                                                            <button class="btn">Seleccionar archivo</button>
                                                            <?= $this->Form->file('options['.$opt_cont.'][image]', ['type' => 'file', 'class' => 'edit-properties-form-element-image shop-product-design-image' ]); ?>                                    
                                                        </div>
                                                        <div class="edit-properties-form-name-file-new-image"><span class="image-name">Ninguna imagen seleccionada</span> <span class="edit-properties-form-clear-new-image"><i class="fa fa-times"></i></span></div>

                                                    </td>
                                                    <td>
                                                        <span class="vinyl-option-price vinyl-option-price-no-price vinyl-option-price-option-id-<?= $option->id ?> vinyl-option-price-vinyl-id-0" >--.--€</span>
                                                        <?php foreach($option->vinyls as $vinyle): ?>
                                                            <span class="vinyl-option-price vinyl-option-price-option-id-<?= $option->id ?> vinyl-option-price-vinyl-id-<?= $vinyle->id ?>" ><span class="price-value"><?= number_format($vinyle->_joinData->price, 2, ',', '.');  ?></span>€</span>
                                                        <?php endforeach; ?>
                                                        
                                                    </td>
                                                    <td>
                                                        <span class="vinyl-option-price-total vinyl-option-price-total-no-price vinyl-option-price-total-<?= $option->id ?>" ><span class="price-value">--.--</span>€</span>
                                                    </td>
                                                </tr>  
                                                <?php $opt_cont ++; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                        </div>  
                    </div>
                    
                    <div class="col-md-12 pull-right">
                        <div class="panel panel-default color-bg-4">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span class="circle-number">4</span>Último paso</h3>
                                </div>
                                <div class="panel-body">
                                    <!-- <p> Una vez hayamos recibido tu pedido, confirmaremos que todo está OK, enviándote una prueba de diseño digital y la forma de pago a escoger. Recibirás tu pedido en 14 días.</p> -->
                                    <!-- <p>Tu pedido debe contener un mínimo de 5 unidades. Una vez hayamos recibido tu pedido, confirmaremos que todo está OK, enviándote una prueba de diseño digital y la forma de pago a escoger. Recibirás tu pedido en 14 días.</p>-->
                                    <p>Una vez hayas adjuntado tu diseño para la estampación, pulsa en el botón PRESUPUESTAR PEDIDO, para que puedas ver el importe total de tu compra. Aquí podrás efectuar tu compra. Una vez hayamos recibido tu pedido, confirmaremos que todo está OK, enviándote una prueba en diseño digital y la forma de pago a escoger. Recibirás tu pedido en el menor tiempo posible (plazo estimado 14 días).</p>
                                    <?= $this->Form->submit(__('Presupuestar pedido'), ['class' => 'btn btn-gold pull-right']); ?>
                                    <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                                </div>
                            </div>
                    </div>
                <?= $this->Form->end() ?>
                
            <?php else: ?>
                <div class="col-md-12">
                    <p>vacio</p>
                </div>
            <?php endif; ?>

        </div>
    </div>
    
    
    
</section>


<!--

    <a href="" class="product-element" cstm-product-id="<?= $product->id ?>">
        <?= $this->Html->image('properties/'.$property_show->image,['alt'=> $product->name, 'class' =>'product-element-property']) ?>
        <p class="product-element-name"><?= $product->name ?></p>
        <p class="product-element-price">Desde <span><?= number_format($price_show, 2, ',', '.'); ?>€</span></p>
        <p class="product-element-attributes">
            <?php
                foreach($product->attributes as $attribute){
                    echo $this->Html->image('attributes/'.$attribute->icon,['alt'=>$attribute->name, 'title' => $attribute->name, 'data-toggle' => 'tooltip', 'data-placement' => 'top']);
                }        
            ?>
        </p>
        <p class="product-element-colours">
             <?php
                $arr_id_colours = [];
                
                foreach($product->properties as $property){
                    if (!in_array($property->colour->id, $arr_id_colours)) {
                        $arr_id_colours[] = $property->colour->id;
                        echo $this->Html->image('colours/'.$property->colour->icon,['alt'=>$property->colour->name, 'title' => $property->colour->name, 'data-toggle' => 'tooltip', 'data-placement' => 'top']);
                    }

                }        
            ?>
        </p>
    </a>

-->