<section>    
    <div class="container">
        <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
        <div class="row">           
            <div class="col-md-12">
                <h2>Listado de productos</h2>
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            </div>
            <div class="col-md-12">
                <div class="col-md-4" style="padding-left: 0px;">
                <p class="text-justify">Haga clic en las categorías para desplegarlas y elegir sus parámetros de búsqueda.</p>
            </div>
            </div>
            <div class="col-md-4">
                <?= $this->Form->create(null, ['url' => ['action' => 'catalogue'], 'enctype' => 'multipart/form-data', 'type' => 'get', 'id' => '', 'class' => '']) ?>
                <div class="row">
                    <div class="col-md-10">
                        <div class="panel panel-info shide-show">
                            <div class="panel-heading panel_own_style text-uppercase pointer">Categorías</div>
                            <div class="panel-body" style="display: none;">
                                <?php $sub_categories = $categories->compile(); ?>
                                <?php foreach ($categories as $category): ?>
                                    <?php
                                        $checked = false;
                                        
                                        foreach($query_params as $query_param){
                                            foreach($query_param as $query_param_key => $query_param_value){                                                                                        
                                                $param_key = key($query_param_value);
                                                $param_value = $query_param_value[$param_key];
                                           
                                                if( $param_key == "category_id" && $param_value == $category->id){
                                                    $checked = true;
                                                }
                                            }
                                        }
                                    ?>
                                    
                                    <?php if($category->category_id == 0): ?>
                                        
                                    <p><?= $this->Form->checkbox('categories_form[][category_id]', ['value' => $category->id, 'checked'=> $checked , 'id' => 'category_id_' . $category->id, 'class' => 'pointer', 'hiddenField' => false]); ?> <label for="category_id_<?= $category->id ?>" class="filter_label_element pointer"><?= $category->name ?></label></p>
                                                                        
                                        <?php foreach ($sub_categories as $sub_category): ?>
                                            <?php if($sub_category->category_id == $category->id): ?>
                                                <?php
                                                    $checked = false;

                                                    foreach($query_params as $query_param){
                                                        foreach($query_param as $query_param_key => $query_param_value){                                                                                        
                                                            $param_key = key($query_param_value);
                                                            $param_value = $query_param_value[$param_key];

                                                            if( $param_key == "category_id" && $param_value == $sub_category->id){
                                                                $checked = true;
                                                            }
                                                        }
                                                    }
                                                ?>
                                                <p class="sub-category-filter"><?= $this->Form->checkbox('categories_form[][category_id]', ['value' => $sub_category->id, 'checked'=> $checked , 'id' => 'category_id_' . $sub_category->id, 'class' => 'pointer', 'hiddenField' => false]); ?> <label for="category_id_<?= $sub_category->id ?>" class="filter_label_element pointer"><?= $sub_category->name ?></label></p>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    
                                    <?php endif; ?>
                                    
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="panel panel-info shide-show">
                            <div class="panel-heading panel_own_style text-uppercase pointer">Colores</div>
                            <div class="panel-body" style="display: none;">
                                <?php foreach ($colours as $colour): ?>
                                        
                                        <?php
                                            $checked = false;
                                            

                                            foreach($query_params as $query_param){
                                                foreach($query_param as $query_param_key => $query_param_value){                                                                                        
                                                    $param_key = key($query_param_value);
                                                    $param_value = $query_param_value[$param_key];

                                                    if( $param_key == "colour_id" && $param_value == $colour->id){
                                                        $checked = true;
                                                    }
                                                }
                                            }
                                        ?>
                                        <div class="col-md-4">
                                            <label for="colourid<?= $colour->id ?>" class="filter_label_element pointer filter_label_element_colour">
                                                <?= $this->Form->checkbox('colours_form[][colour_id]', ['value' => $colour->id, 'checked' => $checked , 'id' => 'colourid' . $colour->id, 'class' => 'pointer', 'hiddenField' => false]); ?> 
                                                <?= $this->Html->image('colours/'.$colour->icon,['class'=>'index-main-img', 'title' => $colour->name, 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                            </label>
                                        </div>
                                                                
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="panel panel-info shide-show">
                            <div class="panel-heading panel_own_style text-uppercase pointer">Tallas</div>
                            <div class="panel-body" style="display: none;">
                                <?php foreach ($sizes as $size): ?>
                                    <?php
                                        $checked = false;

                                        foreach($query_params as $query_param){
                                            foreach($query_param as $query_param_key => $query_param_value){                                                                                        
                                                $param_key = key($query_param_value);
                                                $param_value = $query_param_value[$param_key];

                                                if( $param_key == "size_id" && $param_value == $size->id){
                                                    $checked = true;
                                                }
                                            }
                                        }
                                    ?>
                                    <p><?= $this->Form->checkbox('sizes_form[][size_id]', ['value' => $size->id, 'checked' => $checked , 'id' => 'sizeid' . $size->id, 'class' => 'pointer', 'hiddenField' => false]); ?> <label for="sizeid<?= $size->id ?>" class="filter_label_element pointer"><?= $size->name ?></label></p>                            
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="panel panel-info shide-show">
                            <div class="panel-heading panel_own_style text-uppercase pointer">Atributos</div>
                            <div class="panel-body" style="display: none;">
                                <?php foreach ($attributes as $attribute): ?>
                                    <?php
                                        $checked = false;

                                        foreach($query_params as $query_param){
                                            foreach($query_param as $query_param_key => $query_param_value){                                                                                        
                                                $param_key = key($query_param_value);
                                                $param_value = $query_param_value[$param_key];

                                                if( $param_key == "attribute_id" && $param_value == $attribute->id){
                                                    $checked = true;
                                                }
                                            }
                                        }
                                    ?>
                                    <p><?= $this->Form->checkbox('attributes_form[][attribute_id]', ['value' => $attribute->id, 'checked' => $checked ,'id' => 'attributeid' . $attribute->id, 'class' => 'pointer', 'hiddenField' => false]); ?> <label for="attributeid<?= $attribute->id ?>" class="filter_label_element pointer"><?= $attribute->name ?></label></p>                            
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <p class="text-center"><button type="submit" class="" id="catalogue_submit_button">Buscar</button></p>
                    </div>                    
                </div>
                <?= $this->Form->end() ?>
            </div>
            <div class="col-md-8">  
                <div class="row">                    
                    <?php if ($products->count() == 0): ?>
                        <div class="col-md-12">
                            <p class="text-center"><?= $this->Html->image('Noresultados.jpg', ['alt' => 'No se han encontrado resultados', 'title'=>'No se han encontrado resultados', 'class' => 'img-responsive']); ?></p>
                            <p class="text-justify catalogue_error_text">No se han encontrado resultados...Pruebe con otra selección.</p>
                        </div>
                    <?php else: ?>
                        <?php foreach ($products as $product): ?>
                            <div class="col-md-4 product_element_min_size_catalogue">
                                <?= $this->Element('ElmProductElementShow', ['product' => $product]) ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="row">
                    <div col-md-12>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->first('<< ' . __('Primera')) ?>
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                                <?= $this->Paginator->last(__('Última') . ' >>') ?>
                            </ul>
                            <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} elemento(s) de un total de {{count}} registros.')]) ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
