<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
            <?= $this->Form->create($option, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Editar opciones de plantilla') ?></legend>
                <p>Primero debe introducir un nombre para la opción, como por ejemplo: <em>"Estampado manga para camiseta"</em></p>
                <div class="col-md-5">                        
                    <span><?= __('Nombre de la opción:') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'required' => 'false', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 15]) ?>
                <div class="col-md-2">
                    <span><?= __('Icono Actual:') ?></span>
                    <p class=""><img src="<?= $this->Url->image('options/' . $option->icon); ?>" /></p>
                </div>
                <div class="col-md-5">
                    <span><?= __('Nuevo Icono:') ?></span>
                    <?= $this->Form->input('icon', ['type' => 'file', 'class' => 'form-control', 'required' => 'false', 'label' => false]) ?> 
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <h3><?= __('Vinilos asociados a la opción:') ?></h3>                    
                    <hr>
                    <p>Marque o desmarque los vinilos que pueda llevar la prenda. Si desea añadir otro vinilo debe marcar su casilla e introducir un precio para el mismo (si no introduce ninguno se le asignará un precio de cero Euros automáticamente). Si desea quitar un vinilo basta con que desmarque su casillay su precio será eliminado automáticamente. Si quiere que un vinilo no tenga coste basta con escribir un cero en su precio. Para los decimales puede usar tanto la coma como el punto para separar la parte entera de la decimal.</p>
                    <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                    <div class="form-inline">
                        <div class="row">
                            <?php $cont = 0; ?>
                            <?php foreach ($vinyls as $vinyl): ?>
                                    <?php
                                    $check = false;
                                    $price = 0;
                                    ?>
                                    <?php foreach ($option->vinyls as $vinyl_t): ?>
                                        <?php
                                        if ($vinyl->id == $vinyl_t->id) {
                                            $check = true;
                                            $price = $vinyl_t->_joinData->price;
                                        }
                                        ?>
                                    <?php endforeach; ?>
                                    <div class="col-sm-3">
                                        <div class="thumbnail">
                                            <p class="text-center"><label for="vinyl_id_<?= $vinyl->id ?>"><img src="<?= $this->Url->image('vinyls/' . $vinyl->icon); ?>" /> <br/> <span class="text-uppercase">Vinilo <?= $vinyl->name ?></span></label></p>
                                            <div class="caption">
                                                <p class="text-center"><?= $this->Form->checkbox('vinyls[' . $cont . '][id]', ['value' => $vinyl->id, 'checked' => $check, 'id' => 'vinyl_id_' . $vinyl->id, 'hiddenField' => false]); ?></p>
                                            </div>
                                            <p class="text-center"><span><?= __('Precio base vinilo ' . $vinyl->name) ?></span></p>
                                            <?= $this->Form->input('vinyls[' . $cont . '][_joinData][price]', ['type' => 'number', 'step' => 'any', 'value' => $price, 'class' => 'form-control', 'label' => false]) ?>
                                 
                                        </div>
                                    </div>
                                <?php $cont++; ?>
                                <?php endforeach; ?>
                        </div>
                    </div>                   
                </div>
<?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
            </fieldset>
            <div class="col-md-12">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>                
                <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver', ['action' => 'options_list'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
            <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
<?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>        
    </div>
</div>
