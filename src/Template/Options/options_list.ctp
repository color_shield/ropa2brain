<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Listado de opciones de plantillas</h3>
            <span>
                <?= $this->Html->link('<i class="fa fa-plus-square"></i> Nueva opción de plantilla', ['action' => 'add_option'], ['class' => 'btn btn-success', 'escape' => false]) ?>
            </span>
            <hr>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="row"><?= $this->Paginator->sort('name', "Talla") ?></th>
                        <th scope="row">Imagen de opción</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($options as $option): ?>
                        <tr>                        
                            <td><?= $option->name ?></td>
                            <td><img src="<?= $this->Url->image('options/' . $option->icon); ?>" /></td>
                            <td>                        
                                <?= $this->Html->link(__('Editar'), ['action' => 'edit_option', $option->id], ['class' => 'btn btn-primary']) ?>
                                <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $option->id], ['class' => 'btn btn-danger', 'confirm' => __('¿Seguro que desea borrar esta opción?')]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('Primera')) ?>
                    <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                    <?= $this->Paginator->last(__('Última') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} elemento(s) de un total de {{count}} registros.')]) ?></p>
            </div>
        </div>
        <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>        
    </div>
</div>


