<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
            <?= $this->Form->create($option, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Crear opciones de plantilla') ?></legend>
                <p>Primero debe introducir un nombre para la opción, como por ejemplo: <em>Estampado manga para camiseta</em></p>
                <div class="col-md-5">                        
                    <span><?= __('Nombre de la opción:') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?>
                </div>                
                <div class="col-md-5">
                    <span><?= __('Icono:') ?></span>
                    <?= $this->Form->input('icon', ['type' => 'file', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?> 
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Vinilos asociados a la opción:') ?></span>                    
                    <hr>
                    <p>Elija los tipos de vinilo que pueden estamparse en la zona de la prenda que ha asignado y los precios bases correspondientes:</p>
                    <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                    <div class="form-inline">
                        <?php $cont = 0; ?>
                        <?php foreach ($vinyls as $vinyl): ?>
                        <?php $price=0; ?>
                            <div class="form-group">
                                <?= $this->Form->checkbox('vinyls['.$cont.'][id]', ['value' => $vinyl->id, 'id' => 'vinyl_id_' . $vinyl->id, 'hiddenField' => false]); ?> 
                                <label for="vinyl_id_<?= $vinyl->id ?>"><img src="<?= $this->Url->image('vinyls/' . $vinyl->icon); ?>" /> <span class="text-uppercase">Vinilo <?= $vinyl->name ?></span></label>
                                <br>
                                <span><?= __('Precio base vinilo '.$vinyl->name) ?></span>
                                <?= $this->Form->input('vinyls['.$cont.'][_joinData][price]', ['type' => 'number', 'value'=>$price, 'step' => 'any', 'class' => 'form-control', 'label' => false]) ?>
                            </div>                                                      
                            <?php $cont++; ?>
                        <?php endforeach; ?> 
                    </div>                   
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
            </fieldset>
            <div class="col-md-5">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>                
                <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver', ['action' => 'options_list'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
<?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>        
    </div>
</div>

