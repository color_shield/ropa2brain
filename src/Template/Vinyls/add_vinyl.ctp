<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
            <?= $this->Form->create($vinyl, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Añadir tipo de vinilo') ?></legend>
                <div class="col-md-5">                        
                    <span><?= __('Nombre') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>                
                <div class="col-md-5">
                    <span><?= __('Icono') ?></span>
                    <?= $this->Form->input('icon', ['type' => 'file', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?> 
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Colores de vinilos disponibles') ?></span>
                    <hr>
                    <p>Elija los colores que el vinilo pueda utilizar en la estampación:</p>
                    <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                    <div class="form-inline">
                        <div class="row">
                        <?php $cont = 0; ?>
                        <?php foreach ($dyes as $dye): ?>
                                <div class="col-sm-4 col-md-2">
                                    <div class="thumbnail">
                                        <p class="text-center"><label for="dye_id_<?= $dye->id ?>"><img style="cursor: pointer;" src="<?= $this->Url->image('dyes/' . $dye->icon); ?>" /><br/>  </label></p>
                                        <div class="caption">
                                            <p class="text-center"> Color: <?= $dye->name ?></p>
                                            <p class="text-center"><?= $this->Form->checkbox('dyes[][id]', ['value' => $dye->id, 'id' => 'dye_id_' . $dye->id, 'hiddenField' => false]); ?></p> 
                                        </div>
                                    </div>
                                </div>
                            <?php $cont++; ?>
                        <?php endforeach; ?>
                        </div>
                    </div>                   
                </div>
            </fieldset>
            <div class="col-md-12">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
                <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver', ['action' => 'vinyls_list'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
        <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>        
    </div>
</div>

