<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Listado de Vinilos</h3>
            <span>
                <?= $this->Html->link('<i class="fa fa-plus-square"></i> Nuevo vinilo', ['action' => 'add_vinyl'], ['class' => 'btn btn-success', 'escape' => false]) ?>
            </span>
            <hr>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="row"><?= $this->Paginator->sort('name', "Nombre") ?></th>

                        <th scope="row"><?= __('Icono') ?></th>

                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($vinyls as $vinyl): ?>
                        <tr>                        
                            <td><?= $vinyl->name ?></td>
                            <td><?= $this->Html->image('vinyls/' . $vinyl->icon, ['alt' => 'imagen del vinilo correspondiente']); ?></td>


                            <td>                        
                                <?= $this->Html->link(__('Editar'), ['action' => 'edit_vinyl', $vinyl->id], ['class' => 'btn btn-primary']) ?>
                                <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $vinyl->id], ['class' => 'btn btn-danger', 'confirm' => __('¿Seguro que desea borrar este vinilo?')]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('Primera')) ?>
                    <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                    <?= $this->Paginator->last(__('Última') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} elemento(s) de un total de {{count}} registros.')]) ?></p>
            </div>
        </div>
        <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
    </div>
</div>
