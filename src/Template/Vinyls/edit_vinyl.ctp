<section class="section-vinyls-edit-vinyl">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
                <?= $this->Form->create($vinyl, ['enctype' => 'multipart/form-data']) ?>
                <fieldset>
                    <legend><?= __('Editar vinilo') ?></legend>            
                    <div class="col-md-5">                        
                        <span><?= __('Nombre del vinilo') ?></span>
                        <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'label' => false]) ?>
                    </div>                
                    <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                    <div class="col-md-12">
                        <span><?= __('Icono del vinilo') ?></span>
                        <p class="text-left"><?= $this->Html->image('vinyls/' . $vinyl->icon, ['alt' => 'Icono del vinilo correspondiente']); ?></p>
                    </div>
                    <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                    <div class="col-md-12">
                        <span><?= __('Nueva imagen del vinilo') ?></span>
                        <?= $this->Form->input('new_image', ['type' => 'file', 'class' => 'form-control', 'label' => false]) ?>
                    </div>
                    <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                    <div class="col-md-12">
                        <span><?= __('Colores asociados al vinilo:') ?></span>                    
                        <hr>
                        <p>Elija cualquiera de los colores disponibles para su vinilo, marque los que desee asociar y desmarque los que no quiere que estén disponibles para ese vinilo:</p>
                        <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                        <div class="form-inline">
                            <div class="row">   
                                <?php $cont = 0; ?>
                                <?php foreach ($dyes as $dye): ?>                                                         
                                    <?php
                                    $check = false;
                                    $price = 0;
                                    ?>
                                    <?php foreach ($vinyl->dyes as $dye_t): ?>
                                        <?php
                                        if ($dye->id == $dye_t->id) {
                                            $check = true;
                                        }
                                        ?>
                                    <?php endforeach; ?>
                                    <div class="col-sm-4 col-md-2">
                                        <div class="thumbnail">
                                            <p class="text-center"><label for="dye_id_<?= $dye->id ?>"><img style="cursor: pointer;" src="<?= $this->Url->image('dyes/' . $dye->icon); ?>" /><br/>  </label></p>
                                            <div class="caption">
                                                <p class="text-center"><p class="text-center"> Color: <?= $dye->name ?></p></p>
                                                <p class="text-center"><?= $this->Form->checkbox('dyes[][id]', ['value' => $dye->id, 'checked' => $check, 'id' => 'dye_id_' . $dye->id, 'hiddenField' => false]); ?></p>
                                            </div>
                                        </div>
                                    </div>                                                  
                                    <?php $cont++; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                </fieldset>
                <div class="col-md-12">
                    <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                    <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
                    <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver', ['action' => 'vinyls_list'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
        </div>
    </div>
</section>