<?php
$total_price_features = 0;
$total_price_options = 0;
$shipping_price = 6.5;
$limit_free_shipping = 150;
?>


<div class="container order-show">

    <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>

    <div class="row">          
        <div class="col-md-9">                    
            <h2 style="margin-top: 0;">Pedido #<?= $order->id ?> / <?= $order->first_name ?> <?= $order->last_name ?></h2>
        </div>
        <div class="col-md-3">                    
            <?= $this->Html->link(__('<i class="fa fa-pencil" aria-hidden="true"></i> Editar pedido'), ['action' => 'editOrder', $order->id], ['class' => 'btn btn-warning pull-right', 'escape' => false]) ?>
        </div>
    </div>

    <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>

    <?php foreach($order->suborders as $suborder): ?>
    
    
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default color-bg-1">
                <div class="panel-heading">
                    <h3 class="panel-title">Producto</h3>
                </div>
                <div class="panel-body">

                    <?php
                    $product_elem = $suborder->features[0]->product;
                    ?>

                    <table class="table">
                        <thead>
                            <tr><th>ID</th><th>Nombre</th><th>Categoría</th><th>Imagen</th></tr>                                    
                        </thead>
                        <tbody>
                            <tr>
                                <td>#<?= $product_elem->id ?></td>
                                <td><?= $product_elem->name ?></td>
                                <td>
                                    <?php if ($product_elem->category->parent_category != null) {
                                        echo $product_elem->category->parent_category->name . ' / ';
                                    } ?> <?= $product_elem->category->name ?>
                                </td>
                                <td><?= $this->Html->image('products/' . $product_elem->image, []) ?></td>
                            </tr>                                    
                        </tbody>            
                    </table>
                    
                    <table class="table">
                        <thead>
                            <tr><th>Talla</th><th>Color</th><!--<th>Imagen</th>--><th>Precio unidad</th><th>Cantidad</th><th>Total fila</th></tr>                                    
                        </thead>
                        <tbody>
                        <?php foreach ($suborder->features as $feature): ?>
                                <tr>
                                    <td><?= $this->Html->image('sizes/' . $feature->size->icon, []) ?> <?= $feature->size->name ?></td>
                                    <td><?= $this->Html->image('colours/' . $feature->property->colour->icon, []) ?> <?= $feature->property->colour->name ?></td>
                                    <!--<td><?= $this->Html->image('properties/' . $feature->property->image, []) ?></td>-->
                                    <td><?= number_format($feature->_joinData->unit_price, 2, ',', '.') ?>€</td>
                                    <td><?= $feature->_joinData->quantity ?></td>
                                    <td><?= number_format(($feature->_joinData->unit_price * $feature->_joinData->quantity), 2, ',', '.') ?>€</td>                                           
                                </tr>                                    

                                <?php
                                $total_price_features += ($feature->_joinData->unit_price * $feature->_joinData->quantity);
                                ?>

                        <?php endforeach; ?>
                        </tbody>            
                    </table>
                    
                    <?php
                    $tatal_num_products = 0;
                    foreach ($suborder->features as $feature) {
                        $tatal_num_products += $feature->_joinData->quantity;
                    }
                    ?>

                    <table class="table">
                        <thead>
                            <tr><th>Opción vinilo</th><th>Color vinilo</th><th>Tipo vinilo</th><th>Imagen cliente</th><th>Precio unidad</th><th>Cantidad</th><th>Total fila</th></tr>                                    
                        </thead>
                        <tbody>
                        <?php foreach ($suborder->options_suborders as $option_suborder): ?>
                                <tr>
                                    <td><?= $this->Html->image('options/' . $option_suborder->option->icon, []) ?> <?= $option_suborder->option->name ?></td>
                                    <td><?= $this->Html->image('dyes/' . $option_suborder->dye->icon, []) ?> <?= $option_suborder->dye->name ?></td>
                                    <td><?= $this->Html->image('vinyls/' . $option_suborder->vinyl->icon, []) ?> <?= $option_suborder->vinyl->name ?></td>
                                    <td><?= $this->Html->image('client_file/' . $option_suborder->client_file, []) ?></td>
                                    <td><?= number_format($option_suborder->stamp_price, 2, ',', '.') ?>€</td>
                                    <td><?= $tatal_num_products ?></td>
                                    <td><?= number_format(($option_suborder->stamp_price * $tatal_num_products), 2, ',', '.') ?>€</td>
                                </tr>        

                                <?php
                                $total_price_options += ($option_suborder->stamp_price * $tatal_num_products);
                                ?>

                            <?php endforeach; ?>
                        </tbody>            
                    </table>
                    
                </div>
            </div>
        </div>  

    </div>

    
    <?php endforeach; ?>

    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-default color-bg-4">
                <div class="panel-heading">
                    <h3 class="panel-title">Estado del pedido</h3>
                </div>
                <div class="panel-body">

                    <?= $this->Form->create(null, ['enctype' => 'multipart/form-data']) ?>
                    <input type="hidden" value="change_status" name="operation" />
                    <?php $bg_color = ''; ?>
                    <?php foreach ($statuses as $status): ?> 
                        <?php
                        if ($status->id == $order->status_id) {
                            $bg_color = $status->hex_code;
                        }
                        ?>
                        <?php endforeach; ?>

                    <select name="status_id" class="form-control status-order-html-select" style="">
                        <?php foreach ($statuses as $status): ?> 
                            <?php
                            $selected = '';
                            $bg_color = '';
                            if ($status->id == $order->status_id) {
                                $selected = 'selected="selected"';
                                $bg_color = $status->hex_code;
                            }
                            ?>
                            <option value="<?= $status->id ?>" cstm-hex-code="<?= $status->hex_code ?>" <?= $selected ?>><?= $status->name ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="status-order-html-select-color" style="background-color: <?= $bg_color ?>;"></div>
                    <?= $this->element('ElmEmptySpace', ['altura' => 10]) ?>
                    <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
                    <?= $this->Form->end() ?>
                 <?= $this->element('ElmEmptySpace', ['altura' => 40]) ?>
                </div>
            </div>


            <div class="panel panel-default color-bg-5">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos de contacto</h3>
                </div>
                <div class="panel-body">
                    <?= $this->element('ElmEmptySpace', ['altura' => 10]) ?>
                    <table class="table">
                        <tr><td>Email</td><td><?= $order->email ?></td></tr>
                        <tr><td>Teléfono</td><td><?= $order->phone ?></td></tr>                            
                    </table>
                    <?= $this->element('ElmEmptySpace', ['altura' => 56]) ?>
                </div>
            </div>

        </div>

        <div class="col-md-6">

            <div class="panel panel-default color-bg-6">
                <div class="panel-heading">
                    <h3 class="panel-title">Precio  final</h3>
                </div>
                <div class="panel-body">

                    <?php
                    if (($total_price_features + $total_price_options) > $limit_free_shipping) {
                        $shipping_price = 0;
                    }
                    ?>
                    <table class="table">
                        <tr><td>Total productos</td><td><?= number_format($total_price_features, 2, ',', '.') ?>€</td></tr>
                        <tr><td>Total diseño</td><td><?= number_format($total_price_options, 2, ',', '.') ?>€</td></tr>
                        <tr><td>Descuentos</td><td><?= $order->discount_percent ?>% aplicado - <?= $order->discount_value ?>€ descont.</td></tr>
                        <tr><td>Gastos de envío</td><td><?= number_format($order->shipping, 2, ',', '.') ?>€</td></tr>  
                        <tr><td>TOTAL</td><td><?= number_format(($order->total), 2, ',', '.') ?>€</td></tr>  
                    </table>
                </div>
            </div>
            
            <div class="panel panel-default color-bg-6">
                <div class="panel-heading">
                    <h3 class="panel-title">Nota del cliente</h3>
                </div>
                <div class="panel-body">
                    <p class="order-comment"><?= h($order->comment) ?></p>              
                </div>
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-md-6">

            <div class="panel panel-default color-bg-7">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos de facturación</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tr><td>Nombre</td><td><?= $order->first_name ?></td></tr>
                        <tr><td>Apellidos</td><td><?= $order->last_name ?></td></tr>
                        <tr><td>Dirección</td><td><?= $order->billing_address ?></td></tr>
                        <tr><td>Dirección 2</td><td><?= $order->billing_address2 ?></td></tr>
                        <tr><td>Población</td><td><?= $order->billing_location ?></td></tr>
                        <tr><td>Provincia</td><td><?= $order->billing_province ?></td></tr>
                        <tr><td>Código postal</td><td><?= $order->billing_zip ?></td></tr>
                    </table>
                </div>
            </div>

        </div>

        <div class="col-md-6">
            <div class="panel panel-default color-bg-8">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos de envío</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tr><td>Nombre</td><td><?= $order->first_name ?></td></tr>
                        <tr><td>Apellidos</td><td><?= $order->last_name ?></td></tr>
                        <tr><td>Dirección</td><td><?= $order->shipping_address ?></td></tr>
                        <tr><td>Dirección 2</td><td><?= $order->shipping_address2 ?></td></tr>
                        <tr><td>Población</td><td><?= $order->shipping_location ?></td></tr>
                        <tr><td>Provincia</td><td><?= $order->shipping_province ?></td></tr>
                        <tr><td>Código postal</td><td><?= $order->shipping_zip ?></td></tr>
                    </table>
                </div>
            </div>


        </div>
    </div>


</div>
