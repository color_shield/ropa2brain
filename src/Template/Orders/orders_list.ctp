<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Listado de Pedidos</h3>
          
            <hr>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="row"><?= $this->Paginator->sort('id',"ID") ?></th>

                        <th scope="row"><?= __('Cliente') ?></th>

                        <th scope="row"><?= __('Precio') ?></th>

                        <th scope="row"><?= __('Estado') ?></th>

                        <th scope="row"><?= __('Fecha') ?></th>
                        
                        <th scope="row"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($orders as $order): ?>
                    <tr>                        
                        <td>#<?= $order->id ?></td>
                        
                        <td><?= $order->first_name ?> <?= $order->last_name ?></td>

                        <td><?= number_format($order->total, 2, ',', '.'); ?>€</td>

                        <td><div style="width: 150px; height: 25px; color: #fff; text-align: center; padding: 3px 0;  background-color: <?= $order->status->hex_code ?>;"><?= $order->status->name ?></div></td>
                        
                        <td><?= $order->created->i18nFormat('dd/MM/yyyy HH:mm:ss'); ?></td>

                        <td>      
                            <?= $this->Html->link(__('<i class="fa fa-eye" aria-hidden="true"></i> Ver'), ['action' => 'ordersShow', $order->id], ['class' => 'btn btn-info', 'escape' => false]) ?>                  
                            <?= $this->Html->link(__('<i class="fa fa-pencil" aria-hidden="true"></i> Editar'), ['action' => 'editOrder', $order->id], ['class' => 'btn btn-warning', 'escape' => false]) ?>
                            <?= $this->Form->postLink(__('<i class="fa fa-trash-o" aria-hidden="true"></i> Borrar'), ['action' => 'delete', $order->id], ['class' => 'btn btn-danger', 'escape' => false, 'confirm' => __('¿Seguro que desea borrar este atributo?')]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('Primera')) ?>
                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                <?= $this->Paginator->last(__('Última') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} elemento(s) de un total de {{count}} registros.')]) ?></p>
        </div>
        </div>        
    </div>
</div>
