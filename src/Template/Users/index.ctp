<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            
                <?php if($global_user->role == 'worker'): ?>
                
                    <h3><?= __("Videos") ?></h3>
                    <?= $this->Html->link(__('Añadir'), ['controller' => 'Videos', 'action' => 'add'], ['class' => 'btn btn-success', 'escape' => false]) ?> 
                    <?= $this->Html->link(__('Lista'), ['controller' => 'Videos', 'action' => 'index'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                       
                <?php elseif($global_user->role == 'admin'): ?>
                
                    <h3><?= __("Videos") ?></h3>
                    <?= $this->Html->link(__('Añadir'), ['controller' => 'Videos', 'action' => 'add'], ['class' => 'btn btn-success', 'escape' => false]) ?> 
                    <?= $this->Html->link(__('Lista'), ['controller' => 'Videos', 'action' => 'index'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                                        
                    <h3><?= __("Usuarios") ?></h3>
                    <?= $this->Html->link(__('Añadir'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-success', 'escape' => false]) ?> 
                    <?= $this->Html->link(__('Lista'), ['controller' => 'Users', 'action' => 'listed'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                    
                <?php elseif($global_user->role == 'superadmin'): ?>
                
                    <h3><?= __("Usuarios") ?></h3>
                    <?= $this->Html->link(__('Añadir'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-success', 'escape' => false]) ?>                     
                    <?= $this->Html->link(__('Lista'), ['controller' => 'Users', 'action' => 'listed'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                    
                    <h3><?= __("Empresas") ?></h3>
                    <?= $this->Html->link(__('Añadir'), ['controller' => 'Business', 'action' => 'add'], ['class' => 'btn btn-success', 'escape' => false]) ?>                     
                    <?= $this->Html->link(__('Lista'), ['controller' => 'Business', 'action' => 'index'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                                              
                <?php endif; ?>
        </div>
    </div>
</div>
        
