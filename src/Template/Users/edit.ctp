<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura'=>120]); ?>
            <?= $this->Form->create($user) ?>
            <fieldset>
                <legend><?= __('Editar usuario') ?></legend>
                <?php
                    $options_role = [];
                    if($current_user->role == 'admin'){
                        $options_role = ['admin'=>'Administrador','worker'=>'Trabajador'];
                    }
                    elseif($current_user->role == 'superadmin'){
                        $options_role = ['admin'=>'Administrador','superadmin'=>'Super Administrador','worker'=>'Trabajador'];
                    }
                ?>
                <div class="input-group">
                    <div class="input-group-addon">Nombre</div>
                    <?= $this->Form->control('name',['class'=>'form-control', 'label' => '']); ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura'=>20]); ?>
                <div class="input-group">
                    <div class="input-group-addon">Email</div>
                    <?= $this->Form->control('email',['class'=>'form-control', 'label' => '']); ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura'=>20]); ?>
                <div class="input-group">
                    <div class="input-group-addon">Contraseña</div>
                    <?= $this->Form->control('password',['class'=>'form-control', 'value'=>'', 'required'=>false, 'label' => '']); ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura'=>20]); ?>
                <div class="input-group">
                    <div class="input-group-addon">Permisos</div>
                    <?= $this->Form->control('role',['options' => $options_role,'class'=>'form-control', 'label' => '']); ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura'=>20]); ?>
                <div class="input-group">
                    <div class="input-group-addon">Empresa</div>
                    <?= $this->Form->control('business_id',['options' => $business, 'class'=>'form-control', 'label' => '']); ?>
                </div>
                 
                    

                
            </fieldset>
            <?= $this->element('ElmEmptySpace', ['altura'=>20]); ?>
            <?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary pull-right',]) ?>
            <?= $this->Form->end() ?>
            
        </div>
    </div>
</div>
