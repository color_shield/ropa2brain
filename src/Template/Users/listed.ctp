<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <h3><?= __('Usuarios') ?> <?= $this->Html->link(__('Añadir'), ['action' => 'add'], ['class'=>'btn btn-success pull-right']) ?></h3>
        <hr />
        <table class="table table-striped" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name','Nombre') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('email','Email') ?></th>
                    
                    <th scope="col"><?= $this->Paginator->sort('business_id','Empresa') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('role','Permisos') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created','Fecha de creación') ?></th>
                    
                    
                    <th scope="col" class="actions"><?= __('Actions','Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= $this->Number->format($user->id) ?></td>
                    <td><?= h($user->name) ?></td>
                    <td><?= h($user->email) ?></td>
                    <td><?= h($user->busines->name) ?></td>
                    <td><?= h($user->role) ?></td>
                    <td><?= $user->created->i18nFormat('d/m/Y') ?></td>
                    
                    
                    
                    <td class="actions">
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class'=>'btn btn-warning']) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $user->id], ['confirm' => __('¿Eliminar el usuario #{0}?', $user->id), 'class'=>'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('Primero')) ?>
                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                <?= $this->Paginator->last(__('Último') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} usuario(s) de un total de {{count}}.')]) ?></p>
        </div>
    </div>
</div>
        
