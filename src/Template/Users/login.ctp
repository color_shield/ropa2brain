<!-- src/Template/Users/login.ctp -->

<section class="container">
    <?= $this->element('ElmEmptySpace', ['altura' => 200]) ?>
    <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
            <?= $this->Form->create(null,['class'=>'users-register-form']) ?>
                <div class="input-group">
                    <span class="input-group-addon"><?= __('Email') ?></span>
                    <?= $this->Form->input('email',array('type' => 'text','class' => 'form-control','placeholder' => __('Email'), 'label' => false)) ?>
                </div>
                <br/>
                <div class="input-group">
                    <span class="input-group-addon"><?= __('Contraseña') ?></span>
                    <?= $this->Form->input('password',array('type' => 'password','class' => 'form-control','placeholder' => __('Contraseña'), 'label' => false)) ?>
                </div>
                <br/>
                <!--<?= $this->Html->link(__('¿Olvidó su contraseña?'), ['controller'=>'users','action'=>'forgetPassword'] ,['class' => 'pull-left'] ); ?> -->
                <?= $this->Recaptcha->display()?>
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <?= $this->Form->submit(__('Login'), ['class' => 'btn btn-primary pull-left', '' , 'id' => 'users-login-send-form']); ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <?= $this->element('ElmEmptySpace', ['altura' => 200]) ?>
</section>       

