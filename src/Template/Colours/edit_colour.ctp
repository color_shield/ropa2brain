<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
            <?= $this->Form->create($colour, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Editar color') ?></legend>            
                <div class="col-md-12">                        
                    <span><?= __('Nombre del color') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'label' => false]) ?>
                </div>                
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Código Hexadecimal') ?></span>
                    <?= $this->Form->input('hexcode', ['type' => 'text', 'class' => 'form-control colorpickerField', 'required' => 'true', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Icono del color') ?></span>
                    <p class="text-left"><?= $this->Html->image('colours/' . $colour->icon, ['alt' => 'Icono del color correspondiente']); ?></p>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Nueva imagen del color') ?></span>
                    <?= $this->Form->input('new_image', ['type' => 'file', 'class' => 'form-control', 'label' => false]) ?>
                </div>
            </fieldset>
            <div class="col-md-12">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
                <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver',['action' => 'colours_list'],['class'=>'btn btn-primary','escape'=> false]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>