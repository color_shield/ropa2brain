<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Listado de Colores</h3>
            <span>
                <?= $this->Html->link('<i class="fa fa-plus-square"></i> Nuevo color',['action' => 'add_colour'],['class'=>'btn btn-success','escape'=> false]) ?>
            </span>
            <hr>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="row"><?= $this->Paginator->sort('name',"Nombre") ?></th>

                        <th scope="row"><?= __('Icono') ?></th>
                        
                        <th scope="row"><?= __('Código Hexadecimal') ?></th>
                        
                        <th scope="row"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($colours as $color): ?>
                    <tr>                        
                        <td><?= $color->name ?></td>
                        <td><?= $this->Html->image('colours/'.$color->icon, ['alt' => 'imagen de la talla correspondiente']); ?></td>
                        <td>#<?= $color->hexcode ?></td>
                        
                        <td>                        
                            <?= $this->Html->link(__('<i class="fa fa-pencil" aria-hidden="true"></i> Editar'), ['action' => 'edit_colour', $color->id], ['class' => 'btn btn-warning', 'escape' => false]) ?>
                            <?= $this->Form->postLink(__('<i class="fa fa-trash-o" aria-hidden="true"></i> Borrar'), ['action' => 'delete', $color->id], ['class' => 'btn btn-danger', 'escape' => false, 'confirm' => __('¿Seguro que desea borrar este color?')]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('Primera')) ?>
                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                <?= $this->Paginator->last(__('Última') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} elemento(s) de un total de {{count}} registros.')]) ?></p>
        </div>
        </div>        
    </div>
</div>