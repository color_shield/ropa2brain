<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
            <?= $this->Form->create($status, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Añadir estado') ?></legend>
                <div class="col-md-12">                        
                    <span><?= __('Nombre') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'required' => 'true', 'label' => false]) ?>
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">
                    <span><?= __('Color') ?></span>
                    <?= $this->Form->input('hex_code', ['type' => 'text', 'class' => 'form-control colorpickerField', 'required' => 'true', 'label' => false]) ?> 
                </div>
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
            </fieldset>
            <div class="col-md-12">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
                <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver', ['action' => 'statusesList'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>        
    </div>
</div>