<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->element('ElmEmptySpace', ['altura' => 50]) ?>
            <?= $this->Form->create($status, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Editar Estado') ?></legend>            
                <div class="col-md-12">                        
                    <span><?= __('Nombre') ?></span>
                    <?= $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'label' => false]) ?>
                </div>                
                <?= $this->element('ElmEmptySpace', ['altura' => 30]) ?>
                <div class="col-md-12">                        
                    <span><?= __('Color') ?></span>
                    <?php
                        $the_hex_code = $status->hex_code;
                        if(substr($the_hex_code,0,1) == '#'){
                            $the_hex_code = substr($the_hex_code,1);
                        }
                    ?>
                    <?= $this->Form->input('hex_code', ['type' => 'text', 'value' => $the_hex_code, 'class' => 'form-control colorpickerField', 'label' => false]) ?>
                </div>     
            </fieldset>
            <div class="col-md-12">
                <?= $this->element('ElmEmptySpace', ['altura' => 20]) ?>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
                <?= $this->Html->link('<i class="fa fa-arrow-left"></i> Volver', ['action' => 'statusesList'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>