<?php
//action
$act = $this->request->action;
//action
$contr = $this->request->controller;
?>
<nav class="navbar navbar-default">
    <div class="container-fluid" id="navbar_contact_data_section">
        <div class="navbar-header navbar-right">
            <p><span id="nav_phone"><i class="fa fa-phone"></i> <a href="tel:619914524">619 914 524</a></span> <span id="nav_mail"><i class="fa fa-envelope"></i> <a href="mailto:ropa@2brain.es?subject=Mensaje%20de%20contacto%20desde%20ropa.2brain.es">Contactar</a></span></p>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-header">
                <?= $this->Html->link($this->Html->image('logo_horizontal_xs.png', ['alt' => 'Stamping 2Brain logo', 'class'=>'img-responsive']),['controller'=>'store', 'action' => 'index'],['class'=>'','escape'=> false]) ?>
                
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">            
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><?= $this->Html->link('INICIO',['controller'=>'store', 'action' => 'index'],['class'=>'','escape'=> false]) ?></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> CAMISETAS<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><?= $this->Html->link('Para Todos',['controller'=>'filter', 'action' => 'catalogue',9],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Para Ellas',['controller'=>'filter', 'action' => 'catalogue',10],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Infantiles',['controller'=>'filter', 'action' => 'catalogue',11],['class'=>'','escape'=> false]) ?></li>
                        <li><a href="https://ropa.2brain.es/filter/catalogue?attributes_form%5B%5D%5Battribute_id%5D=13">Ecológicas</a></li>
                        <li><a href="https://ropa.2brain.es/filter/catalogue?attributes_form%5B%5D%5Battribute_id%5D=22">Técnicas</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> MÁS PRENDAS<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><?= $this->Html->link('Polos',['controller'=>'filter', 'action' => 'catalogue',1],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Delantales',['controller'=>'filter', 'action' => 'catalogue',1],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Gorras',['controller'=>'filter', 'action' => 'catalogue',1],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Sudaderas',['controller'=>'filter', 'action' => 'catalogue',8],['class'=>'','escape'=> false]) ?></li>                        
                        <li><?= $this->Html->link('Polares',['controller'=>'filter', 'action' => 'catalogue',1],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Mochilas / Totebags',['controller'=>'filter', 'action' => 'catalogue',1],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Pañuelos',['controller'=>'filter', 'action' => 'catalogue',1],['class'=>'','escape'=> false]) ?></li>                        
                        <li><?= $this->Html->link('Bodys',['controller'=>'filter', 'action' => 'catalogue',1],['class'=>'','escape'=> false]) ?></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> TIPOS DE VINILO<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><?= $this->Html->link('Todos los vinilos',['controller'=>'store', 'action' => 'vinyls_type'],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Vinilo Básico',['controller'=>'store', 'action' => 'vinyl_basic'],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Vinilo Deformable',['controller'=>'store', 'action' => 'vinyl_deformable'],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Vinilo Especial',['controller'=>'store', 'action' => 'vinyl_special'],['class'=>'','escape'=> false]) ?></li>                        
                        <li><?= $this->Html->link('Vinilo Flúor',['controller'=>'store', 'action' => 'vinyl_fluor'],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Vinilo Glitter',['controller'=>'store', 'action' => 'vinyl_glitter'],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Vinilo Metalizado',['controller'=>'store', 'action' => 'vinyl_metalized'],['class'=>'','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Vinilo Motivos',['controller'=>'store', 'action' => 'vinyl_motive'],['class'=>'','escape'=> false]) ?></li>                        
                        <li><?= $this->Html->link('Vinilo Texturas',['controller'=>'store', 'action' => 'vinyl_textured'],['class'=>'','escape'=> false]) ?></li>
                    </ul>
                </li>                                
                <li class=""><?= $this->Html->link('PREGUNTAS FRECUENTES',['controller'=>'store', 'action' => 'faq'],['class'=>'','escape'=> false]) ?></li>    
                <li class="" ><?= $this->Html->link('<i class="fa fa-shopping-cart" style="font-size: 25px;" aria-hidden="true"></i>',['controller'=>'Shop', 'action' => 'order'],['class'=>'', 'style' => 'padding: 12px 12px 12px 10px;','escape'=> false]) ?></li>  
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>