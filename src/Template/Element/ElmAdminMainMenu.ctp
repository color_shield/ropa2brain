<?php
//action
$act = $this->request->action;
//action
$contr = $this->request->controller;
?>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-header">
                <?= $this->Html->link($this->Html->image('logo_horizontal_xs.png', ['alt' => 'Stamping 2Brain logo', 'class'=>'img-responsive']),['controller'=>'products', 'action' => 'products_list'],['class'=>'','escape'=> false]) ?>
                
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">            
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><?= $this->Html->link('Tienda',['controller'=>'store', 'action' => 'index'],['class'=>'','escape'=> false]) ?></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Pedidos<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                       <li><?= $this->Html->link('Pedidos',['controller'=> 'Orders', 'action' => 'ordersList'],['class'=>'btn btn-primary','escape'=> false]) ?></li>
                       <li><?= $this->Html->link('Estados',['controller'=> 'Statuses', 'action' => 'statusesList'],['class'=>'btn btn-primary','escape'=> false]) ?></li>
                    </ul>
                    
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Productos<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><?= $this->Html->link('Nuevo Producto',['controller'=>'products', 'action' => 'add_product'],['class'=>'btn btn-primary','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Listado Productos',['controller'=>'products', 'action' => 'products_list'],['class'=>'btn btn-primary','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Atributos',['controller'=>'attributes', 'action' => 'attributes_list'],['class'=>'btn btn-primary','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Colores',['controller'=>'colours', 'action' => 'colours_list'],['class'=>'btn btn-primary','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Tallas',['controller'=>'sizes', 'action' => 'sizes_list'],['class'=>'btn btn-primary','escape'=> false]) ?></li>                        
                        <li><?= $this->Html->link('Listado de categorías',['controller'=>'Categories', 'action' => 'categories_list'],['class'=>'btn btn-primary','escape'=> false]) ?></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Plantillas<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><?= $this->Html->link('Como crear una plantilla',['controller'=>'templates', 'action' => 'como_crear_una_plantilla'],['class'=>'btn btn-primary','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Agregar colores de vinilos',['controller'=>'dyes', 'action' => 'dyes_list'],['class'=>'btn btn-primary','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Gestión de vinilos',['controller'=>'vinyls', 'action' => 'vinyls_list'],['class'=>'btn btn-primary','escape'=> false]) ?></li>
                        <li><?= $this->Html->link('Gestionar opciones de plantilla',['controller'=>'options', 'action' => 'options_list'],['class'=>'btn btn-primary','escape'=> false]) ?></li>                        
                        <li><?= $this->Html->link('Gestión de plantillas',['controller'=>'templates', 'action' => 'templates_list'],['class'=>'btn btn-primary','escape'=> false]) ?></li>                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><?= $this->Html->link('Cerrar sesión',['controller'=>'users', 'action' => 'logout'],['class'=>'btn btn-primary','escape'=> false]) ?></li>                        
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>