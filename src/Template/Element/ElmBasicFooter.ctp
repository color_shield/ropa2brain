<div id="footer">    
    <div class="container">
        <div class="row">
            <div class="col-md-12" id="footer_social_bar">
                <div>                        
                    <p class="text-center">
                        <a title="Facebook de 2Brain.es" href="https://www.facebook.com/2brain.es" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                        <a title="Twitter de 2Brain.es" href="http://www.twitter.com/2brain_es" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                        <a title="Youtube de 2Brain.es" href="https://www.youtube.com/channel/UCHq4LS6_rB4MJ2lqEOSGSfg" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                        <a title="Google+ de 2Brain.es" href="https://www.google.com/2brainpuntoes" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                        <a title="Pinterest de 2Brain.es" href="https://www.pinterest.com/2brainpuntoes/" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a>
                        <a title="Instagram de 2Brain.es" href="https://instagram.com/2brain_es/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </p>
                </div>
            </div>
            <div class="col-md-12" id="footer_sitemap">
                <div>
                    <ul id="footer_sitemap_ul" class="">
                        <li>                                
                            <a id="" class="" href="<?= $this->Url->build(["controller" => "Store", "action" => "send_conditions"]); ?>" title="Nuestros términos y condiciones de envío">
                                Envío
                            </a>
                        </li>
                        <li class="footer_ul_li_bordered">
                            <a id="" class="" href="<?= $this->Url->build(["controller" => "Store", "action" => "privacy_policy_and_legal_notice"]); ?>" title="Política de privacidad y Aviso legal">
                                Política de privacidad y Aviso legal
                            </a>
                        </li>
                        <li class="footer_ul_li_bordered">
                            <a id="" class="" href="<?= $this->Url->build(["controller" => "Store", "action" => "terms_and_conditions"]); ?>" title="Nuestros términos y condiciones">
                                Términos y condiciones
                            </a>
                        </li>
                        <li class="footer_ul_li_bordered">
                            <a id="" class="" href="<?= $this->Url->build(["controller" => "Store", "action" => "about_us"]); ?>" title="Averigüe más sobre nosotros">
                                Sobre nosotros
                            </a>
                        </li>
                        <li class="footer_ul_li_bordered">
                            <a id="" class="" href="<?= $this->Url->build(["controller" => "Store", "action" => "pay_safe"]); ?>" title="Nuestra forma de pago segura">
                                Pago seguro
                            </a>
                        </li>
                        <li class="footer_ul_li_bordered">
                            <a id="" class="" href="<?= $this->Url->build(["controller" => "Store", "action" => "contact_us"]); ?>" title="Contact us">
                                Contáctenos
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12" id="footer_powered_by">
                <p class="text-center">Ropa 2Brain -  2018 - Powered by <a title="Webcoden.com" href="http://webcoden.com" target="_blank">Webcoden</a></p>
            </div>
        </div>            
    </div>    
</div>
