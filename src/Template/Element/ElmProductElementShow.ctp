<?php
    $property_show = null;
    $price_show = "--,--";
    $isFirst = true;
    
    if($product->properties != null){
        foreach($product->properties as $tmp_prop){
            if($isFirst){
                $property_show = $tmp_prop;
                $price_show = $tmp_prop->_joinData->price;
                
                $isFirst = false;
            }
            else{
                if($tmp_prop->_joinData->price < $price_show){
                    $price_show = $tmp_prop->_joinData->price;
                }
            }
        
        }
    }
?>

<?php if($property_show != null): ?>

    
    <a href="<?= $this->Url->build(["controller" => "Shop", "action" => "index", $product->id]); ?>" class="product-element" cstm-product-id="<?= $product->id ?>">
        <?= $this->Html->image('products/'.$product->image,['alt'=> $product->name, 'class' =>'product-element-property']) ?>
        <p class="product-element-name"><?= $product->name ?></p>
        <p class="product-element-price">Desde <span><?= number_format($price_show, 2, ',', '.'); ?>€</span></p>
        <p class="product-element-attributes">
            <?php
                foreach($product->attributes as $attribute){
                    echo $this->Html->image('attributes/'.$attribute->icon,['alt'=>$attribute->name, 'title' => $attribute->name, 'data-toggle' => 'tooltip', 'data-placement' => 'top']);
                }        
            ?>
        </p>
        <p class="product-element-colours">
             <?php
                $arr_id_colours = [];
                $count_elems = 0;
                foreach($product->properties as $property){
                    if (!in_array($property->colour->id, $arr_id_colours)) {
                        $count_elems ++;                        
                        $arr_id_colours[] = $property->colour->id;
                        if($count_elems <= 12){
                            echo $this->Html->image('colours/'.$property->colour->icon,['alt'=>$property->colour->name, 'title' => $property->colour->name, 'data-toggle' => 'tooltip', 'data-placement' => 'top']);
                        }
                    }

                }        
            ?>
        </p>
    </a>

<?php else: ?>

<p>vacio</p>


<?php endif; ?>
