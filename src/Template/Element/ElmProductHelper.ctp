<?php

    

    $element_class_value = '';

    if(!empty($element_class)){
        $element_class_value = $element_class;
    }

    switch($action){
        case 'get-sizes':
            $list_sizes = [];
            foreach($product->sizes as $current_size){
                if( !in_array($current_size->id, $list_sizes)){
                    $list_sizes[] = $current_size->id;
                    echo $this->Html->image('sizes/'.$current_size->icon,['alt'=> $current_size->name, 'title' => $current_size->name, 'class' => 'feature-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'top']);
                }                
            }

        break;
        case 'get-colours':
            $list_colours = [];
            foreach($product->properties as $current_property){
                if( !in_array($current_property->colour->id, $list_colours)){
                    $list_colours[] = $current_property->colour->id;
                    echo $this->Html->image('colours/'.$current_property->colour->icon,['alt'=> $current_property->colour->name, 'class' => 'feature-icon', 'title' => $current_property->colour->name, 'data-toggle' => 'tooltip', 'data-placement' => 'top']);
                }                
            }
        break;
        case 'get-images':



        break;
        case 'get-prices':
            $min_price = 0;
            $max_price = 0;
            $isFirst = true;
            
            foreach($product->sizes as $size_element){
                $current_price = $size_element->_joinData->price;
                
                if($isFirst){
                    $min_price = $current_price;
                    $max_price = $current_price;
                    
                    $isFirst = false;
                }
                else{
                    if($current_price < $min_price){
                        $min_price = $current_price;
                    }
                    
                    if($current_price > $max_price){
                        $max_price = $current_price;
                    }
                }
                            
            }
            
            echo '<div class="col-md-12 '.$element_class_value.'"><span>'.number_format($min_price, 2).' € ~ '.number_format($max_price, 2).' €</span></div> ';            


        break;


    }


?>
