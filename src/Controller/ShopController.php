<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;
//use Cake\View\Helper\SessionHelper;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ShopController extends AppController
{
    
    public $paginate = [
        'limit' => 15        
    ];
    
    public function initialize()
    {
        parent::initialize();   
        
        $this->loadComponent('Auth');
        $this->loadComponent('CmpInputSecure');
        $this->loadComponent('CmpUsers');
        $this->loadComponent('CmpFiles');
        $this->loadComponent('CmpMailer');
        $this->loadComponent('CmpOrderCalculator');
        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => true, // true/false
            'sitekey' => '6LeSfFMUAAAAAPFCrU70BYF8_Uv6rOBPfOo_DdiN',
            'secret' => '6LeSfFMUAAAAAE6LY_KEO61dgHI3KuUzAc6eZ0Y5',
            'type' => 'image', // image/audio
            'theme' => 'light', // light/dark
            'lang' => 'es', // default en
            'size' => 'normal'  // normal/compact
        ]);
        
        
    }
    
    public function isAuthorized() {
        
        /***************/
        return true;
        /***************/
        
        $user = $this->CmpUsers->current();
        if ($user->role == 'superadmin') {
            return true;
        } else {
            
            if($user->role == 'admin'){
                
                if (!in_array($this->request->action, [])) {
                    return true;
                }                
            }
            elseif($user->role == 'worker'){
                
                if (!in_array($this->request->action, ['add','edit'])) {
                    return true;
                }                
            }
            
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }

    public function index($id=0)
    {
        $this->loadModel('Products');
        $this->loadModel('Features');
        $this->loadModel('Dyes');
        
        $product = $this->Products
            ->find()
            ->contain(
                [
                    'Categories',
                    'Categories.ParentCategories',
                    'Properties','Properties.Colours',
                    'Sizes','Attributes','Templates',
                    'Templates.Options',
                    'Templates.Options.Vinyls',
                    'Templates.Options.Vinyls.Dyes'
                ])
            ->where(['Products.id' => $id])
            ->first();
        
        $features = $this->Features
            ->find()
            ->where(['Features.product_id' => $product->id])
            ->contain(['Properties','Properties.Colours','Sizes'])
            ->order(['Features.property_id' => 'ASC','Sizes.date_order' => 'ASC']);
        
        $dyes = $this->Dyes->find();
            
        
        
        $this->set('product',$product);
        $this->set('features',$features);
        $this->set('dyes',$dyes);
        
        $this->viewBuilder()->layout('client');
    }
    
    public function order($product_id=0)
    {
        $this->loadModel('Products');
        $this->loadModel('Features');
        $this->loadModel('Dyes');
        $this->loadModel('Orders');
        $this->loadModel('Suborders');
        $this->loadModel('FeaturesSuborders');
        $this->loadModel('Options');
        $this->loadModel('OptionsSuborders');
        
        $session = $this->request->session();
        $order_id = $session->read('Client.order_id');
        $order_key = $session->read('Client.order_key');
        
        $order = null;
        
        if(!empty($order_id)){
            if(!empty($order_key)){
                $order = $this->Orders->find()->where(['Orders.id' => $order_id, 'Orders.order_key' => $order_key, 'Orders.completed' => 0])->first();                
            }
        }
        
        if(empty($order_key)){
            $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $order_key ='';
            $max = strlen($characters) - 1;
            for ($i = 0; $i < 100; $i++) {
                $order_key .= $characters[mt_rand(0, $max)];
            }
        }
        
        if($order == null){
            $order = $this->Orders->newEntity();
            $order->status_id = 1;
            $order->payment_method = "Transferencia Bancaria";
            $order->shipping_method = "Correos";
            $order->order_key = $order_key;
            $this->Orders->save($order);
        }
        
        $session->write('Client.order_id', $order->id);
        $session->write('Client.order_key', $order_key);
        
        
        if($product_id > 0){
            
            if ($this->request->is(['patch', 'post', 'put'])) {
                

                $this->set('rq',$this->request->data);

                $product = $this->Products->find()->where(['Products.id' => $product_id])->contain(['Categories'])->first();



                // SUBORDER

                
                $suborder = $this->Suborders->newEntity();
                $suborder->order_id = $order->id;                
                $this->Suborders->save($suborder);


                // FEATURES

                $post_features = $this->request->data['features'];
                $data_features = [];
                $arr_features_ids = [];
                $arr_features_ids[] = 0;

                for($i=0; $i< count($post_features); $i++ ){
                    if(isset($post_features[$i]['id']) && isset($post_features[$i]['quantity'])){
                        if($post_features[$i]['quantity'] != ""){
                            if(is_numeric($post_features[$i]['quantity'])){
                                if($post_features[$i]['quantity'] > 0){
                                    $data_features_elem = [];
                                    $data_features_elem ['id'] = $post_features[$i]['id'];
                                    $data_features_elem ['quantity'] = $post_features[$i]['quantity'];

                                    $data_features[] = $data_features_elem;
                                    $arr_features_ids[] = $post_features[$i]['id'];

                                }
                            }                        
                        }
                    }                
                }


                $features = $this->Features->find()->where(['Features.product_id' => $product->id, 'Features.id IN' => $arr_features_ids]);



                // OPTIONS

                $post_options = $this->request->data['options'];
                $data_options = [];
                $arr_options_ids = [];
                $arr_options_ids[] = 0;

                for($i=0; $i< count($post_options); $i++ ){
                    if(isset($post_options[$i]['id'])){
                        if($post_options[$i]['vinyl_id'] > 0){
                            $data_option_elem = [];
                            $data_option_elem ['id'] = $post_options[$i]['id'];
                            $data_option_elem ['vinyl_id'] = $post_options[$i]['vinyl_id'];
                            $data_option_elem ['dye_id'] = $post_options[$i]['dye_id'];
                            $data_option_elem ['client_file'] = ''; //$post_options[$i]['image'];

                            /**
                            ** START INPUT FILE
                            **/

                            /** VARIABLES **********************************/

                            $save_in_folder = 'img' . DS . 'client_file';
                            $extensions = ['png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'];

                            /***********************************************/            
                            $input_file_data = $post_options[$i]['image'];  
                            if ($input_file_data['size'] > 0) {
                                $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                                if($file_result['error'] != '') {
                                    $text_error = __('Ocurrió un error.');
                                    switch ($file_result['error'])
                                    {
                                        case 'MAX_FILES_ERROR':
                                        $text_error = 'Excedido el límite maximo de imágenes.';
                                        break;
                                        case 'NOT_VALID_EXTENSION_ERROR':
                                        $text_error = 'Extensión no valida.';
                                        break;
                                        case 'ERROR_OTHER':
                                        $text_error = 'Ocurrió un error.';
                                        break;
                                    }
                                    $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                                    return $this->redirect(['action' => 'index']);
                                }       
                                $data_option_elem['client_file'] = $file_result['file_name'][0];
                            }            
                            /**
                            ** END INPUT FILE
                            **/

                            $data_options[] = $data_option_elem;
                            $arr_options_ids[] = $post_options[$i]['id'];                                              
                        }
                    }                
                }


                $search_template_id = $product->template_id;

                $options = $this->Options->find()->where(['Options.id IN' => $arr_options_ids])->contain(['Vinyls']);
                $options->matching('Templates', function ($q)  use ($search_template_id) {
                    return $q->where(['Templates.id' => $search_template_id]);
                });


                // ORDER, FEAUTRES, OPTIONS



                foreach($features as $f_elem){
                    $featured_suborder = $this->FeaturesSuborders->newEntity();
                    $featured_suborder->feature_id = $f_elem->id;
                    $featured_suborder->suborder_id = $suborder->id;
                    $featured_suborder->unit_price = $f_elem->price;
                    $featured_suborder->quantity = 0;
                    foreach ($data_features as $data_features_element){
                        if($data_features_element['id'] == $f_elem->id){
                            $featured_suborder->quantity = $data_features_element['quantity'];
                        }
                    }

                    $this->FeaturesSuborders->save($featured_suborder);
                }

                foreach($options as $opt_elem){
                    foreach ($data_options as $data_options_element){
                        if($data_options_element['id'] == $opt_elem->id){
                            $option_suborder = $this->OptionsSuborders->newEntity();
                            $option_suborder->option_id = $opt_elem->id;
                            $option_suborder->suborder_id = $suborder->id;
                            $option_suborder->client_file = $data_options_element['client_file'];
                            $option_suborder->dye_id = $data_options_element['dye_id'];
                            $option_suborder->vinyl_id = $data_options_element['vinyl_id'];
                            $option_suborder->stamp_price = 0;
                            foreach($opt_elem->vinyls as $elem_vinyle){
                                if($elem_vinyle->id == $option_suborder->vinyl_id){
                                    $option_suborder->stamp_price = $elem_vinyle->_joinData->price;
                                }
                            }

                            $this->OptionsSuborders->save($option_suborder);
                        }
                    }                    
                }

                $order = $this->Orders->find()->where(['Orders.id' => $order->id])->contain([
                    'Suborders',
                    'Suborders.Options',
                    'Suborders.OptionsSuborders',
                    'Suborders.OptionsSuborders.Options',
                    'Suborders.OptionsSuborders.Dyes',
                    'Suborders.OptionsSuborders.Vinyls',
                    'Suborders.Features',
                    'Suborders.Features.Sizes',
                    'Suborders.Features.Properties',
                    'Suborders.Features.Properties.Colours',
                    'Suborders.Features.Products',
                    'Suborders.Features.Properties',
                    'Suborders.Features.Products.Categories',
                    'Suborders.Features.Products.Categories.ParentCategories'
                ])->first();/*->order(['Features.Sizes.date_order' => 'ASC'])*/


                

                $order = $this->CmpOrderCalculator->calc_all($order);
                $this->Orders->save($order);
                


            } 
            else{
                $this->Flash->error(__('Error en los datos'));
                return $this->redirect(['action' => 'index', $id]);
            }
        
        }
        
        
        
        $order = $this->Orders->find()->where(['Orders.id' => $order->id])->contain([
            'Suborders',
            'Suborders.Options',
            'Suborders.OptionsSuborders',
            'Suborders.OptionsSuborders.Options',
            'Suborders.OptionsSuborders.Dyes',
            'Suborders.OptionsSuborders.Vinyls',
            'Suborders.Features',
            'Suborders.Features.Sizes',
            'Suborders.Features.Properties',
            'Suborders.Features.Properties.Colours',
            'Suborders.Features.Products',
            'Suborders.Features.Properties',
            'Suborders.Features.Products.Categories',
            'Suborders.Features.Products.Categories.ParentCategories'
        ])->first();/*->order(['Features.Sizes.date_order' => 'ASC'])*/
        
        //$dyes = $this->Dyes->find();
        
        
        

        //$this->set('dyes',$dyes);
        $this->set('order',$order);
        //$this->set('orderShow',$order);
        
        
        
        
        $this->viewBuilder()->layout('client');
    }
    
    public function deleteSuborder($suborder_id){
              
        $this->request->allowMethod(['post', 'delete']);
        
        $this->loadModel('Orders');
        $this->loadModel('Suborders');
        $this->loadModel('FeaturesSuborders');
        $this->loadModel('OptionsSuborders');
        
        $session = $this->request->session();
        $order_id = $session->read('Client.order_id');
        $order_key = $session->read('Client.order_key');

        $order = $this->Orders->find()->where(['Orders.id' => $order_id, 'Orders.order_key' => $order_key, 'Orders.completed' => 0])->first();
        
        if($order != null){
            $suborder = $this->Suborders->find()->where(['Suborders.id' => $suborder_id, 'Suborders.order_id' => $order->id])->first();
            
            if($suborder != null){                
                if ($this->Suborders->delete($suborder)) {
                    $features_suborders_del = $this->FeaturesSuborders->find()->where(['FeaturesSuborders.suborder_id' => $suborder_id]);
                    foreach ($features_suborders_del as $feature_suborder_del){
                        $this->FeaturesSuborders->delete($feature_suborder_del);
                    }
                    
                    $options_suborders_del = $this->OptionsSuborders->find()->where(['OptionsSuborders.suborder_id' => $suborder_id]);
                    foreach ($options_suborders_del as $option_suborder_del){
                        $this->OptionsSuborders->delete($option_suborder_del);
                    }
                    
                    $order = $this->Orders->find()->where(['Orders.id' => $order->id])->contain([
                        'Suborders',
                        'Suborders.Options',
                        'Suborders.OptionsSuborders',
                        'Suborders.OptionsSuborders.Options',
                        'Suborders.OptionsSuborders.Dyes',
                        'Suborders.OptionsSuborders.Vinyls',
                        'Suborders.Features',
                        'Suborders.Features.Sizes',
                        'Suborders.Features.Properties',
                        'Suborders.Features.Properties.Colours',
                        'Suborders.Features.Products',
                        'Suborders.Features.Properties',
                        'Suborders.Features.Products.Categories',
                        'Suborders.Features.Products.Categories.ParentCategories'
                    ])->first();/*->order(['Features.Sizes.date_order' => 'ASC'])*/

                    $order = $this->CmpOrderCalculator->calc_all($order);
                    $this->Orders->save($order);
                    
                    $this->Flash->success(__('El producto ha sido eliminado del carrito correctamente.'));
                } else {
                    $this->Flash->error(__('El producto no se ha eliminado. Por favor, pruebe de nuevo.'));
                }
            }
        }
        
        
        return $this->redirect(['action' => 'order']);
        
    }
        
    
    public function complete()
    {
        $this->loadModel('Orders');
        
        $session = $this->request->session();
        $order_id = $session->read('Client.order_id');
        $order_key = $session->read('Client.order_key');

        $order = $this->Orders->find()->where(['Orders.id' => $order_id, 'Orders.order_key' => $order_key, 'Orders.completed' => 0])->first();
        
        if ($order == null) {
            $this->Flash->error(('Error en el pedido'));
            return $this->redirect(['action' => 'order']);

        } 
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            if(!empty($this->request->data['shipping_to_billing'])){
                $this->request->data['billing_address'] = $this->request->data['shipping_address'];
                $this->request->data['billing_address2'] = $this->request->data['shipping_address2'];
                $this->request->data['billing_location'] = $this->request->data['shipping_location'];
                $this->request->data['billing_province'] = $this->request->data['shipping_province'];
                $this->request->data['billing_zip'] = $this->request->data['shipping_zip'];
            }
            
            $order = $this->Orders->patchEntity($order, $this->request->data);
            $order->completed = 1;

            $response_captcha = $this->request->data['g-recaptcha-response'];
            
            //if (0) {
            if ($response_captcha == "" || $response_captcha == null || empty($response_captcha)) {
                $this->Flash->error(('Por favor, marque la casilla de "No soy un robot" del formulario para poder enviarlo.'));
                return $this->redirect(['action' => 'order']);
                    
            } 
            else {
                if ($this->Orders->save($order)) {
                    
                    $session->write('Client.order_id', "0");
                    $session->write('Client.order_key', "");
                    
                    $this->set('order',$order);
                                    
                    $data_array=[];
                    $data_array['order_id']= $order->id;
                    $data_array['email']= $order->email;

                    $this->CmpMailer->orderSaved($data_array);
                    $this->Flash->success(('Su pedido ha sido enviado correctamente.'));

                }
                else{
                    $this->Flash->error(__('Error pedido'));
                    return $this->redirect(['action' => 'order']);
                }
                
            }
            
            
        }
        else{
            $this->Flash->error(__('Error pedido'));
            return $this->redirect(['action' => 'order']);
        }
        
        $this->set("order",$order);
        
        $this->viewBuilder()->layout('client');
    }
    
    
    
}




        
        