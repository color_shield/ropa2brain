<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FeaturesController extends AppController
{
    
    public $paginate = [
        'limit' => 150,
        'order' => [
            'Features.id' => 'asc'
        ]
    ];
    
    public function initialize()
    {
        parent::initialize();   
        
        $this->loadComponent('Auth');
        $this->loadComponent('CmpInputSecure');
        $this->loadComponent('CmpUsers');
        $this->loadComponent('CmpFiles');
        
        
    }
    
    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function index()
    {
        
    }
    
    
    public function stepOne($product_id)
    {
        $this->loadModel('Templates');
        $this->loadModel('Categories');
        $this->loadModel('Attributes');
        $this->loadModel('Products');
        $this->loadModel('Colours');
        $this->loadModel('Sizes');
        $this->loadModel('Properties');
        
        $product = $this->Products->find()->where(['Products.id' => $product_id])->contain(['Templates','Categories','Attributes']);
        
        //$product = $this->Products->newEntity();
        if ($this->request->is('post')) {
                         
            $post_properties = $this->request->getData('properties');
            $data_properties = [];
            
            for($i = 0; $i < count($post_properties); $i++){
                if(!empty($post_properties[$i]['colour_id'])){
                    
                    if($post_properties[$i]['colour_id'] != 0 && $post_properties[$i]['colour_id'] != null && $post_properties[$i]['colour_id'] != ''){                    
                        $data_properties_element = [];
                        $data_properties_element['id'] = null;
                        $data_properties_element['colour_id'] = $post_properties[$i]['colour_id'];

                        /**
                        ** START INPUT FILE
                        **/

                        /** VARIABLES **********************************/

                        $save_in_folder = 'img' . DS . 'properties';
                        $extensions = ['png', 'jpg', 'jpeg'];

                        /***********************************************/            
                        $input_file_data = $post_properties[$i]['image'];  
                        if ($input_file_data['size'] > 0) {
                            $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                            if($file_result['error'] != '') {
                                $text_error = __('Ocurrió un error.');
                                switch ($file_result['error'])
                                {
                                    case 'MAX_FILES_ERROR':
                                    $text_error = 'Excedido el límite maximo de imágenes.';
                                    break;
                                    case 'NOT_VALID_EXTENSION_ERROR':
                                    $text_error = 'Extensión no valida.';
                                    break;
                                    case 'ERROR_OTHER':
                                    $text_error = 'Ocurrió un error.';
                                    break;
                                }
                                $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                                return $this->redirect(['action' => 'addAttribute']);
                            }       
                            $data_properties_element['image'] = $file_result['file_name'][0];
                        }            
                        /**
                        ** END INPUT FILE
                        **/

                        //$post_colours[$i]['image'] = '';

                        $data_properties[] = $data_properties_element;
                    }
                }
            }
            
            
            $entities = $this->Properties->newEntities($data_properties);
            $results = $this->Properties->saveMany($entities);
            
            $text_list_properties_ids = '';
            $is_first = true;
            foreach ($results as $result){
                if(!$is_first){
                    $text_list_properties_ids .='-';
                }
                else{
                    $is_first = false;
                }
                
                $text_list_properties_ids .= $result->id;
            }
            
            return $this->redirect(['controller'=>'Features', 'action' => 'stepTwo', $product_id, $text_list_properties_ids]);
           
        }
        
        //$business = $this->Users->Business->find('list', ['limit' => 200]);
        $attributes = $this->Attributes->find();
        $categories = $this->Categories->find('list', ['limit' => 200]);
        $templates = $this->Templates->find('list', ['limit' => 200]);
        
        $colours = $this->Colours->find();
        $sizes = $this->Sizes->find();
        
                
        $this->set(compact('colours'));
        $this->set(compact('sizes'));
        $this->set(compact('product'));
        $this->set(compact('attributes'));
        $this->set(compact('categories'));
        $this->set(compact('templates'));
        
        $this->viewBuilder()->layout('default');
    }

    
    public function stepTwo($product_id,$list_text_properties_ids)
    {
        $this->loadModel('Templates');
        $this->loadModel('Categories');
        $this->loadModel('Attributes');
        $this->loadModel('Products');
        $this->loadModel('Colours');
        $this->loadModel('Sizes');
        $this->loadModel('Properties');
        
        $product = $this->Products->find()->where(['Products.id' => $product_id])->contain(['Templates','Categories','Attributes'])->first();
        
        
        $arr_properties_ids = explode("-", $list_text_properties_ids);
        $properties = $this->Properties->find()->where(['Properties.id IN' => $arr_properties_ids])->contain('Colours');
        
        $this->set('properties',$properties);
        
        //$product = $this->Products->newEntity();
        if ($this->request->is('post')) {
                         
            $post_features = $this->request->getData('features');
            
            $data_features = [];
            
            for($i = 0; $i < count($post_features); $i++){
                
                if(!empty($post_features[$i]['size_id']) ){
                    
                    if($post_features[$i]['size_id'] != '' && $post_features[$i]['size_id'] != null && $post_features[$i]['size_id'] != 0 ){

                        $data_feature_element = [];

                        $data_feature_element['id'] = null;                
                        $data_feature_element['product_id'] = $post_features[$i]['product_id'];
                        $data_feature_element['property_id'] = $post_features[$i]['property_id'];
                        $data_feature_element['size_id'] = $post_features[$i]['size_id'];

                        $data_features[] = $data_feature_element;
                    }
                }
            }
            
            
            $entities = $this->Features->newEntities($data_features);
            $results = $this->Features->saveMany($entities);
                                    
            return $this->redirect(['controller'=>'Features', 'action' => 'stepThree', $product->id]);
           
        }
        
        //$business = $this->Users->Business->find('list', ['limit' => 200]);
        $attributes = $this->Attributes->find();
        $categories = $this->Categories->find('list', ['limit' => 200]);
        $templates = $this->Templates->find('list', ['limit' => 200]);
        
        $colours = $this->Colours->find();
        $sizes = $this->Sizes->find();
        
                
        $this->set(compact('colours'));
        $this->set(compact('sizes'));
        $this->set(compact('product'));
        $this->set(compact('attributes'));
        $this->set(compact('categories'));
        $this->set(compact('templates'));
        
        $this->viewBuilder()->layout('default');
    }
    
    
    public function stepThree($product_id)
    {
        $this->loadModel('Templates');
        $this->loadModel('Categories');
        $this->loadModel('Attributes');
        $this->loadModel('Products');
        $this->loadModel('Colours');
        $this->loadModel('Sizes');
        $this->loadModel('Properties');
        
        $product = $this->Products->find()->where(['Products.id' => $product_id])->contain(['Templates','Categories','Attributes'])->first();
               
        
        
        //$product = $this->Products->newEntity();
        if ($this->request->is('post')) {
                         
            $post_features = $this->request->getData('features');
            
            /*for($i = 0; $i < count($post_colours); $i++){
                $post_colours[$i]['colour_id'] = $post_colours[$i]['id'];
                $post_colours[$i]['id'] = null;
                $post_colours[$i]['image'] = '';
            }*/
            
            
            $entities = $this->Features->newEntities($post_features);
            $results = $this->Features->saveMany($entities);
                                    
            $this->Flash->success(__('Guardado correctamente.'));
            return $this->redirect(['controller'=>'Products', 'action' => 'productsList', $product->id]);
           
        }
        
        //$business = $this->Users->Business->find('list', ['limit' => 200]);
        $attributes = $this->Attributes->find();
        $categories = $this->Categories->find('list', ['limit' => 200]);
        $templates = $this->Templates->find('list', ['limit' => 200]);
        
        $colours = $this->Colours->find();
        $sizes = $this->Sizes->find();
        
        $features = $this->Features->find()->where(['Features.product_id' => $product->id])->contain(['Properties','Properties.Colours','Sizes']);
        
        $this->set(compact('features'));
        
        $this->set(compact('colours'));
        $this->set(compact('sizes'));
        $this->set(compact('product'));
        $this->set(compact('attributes'));
        $this->set(compact('categories'));
        $this->set(compact('templates'));
        
        $this->viewBuilder()->layout('default');
    }
    
    
    public function stock($product_id,$enable_edit=0)
    {
        $this->loadModel('Templates');
        $this->loadModel('Categories');
        $this->loadModel('Attributes');
        $this->loadModel('Products');
        $this->loadModel('Colours');
        $this->loadModel('Sizes');
        $this->loadModel('Properties');
        
        $product = $this->Products->find()->where(['Products.id' => $product_id])->contain(['Templates','Categories','Attributes'])->first();
               
        $features = $this->Features->find()->where(['Features.product_id' => $product->id])->contain(['Properties','Properties.Colours','Sizes'])->order(['Features.property_id' => 'asc']);
        
        $this->set('features',$features);
        
        $this->set('enable_edit',$enable_edit);
        
        //$product = $this->Products->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
                         
            $post_features = $this->request->getData('features');
            
            /*for($i = 0; $i < count($post_colours); $i++){
                $post_colours[$i]['colour_id'] = $post_colours[$i]['id'];
                $post_colours[$i]['id'] = null;
                $post_colours[$i]['image'] = '';
            }*/
            
            
            $entities = $this->Features->newEntities($post_features);
            $results = $this->Features->saveMany($entities);
            
            $this->Flash->success(__('Guardado correctamente.'));
                                    
            return $this->redirect(['controller'=>'Features', 'action' => 'stock', $product->id, $enable_edit ]);
           
        }
        
        //$business = $this->Users->Business->find('list', ['limit' => 200]);
        $attributes = $this->Attributes->find();
        $categories = $this->Categories->find('list', ['limit' => 200]);
        $templates = $this->Templates->find('list', ['limit' => 200]);
        
        $colours = $this->Colours->find();
        $sizes = $this->Sizes->find();
        
                
        $this->set(compact('colours'));
        $this->set(compact('sizes'));
        $this->set(compact('product'));
        $this->set(compact('attributes'));
        $this->set(compact('categories'));
        $this->set(compact('templates'));
        
        $this->viewBuilder()->layout('default');
    }
    
    
    
    public function editProperties($product_id)
    {
        $this->loadModel('Templates');
        $this->loadModel('Categories');
        $this->loadModel('Attributes');
        $this->loadModel('Products');
        $this->loadModel('Colours');
        $this->loadModel('Sizes');
        $this->loadModel('Properties');
        $this->loadModel('Features');
        
        $product = $this->Products->find()->where(['Products.id' => $product_id])->contain(['Properties','Templates','Categories','Attributes'])->first();
                
        if ($this->request->is(['patch', 'post', 'put'])) {
                         
            $post_properties = $this->request->getData('properties');
            $data_properties = [];
            $new_list_colors = [];
            
            
            // Comprobación checkbox que están marcados
            for($i = 0; $i < count($post_properties); $i++){
                if(!empty($post_properties[$i]['colour_id'])){
                    
                    if($post_properties[$i]['colour_id'] != 0 && $post_properties[$i]['colour_id'] != null && $post_properties[$i]['colour_id'] != ''){                    
                        $data_properties_element = [];
                        $data_properties_element['id'] = $post_properties[$i]['id'];
                        $data_properties_element['image'] = '';
                        
                        $property_query_elem = null; 
                        
                        if($data_properties_element['id'] != null){
                            $property_query_elem = $this->Properties->find()->where(['Properties.id' => $data_properties_element['id'] ])->first(); 
                            $data_properties_element['image'] = $property_query_elem->image;
                        }
                        
                        
                        $new_list_colors[] = $data_properties_element['colour_id'] = $post_properties[$i]['colour_id'];

                        /**
                        ** START INPUT FILE
                        **/

                        /** VARIABLES **********************************/

                        $save_in_folder = 'img' . DS . 'properties';
                        $extensions = ['png', 'jpg', 'jpeg'];

                        /***********************************************/            
                        $input_file_data = $post_properties[$i]['image'];  
                        if ($input_file_data['size'] > 0) {
                            $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                            if($file_result['error'] != '') {
                                $text_error = __('Ocurrió un error.');
                                switch ($file_result['error'])
                                {
                                    case 'MAX_FILES_ERROR':
                                    $text_error = 'Excedido el límite maximo de imágenes.';
                                    break;
                                    case 'NOT_VALID_EXTENSION_ERROR':
                                    $text_error = 'Extensión no valida.';
                                    break;
                                    case 'ERROR_OTHER':
                                    $text_error = 'Ocurrió un error.';
                                    break;
                                }
                                $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                                return $this->redirect(['action' => 'addAttribute']);
                            }       
                            $data_properties_element['image'] = $file_result['file_name'][0];
                        }            
                        /**
                        ** END INPUT FILE
                        **/

                        if( $property_query_elem != null){
                            $property_query_elem->image = $data_properties_element['image'];
                            $this->Properties->save($property_query_elem);
                        } 

                        $data_properties[] = $data_properties_element;
                    }
                }
            }
            
            
            // Lista de los elementos que vamos a eliminar. Elementos que no están marcados actualmente pero existian antes
            $in_properties_product_query = $this->Products->find()->where(['Products.id' => $product_id])->contain(['Properties','Templates','Categories','Attributes'])->first();
            $in_properties_product_arr_ids = [];
            $in_properties_product_arr_ids [] = 0;
            foreach ($in_properties_product_query->properties as $tmp_prop){
                if(!in_array($tmp_prop->id, $in_properties_product_arr_ids)){
                    $in_properties_product_arr_ids [] = $tmp_prop->id;
                }                
            }
            
            $query_delete_colours = $this->Properties->find()->select(['id'])->distinct(['id'])->where([ 'Properties.id IN' => $in_properties_product_arr_ids,'Properties.colour_id NOT IN' => $new_list_colors]);
            
            // Lista de elementos que hay que añadir. Elementos marcados ahora pero que antes no lo estaban
            $list_ids_colors_to_add = [];            
            
            foreach ($data_properties as $d_prop){
                $matched = false;
                foreach ($product->properties as $p_prop){
                    if($d_prop['colour_id'] == $p_prop->colour_id ){
                        $matched = true;                        
                    }
                }
                
                if(!$matched){
                    $list_ids_colors_to_add [] = $d_prop['colour_id'];
                }
                
            }
            
            
            $data_properties_to_add = [];
            foreach ($data_properties as $d_prop){
                if( in_array($d_prop['colour_id'], $list_ids_colors_to_add) ){
                    $data_properties_to_add[] = $d_prop;
                }
                
            }
            
            // Eliminación de los elementos y los registros relacionados 
            foreach($query_delete_colours as $property_del){
                $features_del = $this->Features->find()->where(['Features.property_id' => $property_del->id]);
                
                foreach($features_del as $f_del){
                    $this->Features->delete($f_del);
                }
                
                $this->Properties->delete($property_del);
            }
            
            
            
            $list_ids_properties_exist = [];            
            
            foreach ($product->properties as $p_prop){              
                
                $list_ids_properties_exist [] = $p_prop->id;
            }
            
            
            $entities = $this->Properties->newEntities($data_properties_to_add);
            $results = $this->Properties->saveMany($entities);
            
            
            
            $text_list_properties_ids = '';
            $is_first = true;
            foreach ($results as $result){
                if(!in_array($result->id, explode('-',$text_list_properties_ids) )){
                    if(!$is_first){
                        $text_list_properties_ids .='-';
                    }
                    else{
                        $is_first = false;
                    }

                    $text_list_properties_ids .= $result->id;
                }
            }
            
            foreach ($list_ids_properties_exist as $exist_id){
                if(!in_array($exist_id, explode('-',$text_list_properties_ids) )){
                    if(!$is_first){
                        $text_list_properties_ids .='-';
                    }
                    else{
                        $is_first = false;
                    }

                    $text_list_properties_ids .= $exist_id;
                }
            }
            
            
            
            return $this->redirect(['controller'=>'Features', 'action' => 'editFeatures', $product_id, $text_list_properties_ids]);
           
        }
        
       
        $attributes = $this->Attributes->find();
        $categories = $this->Categories->find('list', ['limit' => 200]);
        $templates = $this->Templates->find('list', ['limit' => 200]);
        
        $colours = $this->Colours->find();
        $sizes = $this->Sizes->find();
        
        $features = $this->Features->find()->where(['Features.product_id' => $product->id]);
        
                
        $this->set(compact('features'));
                
        $this->set(compact('colours'));
        $this->set(compact('sizes'));
        $this->set(compact('product'));
        $this->set(compact('attributes'));
        $this->set(compact('categories'));
        $this->set(compact('templates'));
        
        $this->viewBuilder()->layout('default');
    }
    
    
    
    
    
    public function editFeatures($product_id,$list_text_properties_ids)
    {
        $this->loadModel('Templates');
        $this->loadModel('Categories');
        $this->loadModel('Attributes');
        $this->loadModel('Products');
        $this->loadModel('Colours');
        $this->loadModel('Sizes');
        $this->loadModel('Properties');
        $this->loadModel('Features');
        
        $product = $this->Products->find()->where(['Products.id' => $product_id])->contain(['Templates','Categories','Attributes'])->first();
        
        
        $arr_properties_ids = explode("-", $list_text_properties_ids);
        $properties = $this->Properties->find()->where(['Properties.id IN' => $arr_properties_ids])->contain('Colours');
        
        $this->set('properties',$properties);
        
        $features = $this->Features->find()->where(['Features.product_id' => $product->id])->contain('Properties');
        
        $this->set('features',$features);
        
        //$product = $this->Products->newEntity();
        if ($this->request->is('post')) {
                         
            $post_features = $this->request->getData('features');
            
            $data_features = [];
            
            for($i = 0; $i < count($post_features); $i++){
                
                if(!empty($post_features[$i]['size_id']) ){
                    
                    if($post_features[$i]['size_id'] != '' && $post_features[$i]['size_id'] != null && $post_features[$i]['size_id'] != 0 ){

                        $data_feature_element = [];

                        $data_feature_element['id'] = $post_features[$i]['id'];;                
                        $data_feature_element['product_id'] = $post_features[$i]['product_id'];
                        $data_feature_element['property_id'] = $post_features[$i]['property_id'];
                        $data_feature_element['size_id'] = $post_features[$i]['size_id'];

                        $data_features[] = $data_feature_element;
                    }
                }
            }
            
            $list_features_id_post_not_delete = [];
            $list_data_add_features = [];
            foreach ($data_features as $d_feat){
                if($d_feat['id'] != null){
                    $list_features_id_post_not_delete[] = $d_feat['id'];
                }
                else{
                    $list_data_add_features [] = $d_feat;
                }
            }
            
            if(count($list_features_id_post_not_delete) <= 0){
                $list_features_id_post_not_delete[0] = 0;
            }
            
            
            
            /*$in_properties_product_query = $this->Products->find()->where(['Products.id' => $product_id])->contain(['Properties','Templates','Categories','Attributes'])->first();
            $in_properties_product_arr_ids = [];
            $in_properties_product_arr_ids [] = 0;
            foreach ($in_properties_product_query->properties as $tmp_prop){
                if(!in_array($tmp_prop->id, $in_properties_product_arr_ids)){
                    $in_properties_product_arr_ids [] = $tmp_prop->id;
                }                
            }*/
            
            $features_delete_query = $this->Features->find()->where(['Features.product_id' => $product->id,'Features.id NOT IN' => $list_features_id_post_not_delete]);
            foreach ($features_delete_query as $feature_del){
                $this->Features->delete($feature_del);
            }
            
            
            $entities = $this->Features->newEntities($list_data_add_features);
            $results = $this->Features->saveMany($entities);
            
            $this->Flash->success(__('Guardado correctamente.'));
                                    
            return $this->redirect(['controller'=>'Features', 'action' => 'stock', $product->id, 1]);
           
        }
        
        //$business = $this->Users->Business->find('list', ['limit' => 200]);
        $attributes = $this->Attributes->find();
        $categories = $this->Categories->find('list', ['limit' => 200]);
        $templates = $this->Templates->find('list', ['limit' => 200]);
        
        $colours = $this->Colours->find();
        $sizes = $this->Sizes->find();
        
                
        $this->set(compact('colours'));
        $this->set(compact('sizes'));
        $this->set(compact('product'));
        $this->set(compact('attributes'));
        $this->set(compact('categories'));
        $this->set(compact('templates'));
        
        $this->viewBuilder()->layout('default');
    }
    
    
    
    
    

    public function delete($id = null)
    {
        
    }
}
