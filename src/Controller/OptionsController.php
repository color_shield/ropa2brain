<?php

// src/Controller/UsersController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

class OptionsController extends AppController {

    public $paginate = [
        'limit' => 15,
        'order' => [
            'Attributes.id' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Auth'); 
        $this->loadComponent('CmpFiles');
        $this->loadComponent('CmpUsers');
    }
    
    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function addOption() {
        $this->loadModel('Vinyls');
        $vinyls = $this->Vinyls->find();
        $option = $this->Options->newEntity();
        if ($this->request->is('post')) {

            $input_file_name = 'icon';
            $save_in_folder = 'img' . DS . 'options';
            $extensions = ['png', 'jpg', 'jpeg'];

            /*             * ******************************************** */
            $input_file_data = $this->request->data[$input_file_name];
            if ($input_file_data['size'] > 0) {
                $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                if ($file_result['error'] != '') {
                    $text_error = __('Ocurrió un error.');
                    switch ($file_result['error']) {
                        case 'MAX_FILES_ERROR':
                            $text_error = 'Excedido el límite maximo de imágenes.';
                            break;
                        case 'NOT_VALID_EXTENSION_ERROR':
                            $text_error = 'Extensión no valida.';
                            break;
                        case 'ERROR_OTHER':
                            $text_error = 'Ocurrió un error.';
                            break;
                    }
                    $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                    return $this->redirect(['action' => 'add_option']);
                }                
                $this->request->data[$input_file_name] = $file_result['file_name'][0];                
            }
            $post_properties = $this->request->data['vinyls'];

            $data_properties = [];

            for ($i = 0; $i < count($post_properties); $i++) {
                if (!empty($post_properties[$i]['id'])) {

                    if ($post_properties[$i]['id'] != 0 && $post_properties[$i]['id'] != null && $post_properties[$i]['id'] != '') {
                        $data_properties_element = [];
                        $data_properties_element['id'] = $post_properties[$i]['id'];
                        $data_properties_element['_joinData'] = $post_properties[$i]['_joinData'];

                        $data_properties [] = $data_properties_element;
                    }
                }
            }
            $this->request->data['vinyls'] = $data_properties;
            $option = $this->Options->patchEntity($option, $this->request->getData(), ['associated' => ['Vinyls']]);
            if ($this->Options->save($option)) {
                $this->Flash->success(__('La opción de plantilla se ha guardado correctamente.'));
                return $this->redirect(['action' => 'options_list']);
            }
            $this->Flash->error(__('La opción de plantilla no ha sido guardada, intentelo de nuevo.'));
        }
        $this->set(compact('option', 'vinyls'));
        $this->viewBuilder()->layout('default');
    }

    public function optionsList() {
        $options = $this->Options->find();
        $options = $this->paginate($options);
        $this->set(compact('options'));
    }

    public function delete($option_id) {
        $this->request->allowMethod(['post', 'delete']);
        $option = $this->Options->get($option_id);

        $delete_file = [];
        $delete_file['folder'] = 'img' . DS . 'options';
        $delete_file['file_name'] = $option->icon;

        if ($this->Options->delete($option)) {
            $this->CmpFiles->deleteFile($delete_file);
            $this->Flash->success(__('La opción de plantilla ha sido borrada correctamente.'));
        } else {
            $this->Flash->error(__('La opción de plantilla no puede ser borrada. Por favor, pruebe otra vez.'));
        }

        return $this->redirect(['action' => 'options_list']);
        $this->viewBuilder()->layout('default');
    }

    public function editOption($option_id) {
        $this->loadModel('Vinyls');
        $vinyls = $this->Vinyls->find();
        $option = $this->Options->find()->where(['Options.id' => $option_id])->contain('Vinyls')->first();
        $option_image = $option->icon;
        if ($this->request->is(['patch', 'post', 'put'])) {

            $input_file_name = 'icon';
            $save_in_folder = 'img' . DS . 'options';
            $extensions = ['png', 'jpg', 'jpeg'];

            $delete_file = [];
            $delete_file['folder'] = 'img' . DS . 'options';
            $delete_file['file_name'] = $option->icon;


            /*             * ******************************************* */
            $input_file_data = $this->request->data[$input_file_name];
            if ($input_file_data['size'] > 0) {
                $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                if ($file_result['error'] != '') {
                    $text_error = __('Ocurrió un error.');
                    switch ($file_result['error']) {
                        case 'MAX_FILES_ERROR':
                            $text_error = 'Excedido el límite maximo de imágenes.';
                            break;
                        case 'NOT_VALID_EXTENSION_ERROR':
                            $text_error = 'Extensión no valida.';
                            break;
                        case 'ERROR_OTHER':
                            $text_error = 'Ocurrió un error.';
                            break;
                    }
                    $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                    return $this->redirect(['action' => 'add_option']);
                }
                $this->request->data[$input_file_name] = $file_result['file_name'][0];
                $this->CmpFiles->deleteFile($delete_file);
            } else {
                $this->request->data[$input_file_name] = $option_image;
            }
            /*             * ******************************************* */

            $post_properties = $this->request->data['vinyls'];
            $data_properties = [];
            for ($i = 0; $i < count($post_properties); $i++) {
                if (!empty($post_properties[$i]['id'])) {
                    if ($post_properties[$i]['id'] != 0 && $post_properties[$i]['id'] != null && $post_properties[$i]['id'] != '') {
                        $data_properties_element = [];
                        $data_properties_element['id'] = $post_properties[$i]['id'];
                        $data_properties_element['_joinData'] = $post_properties[$i]['_joinData'];
                        $data_properties [] = $data_properties_element;
                    }
                }
            }
            $this->request->data['vinyls'] = $data_properties;
            $option = $this->Options->patchEntity($option, $this->request->getData(), ['associated' => ['Vinyls']]);
            if ($this->Options->save($option)) {
                $this->Flash->success(__('La opción de plantilla se ha editado correctamente.'));
                return $this->redirect(['action' => 'options_list']);
            }
            $this->Flash->error(__('La opción de plantilla no ha sido editada, intentelo de nuevo.'));
        }
        $this->set(compact('option', 'vinyls'));
        $this->viewBuilder()->layout('default');
    }

}
