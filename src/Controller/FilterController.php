<?php

// src/Controller/UsersController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

class FilterController extends AppController {

    public $paginate = [
        'limit' => 20        
    ];

    public function initialize() {
        parent::initialize();
        //$this->loadComponent('Auth'); 
        $this->loadComponent('CmpFiles');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }

    public function catalogue($category = "") {
        $this->loadModel('Products');
        $this->loadModel('Attributes');
        $this->loadModel('Categories');
        $this->loadModel('Properties');
        $this->loadModel('Colours');
        $this->loadModel('Sizes');
        $this->loadModel('Features');
        $this->loadModel('Templates');

        $colours = $this->Colours->find();
        $attributes = $this->Attributes->find();
        $categories = $this->Categories->find();
        $sizes = $this->Sizes->find()->distinct()->order(['Sizes.date_order'=>'asc']);

        $query_params = [];
                
        /*

        if ($category == "") {
            $products = $this->Products->find()->contain(['Categories', 'Properties', 'Properties.Colours', 'Sizes', 'Attributes', 'Templates']);
        } else {
            $products = $this->Products->find()->contain(['Categories', 'Properties', 'Properties.Colours', 'Sizes', 'Attributes', 'Templates']);
            $products->matching('Categories', function ($q) use ($category) {
                return $q->where(['Categories.id' => $category]);
            });
        }
        */
        
        if ($this->request->is('get')) {
            $categories_form = $this->request->getQuery('categories_form');
            $colours_form = $this->request->getQuery('colours_form');
            $sizes_form = $this->request->getQuery('sizes_form');
            $attributes_form = $this->request->getQuery('attributes_form');
            
            $query_params = $this->request->getQueryParams();
            

            $products = $this->Products->find()->contain(['Categories', 'Properties', 'Properties.Colours', 'Sizes', 'Attributes', 'Templates']);

            if (isset($categories_form)) {
                //$var1="categorias mandadas";
                $categories_elements = [];
                for ($i = 0; $i < count($categories_form); $i++) {
                    $categories_elements[] = $categories_form[$i]['category_id'];
                }
                $products->matching('Categories', function ($q) use ($categories_elements) {
                    return $q->where(['OR' => [['Categories.id IN' => $categories_elements], ['Categories.category_id IN' => $categories_elements]]]);
                });
                $products->group('Products.id');
            } elseif ($category!="") {
                $products = $this->Products->find()->contain(['Categories', 'Properties', 'Properties.Colours', 'Sizes', 'Attributes', 'Templates']);
                $products->matching('Categories', function ($q) use ($category) {
                    return $q->where(['OR' => [['Categories.id' => $category], ['Categories.category_id' => $category]]]);
                });
            }else{
                $products = $this->Products->find()->contain(['Categories', 'Properties', 'Properties.Colours', 'Sizes', 'Attributes', 'Templates']);
            }

            if (isset($colours_form)) {
                $colours_elements = [];
                for ($i = 0; $i < count($colours_form); $i++) {
                    $colours_elements[] = $colours_form[$i]['colour_id'];
                }
                $products->matching('Properties.Colours', function ($q) use ($colours_elements) {
                    return $q->where(['Colours.id IN' => $colours_elements]);
                });
                $products->group('Products.id');
            }
            if (isset($sizes_form)) {
                $sizes_elements = [];
                for ($i = 0; $i < count($sizes_form); $i++) {
                    $sizes_elements[] = $sizes_form[$i]['size_id'];
                }
                $products->matching('Sizes', function ($q) use ($sizes_elements) {
                    return $q->where(['Sizes.id IN' => $sizes_elements]);
                });
                $products->group('Products.id');
            }
            if (isset($attributes_form)) {
                $attributes_elements = [];
                for ($i = 0; $i < count($attributes_form); $i++) {
                    $attributes_elements[] = $attributes_form[$i]['attribute_id'];
                }
                $products->matching('Attributes', function ($q) use ($attributes_elements) {
                    return $q->where(['Attributes.id IN' => $attributes_elements]);
                });
                $products->group('Products.id');
            }
            $products = $this->paginate($products);
            $this->set(compact('products'));
        }
        // -----------------

        //$products = $this->paginate($products);
        $this->set(compact('products', 'category', 'colours', 'categories', 'attributes', 'sizes'));
        $this->set("query_params",$query_params);

        $this->viewBuilder()->layout('client');
    }

}
