<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    
    public $paginate = [
        'limit' => 25,
        'order' => [
            'Users.id' => 'asc'
        ]
    ];
    
    public function initialize()
    {
        parent::initialize();   
        
        $this->loadComponent('Auth');
        $this->loadComponent('CmpInputSecure');
        $this->loadComponent('CmpUsers');
        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => false, // true/false
            'sitekey' => '6LfC00YUAAAAAKSLT_j5r-14RsHSwBcE2wCoBVDj', //if you don't have, get one: https://www.google.com/recaptcha/intro/index.html
            'secret' => '6LfC00YUAAAAAFPZMegvQ7I75kf8CCLR4zl7z6tk',
            'type' => 'image', // image/audio
            'theme' => 'light', // light/dark
            'lang' => 'es', // default en
            'size' => 'normal'  // normal/compact
        ]);
        
        
        
    }
    
    public function isAuthorized() {
        
        $user = $this->CmpUsers->current();
        if ($user->role == 'superadmin') {
            return true;
        } else {
            
            if($user->role == 'admin'){
                
                if (!in_array($this->request->action, [])) {
                    return true;
                }                
            }
            elseif($user->role == 'worker'){
                
                if (!in_array($this->request->action, ['add','edit'])) {
                    return true;
                }                
            }
            
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['login', 'forgotPassword', 'changePassword']);
    }

    
    
    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function forgotPassword() {
        if ($this->request->is('post')) {
            $email = $this->request->data["email"];
            $result = $this->Users->find()->where(['email' => $email]);

            if ($result->count() > 0) {
                $user = $result->first();

                if ($user->role != 'admin') {
                    $user = $this->CmpUsers->generateForgotPasswordKey($user);

                    $email_from = Configure::read('EmailAddresses.default.notifications');
                    $this->CmpEmails->forgotPassword(['to' => $user->email, 'from' => $email_from]);
                    $this->set('from', $user);

                    $this->Flash->success(__('Forgot password sent'));
                    return $this->redirect(['action' => 'forgotPassword']);
                }

                $this->Flash->error(__('User does not exist'));
                return $this->redirect(['action' => 'forgotPassword']);
            }
            $this->Flash->error(__('User does not exist'));
        }

        $this->viewBuilder()->layout('default');
    }

    public function changePassword($key = '') {

        if ($this->request->is('post')) {

            if (!empty($this->request->data["recover_key"])) {
                $recover_key = $this->request->data["recover_key"];
                $result = $this->Users->find()->where(['recover_key' => $recover_key]);

                if ($result->count() > 0) {
                    $user = $result->first();
                    if ($user->role != 'admin') {
                        $new_password = $this->request->data["password"];
                        $repeat_new_password = $this->request->data["repeat_password"];
                        if ($new_password == $repeat_new_password) {
                            $user->password = $new_password;
                            $this->Users->save($user);

                            $this->Flash->success(__('Save successfuly'));
                            return $this->redirect(['action' => 'changePassword']);
                        }
                        $this->Flash->error(__('User does not exist'));
                    }
                }
                $this->Flash->error(__('User does not exist'));
            }
            $this->Flash->error(__('User does not exist'));
        }

        $this->set('recover_key', $key);
    }

    public function login() {
        
        if ($this->request->is('post')) {

            $this->request->data['password'] = $this->CmpInputSecure->sanitize($this->request->data['password']);

            /*if ($this->Recaptcha->verify()) {*/
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    $this->set('user', $user);
                    if ($user['role'] == 'worker')
                        return $this->redirect(['controller' => 'Users', 'action' => 'index']);
                    else
                        return $this->redirect(['controller' => 'Users', 'action' => 'index']);
                }
                $this->Flash->error(__('Datos de identificación incorrectos.'));
            /*}
            else {
                $this->Flash->error(__('Please pass Google Recaptcha first'));
            }*/
        }
        if ($this->request->is('get')) {

            
        }

        $this->viewBuilder()->layout('default');
    }
    
    
    
    
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $current_user = $this->CmpUsers->current();     
        $this->set('current_user',$current_user);
        
        
        $this->viewBuilder()->layout('default');
    }
    
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function listed()
    {
        $current_user = $this->CmpUsers->current();     
        $this->set('current_user',$current_user);
        
        if($current_user->role == 'worker'){
            return $this->redirect(['controller' => 'Videos', 'action' => 'index']);
        }
        
        /*$this->paginate = [
            'contain' => ['Business']
        ];*/
        
        $query_users = $this->Users->find();
        
        if($current_user->role != 'superadmin'){
            $query_users->where(['Users.business_id'=>$current_user->business_id, 'Users.role != ' => 'superadmin']);
        }
        
        $query_users->contain('Business');
        
        $users = $this->paginate($query_users);   

        $this->set(compact('users'));
        
        $this->viewBuilder()->layout('default');
    }
    

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    
    /*public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Business', 'Videos']
        ]);

        $this->set('user', $user);
        $this->viewBuilder()->layout('default');
    }*/

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $current_user = $this->CmpUsers->current();     
        $this->set('current_user',$current_user);
        
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            
            if($current_user->role == 'admin'){
                $user->business_id = $current_user->business_id; 
                if($user->role == 'superadmin'){
                    $user->role = 'admin';
                }
            }
            
            if ($this->Users->save($user)) {
                $this->Flash->success(__('El usuario se ha guardado correctamente.'));

                return $this->redirect(['action' => 'listed']);
            }
            $this->Flash->error(__('El usuario no ha sido guardado, intentelo de nuevo.'));
        }
        $business = $this->Users->Business->find('list', ['limit' => 200]);
        if($current_user->role == 'admin'){
            $business->where(['id'=>$current_user->business_id]);                
        }
        $this->set(compact('user', 'business'));
        $this->viewBuilder()->layout('default');
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        
        $current_user = $this->CmpUsers->current();     
        $this->set('current_user',$current_user);
        
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        
        if($current_user->role == 'admin' && ($user->business_id != $current_user->business_id) ){
            exit();
        }
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            if(empty($this->request->data['password'])) {
                $user = $this->Users->patchEntity($user, $this->request->data,[
                    'fieldList' => ['email', 'name', 'role', 'business_id']
                ]); 
            }
            else{
                $user = $this->Users->patchEntity($user, $this->request->getData());
            }
            
            if($current_user->role == 'admin'){
                $user->business_id = $current_user->business_id; 
                if($user->role == 'superadmin'){
                    $user->role = 'admin';
                }
            }
            
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuario guardado correctamente.'));

                return $this->redirect(['action' => 'listed']);
            }
            $this->Flash->error(__('No se ha podido guardar el usuario, inténtelo más tarde.'));
        }
        $business = $this->Users->Business->find('list', ['limit' => 200]);
        if($current_user->role == 'admin'){
            $business->where(['id'=>$current_user->business_id]);                
        }
        $this->set(compact('user', 'business'));
        $this->viewBuilder()->layout('default');
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $current_user = $this->CmpUsers->current();     
        
        
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        
        if($current_user->role == 'admin' && ($user->business_id != $current_user->business_id) ){
            exit();
        }
        
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('Se ha eliminado el usuario correctamente.'));
        } else {
            $this->Flash->error(__('Error al eliminar el usuario.'));
        }

        return $this->redirect(['action' => 'listed']);
        
    }
}
