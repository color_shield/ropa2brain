<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AttributesController extends AppController
{
    
    public $paginate = [
        'limit' => 15,
        'order' => [
            'Attributes.id' => 'asc'
        ]
    ];
    
    public function initialize()
    {
        parent::initialize();   
        
        $this->loadComponent('Auth');
        $this->loadComponent('CmpInputSecure');
        $this->loadComponent('CmpUsers');
        $this->loadComponent('CmpFiles');
        
        
    }
    
    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $attributes = $this->Attributes->find();
        
        $this->set('attributes',$attributes);
        
        $this->viewBuilder()->layout('default');
    }
    
    
    public function addAttribute()
    {
        $attribute = $this->Attributes->newEntity();
        if ($this->request->is('post')) {
            
            /**
            ** START INPUT FILE
            **/
            
            /** VARIABLES **********************************/
            
            $input_file_name = 'icon';
            $save_in_folder = 'img' . DS . 'attributes';
            $extensions = ['png', 'jpg', 'jpeg'];
            
            /***********************************************/            
            $input_file_data = $this->request->data[$input_file_name];  
            if ($input_file_data['size'] > 0) {
                $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                if($file_result['error'] != '') {
                    $text_error = __('Ocurrió un error.');
                    switch ($file_result['error'])
                    {
                        case 'MAX_FILES_ERROR':
                        $text_error = 'Excedido el límite maximo de imágenes.';
                        break;
                        case 'NOT_VALID_EXTENSION_ERROR':
                        $text_error = 'Extensión no valida.';
                        break;
                        case 'ERROR_OTHER':
                        $text_error = 'Ocurrió un error.';
                        break;
                    }
                    $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                    return $this->redirect(['action' => 'addAttribute']);
                }       
                $this->request->data[$input_file_name] = $file_result['file_name'][0];
            }            
            /**
            ** END INPUT FILE
            **/
            
            $attribute = $this->Attributes->patchEntity($attribute, $this->request->getData());
                             
            if ($this->Attributes->save($attribute)) {
                $this->Flash->success(__('El atributo se ha guardado correctamente.'));

                return $this->redirect(['action' => 'addAttribute']);
            }
            $this->Flash->error(__('El atributo no ha sido guardado, intentelo de nuevo.'));
        }
                
        $this->set(compact('attribute'));
        
        $this->viewBuilder()->layout('default');
    }
    
    public function attributesList(){
        $attributes = $this->Attributes->find();
        $attributes = $this->paginate($attributes);
        $this->set(compact('attributes'));
    }
    

    
    public function editAttribute($id = null) {
       $attribute = $this->Attributes->get($id);

       if ($this->request->is(['patch', 'post', 'put'])) {
           $attribute = $this->Attributes->patchEntity($attribute, $this->request->getData());
           if ($this->request->data['new_image'] != "") {
               $input_file_name = 'new_image';
               $save_in_folder = 'img' . DS . 'attributes';
               $extensions = ['png', 'jpg', 'jpeg'];

               /*                 * ******************************************** */
               $input_file_data = $this->request->data[$input_file_name];
               if ($input_file_data['size'] > 0) {
                    $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                    if ($file_result['error'] != '') {
                        $text_error = __('Ocurrió un error.');
                        switch ($file_result['error']) {
                            case 'MAX_FILES_ERROR':
                                $text_error = 'Excedido el límite maximo de imágenes.';
                                break;
                            case 'NOT_VALID_EXTENSION_ERROR':
                                $text_error = 'Extensión no valida.';
                                break;
                            case 'ERROR_OTHER':
                                $text_error = 'Ocurrió un error.';
                                break;
                       }
                       $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                       return $this->redirect(['action' => 'editAttribute']);
                    }
                   
                    $delete_file = ['folder'=> $save_in_folder, 'file_name'=> $attribute->icon];
                    $this->CmpFiles->deleteFile($delete_file);
                   
                    $attribute->icon = $file_result['file_name'][0];                    
               }
           }
           if ($this->Attributes->save($attribute)) {
               $this->Flash->success(__('El atributo se ha editado correctamente.'));

               return $this->redirect(['action' => 'attributesList']);
           }
           $this->Flash->error(__('El atributo no se ha podido editar. Por favor, pruebe otra vez.'));
       }
       $this->set(compact('attribute'));

       $this->viewBuilder()->layout('default');
   }

    public function delete($attribute_id){
        $this->request->allowMethod(['post', 'delete']);
        $attribute = $this->Attributes->get($attribute_id);
        
        $delete_file = ['folder'=> 'img' . DS . 'attributes','file_name'=>$attribute->icon];
        $this->CmpFiles->deleteFile($delete_file);

        if ($this->Attributes->delete($attribute)) {
            $this->Flash->success(('El atributo ha sido borrado correctamente.'));
        } else {
            $this->Flash->error(('El atributo no puede ser borrado. Por favor, pruebe otra vez.'));
        }

        return $this->redirect(['action' => 'attributes_list']);
        $this->viewBuilder()->layout('default');
    }
}
