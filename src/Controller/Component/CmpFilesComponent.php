<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Network\Exception\InternalErrorException;
use Cake\Utility\Text;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Routing\Router;

/**** ERROR CODES ****
 * 
 * ''   : Sin errores
 * 'NOT_VALID_EXTENSION_ERROR' : Formato no permitido
 * 'MAX_FILES_ERROR' : Superado el numero limite de archivos
 * 
 */

class CmpFilesComponent extends Component
{
    private $default_max_files = 5;
    private $default_multiple = false;
    private $default_extensions = array('png','jpg','jpeg');
    private $default_folder = 'uploads'; /* webroot/uploads/ */
    private $default_path_folder = false;
    //private $default_path = 'downloads';
    
    
    private function completeUploadConfig($config)
    {
        if(empty($config['multiple']))
            $config['multiple'] = $this->default_multiple;
        if(empty($config['max_files']))
            $config['max_files'] = $this->default_max_files;
        if(empty($config['extensions']))
            $config['extensions'] = $this->default_extensions;
        if(empty($config['folder']))
            $config['folder'] = $this->default_folder;
        if(empty($config['path_folder']))
            $config['path_folder'] = $this->default_path_folder;
        /*if(empty($config['path']))
            $config['path'] = $this->default_path;*/
        
        return $config;
    }
    
    //save() return error code.
    public function upload($data,$config=array())
    {           
        $result = ['error'=>'','file_name' => array(),'uploaded_data' => array()];
        
        $config = $this->completeUploadConfig($config);
        if(!empty($data))
        {
            if($config['multiple'])
            {
                if(count($data) > $config['max_files']){
                    $result['error'] = 'MAX_FILES_ERROR'.count($data);
                    return $result; // <-- BREAK
                }
                
                foreach ($data as $file)
                {                    
                    $file_name = $file['name'];
                    $file_temp_name = $file['tmp_name'];
                    //$dir = $this->wwwRoot . implode(DS, $path);
                    //$dir = WWW_ROOT.$config['path'].DS.$config['folder'];
                    $dir = WWW_ROOT.$config['folder'];
                    $extension = substr( strrchr($file_name, '.') , 1);
                    if(!in_array($extension , $config['extensions'] )){
                        $result['error'] = 'NOT_VALID_EXTENSION_ERROR';
                        return $result; // <-- BREAK
                    }elseif(is_uploaded_file($file_temp_name)){
                        $file_name_uuid = Text::uuid().'.'.$extension;
                        $result['file_name'][] = $file_name_uuid;
                        move_uploaded_file($file_temp_name, $dir.DS.$file_name_uuid);
                    }
                }
                
            }
            else
            {
                $file = $data;
                $file_name = $file['name'];
                $file_temp_name = $file['tmp_name'];
                $result['uploaded_data'][0]=$file['error'];
                $result['uploaded_data'][1]=$file['name'];
                $result['uploaded_data'][2]=$file['size'];
                $result['uploaded_data'][3]=$file['tmp_name'];
                $result['uploaded_data'][4]=$file['type'];
                //$dir = $this->wwwRoot . implode(DS, $path);
                //$dir = WWW_ROOT.$config['path'].DS.$config['folder']; 
                $dir = WWW_ROOT.$config['folder'];      
                if($config['path_folder']){
                    /*$dir = Router::url('/', true).$config['folder'];   
                    $dir = str_replace('//', '/', $dir);
                    $dir = str_replace('/', '\\', $dir);
                    $dir = str_replace('http', 'W', $dir);
                    $dir = str_replace('localhost', 'htdocs', $dir);*/
                    $dir = ROOT.DS.$config['folder'];  
                }
                $extension = substr( strrchr($file_name, '.') , 1);
                if(!in_array( $extension , $config['extensions'] )){
                    $result['error'] = 'NOT_VALID_EXTENSION_ERROR';
                    return $result; // <-- BREAK
                }elseif(is_uploaded_file($file_temp_name)){
                    $file_name_uuid = Text::uuid().'.'.$extension;
                    $result['file_name'][0] = $file_name_uuid;
                    move_uploaded_file($file_temp_name, $dir.DS.$file_name_uuid);
                }
                else{
                    $result['error'] = 'ERROR_OTHER';
                    return $result; // <-- BREAK
                }
            }
            
            return $result;  // <-- BREAK
        }
    }
    
    public function deleteFile($data){
        $file = new File(WWW_ROOT.$data['folder'].DS.$data['file_name']);   
        $result = $file->delete();
        $file->close(); // Be sure to close the file when you're done  
        
        return $result;
    }
    
    public function duplicateFile($data){
        
        $result = "";
        
        if(!empty($data['file_name']) && !empty($data['folder']) ){
            if($data['file_name'] != "" && $data['folder'] != "" ){
                
                $origin_file = WWW_ROOT.$data['folder'].DS.$data['file_name'];  
                $extension = substr( strrchr($data['file_name'], '.') , 1);
                $file_name_uuid = Text::uuid().'.'.$extension;
                $target_file = WWW_ROOT.$data['folder'].DS. $file_name_uuid;

                //if ($file->exists()) {
                    //$result = $file_name_uuid;
                    //$dir = new Folder(WWW_ROOT.$data['folder']);
                    if( copy($origin_file, $target_file) ){
                        $result = $file_name_uuid;
                    }
                //}
                //$file->close(); // Be sure to close the file when you're done  
                
            }
        }
        return $result;
    }
    
    public function createFolder($route){
        
        $checkFolder = new Folder($route);
        
        $result = $route;
        
        if (is_null($checkFolder->path)) {
        
            $folder = new Folder();
            
            if ($folder->create($route)) {
                $result = $route;
            }        
            else{
                $result = null;
            }
        }
        
        return $result;
    }
    
    
    public function getWebRoot(){
        $dir = new Folder(WWW_ROOT);
        
        return $dir;
    }
    
    
}
