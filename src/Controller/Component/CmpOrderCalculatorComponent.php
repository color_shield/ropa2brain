<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Network\Exception\InternalErrorException;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\Mailer\Email;

//use Cake\Network\Email;



class CmpOrderCalculatorComponent extends Component {

    public function initialize(array $config) {
        $this->Orders = TableRegistry::get('Orders'); 
    }

    public function calc_all($order) {
        $order->subtotal = 0;
        $order->order_item_count = 0;
         
        foreach ($order->suborders as $suborder){
            foreach ($suborder->features as $feature){
                $order->subtotal += $feature->_joinData->unit_price * $feature->_joinData->quantity;
                $order->order_item_count += $feature->_joinData->quantity;
                foreach ($suborder->options as $option){
                    $order->subtotal += $option->_joinData->stamp_price * $feature->_joinData->quantity;
                }
            }
        }
        
        $order->shipping = 0;
        if($order->subtotal < 130){
            $order->shipping = 7.5;
        }
        
        $order->discount_percent = 0;
        $order->discount_value = 0;
        
        if($order->order_item_count >= 10){
            $order->discount_percent = 15;
        }
        if($order->order_item_count >= 25){
            $order->discount_percent = 24;
        }
        if($order->order_item_count >= 50){
            $order->discount_percent = 27;
        }
        if($order->order_item_count >= 100){
            $order->discount_percent = 30;
        }
        

        if($order->discount_percent > 0){
            $order->discount_value = $order->subtotal * ($order->discount_percent / 100);            
        }
                
        $order->total = ($order->subtotal - $order->discount_value) + $order->shipping;
        $order->tax = ($order->total * 0.21) / 1.21;
       
        return $order;
    }

}

