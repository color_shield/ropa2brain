<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Network\Exception\InternalErrorException;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\Mailer\Email;

//use Cake\Network\Email;



class CmpInputSecureComponent extends Component {

    public function initialize(array $config) {
        //$this->Domains = TableRegistry::get('Domains'); 
    }

    public function sanitize($imput_value) {

        $imput_value = strip_tags($imput_value);

        $imput_value = h($imput_value, true, "utf-8");

        $imput_value = htmlentities($imput_value, ENT_QUOTES | ENT_HTML5, 'UTF-8');

        return $imput_value;
    }

}
