<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Network\Exception\InternalErrorException;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\Controller\Component\AuthComponent;
use Cake\I18n\Time;


class CmpFeaturedProductsComponent extends Component
{   
    //public $components = ['Auth'];
    
    
    public function initialize(array $config)
    {
        $this->Products = TableRegistry::get('Products'); 
    }
        
    public function newProducts($limit=3)
    { 
        $list_new_products = $this->Products->find()->contain(['Categories','Properties','Properties.Colours','Sizes','Attributes'])->order(['Products.created'=>'desc'])->limit(4);
        
        return $list_new_products;
       
    }
    
}