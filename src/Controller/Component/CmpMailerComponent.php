<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Network\Exception\InternalErrorException;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\Mailer\Email;

//use Cake\Network\Email;



class CmpMailerComponent extends Component {

       
    public function contactUs($contact_name,$contact_destination, $from, $message) {
        
        $subject = "Mensaje de ropa.2brain.es para a la atención de: ".$contact_destination;
        $email = new Email();
        try{
        $email->emailFormat('html')
                ->setTemplate('contact')
                ->setLayout('contact')
                ->setViewVars(['contact_destination' => $contact_destination, 'contact_name'=>$contact_name, 'from'=>$from, 'message'=>$message])
                ->from(['tienda@2brain.es' => '2brain.es'])
                ->to(['web@webcoden.com', 'xshaidanx@gmail.com', 'ropa@2brain.es'])
                ->subject($subject)
                ->send();
        } catch (Exception $e){
            echo 'Exception : ',  $e->getMessage(), "\n";
        }
        return true;
    }

    public function orderSaved($data_array){
        
        $subject = "Se ha realizado un pedido para ropa.2brain.es";
        $email = new Email();
        $email2 = new Email();
        try{
        $email->emailFormat('html')
                ->setTemplate('order')
                ->setLayout('order')
                ->setViewVars(['order_id' => $data_array['order_id']])
                ->from(['tienda@2brain.es' => '2brain.es'])
                ->to(['web@webcoden.com', 'xshaidanx@gmail.com', 'ropa@2brain.es'])
                ->subject($subject)
                ->send();
        } catch (Exception $e){
            echo 'Exception : ',  $e->getMessage(), "\n";
        }
        $subject2 = "Confirmación de pedido desde Ropa.2Brain.es";
        try{
        $email2->emailFormat('html')
                ->setTemplate('confirmation')
                ->setLayout('confirmation')
                ->setViewVars(['order_id' => $data_array['order_id']])
                ->from(['tienda@2brain.es' => '2brain.es'])
                ->to($data_array['email'])
                ->subject($subject2)
                ->send();
        } catch (Exception $e){
            echo 'Exception : ',  $e->getMessage(), "\n";
        }
        return true;
    }
   
}
