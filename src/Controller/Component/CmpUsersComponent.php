<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Network\Exception\InternalErrorException;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\Controller\Component\AuthComponent;
use Cake\I18n\Time;


class CmpUsersComponent extends Component
{   
    public $components = ['Auth'];
    
    
    public function initialize(array $config)
    {
        $this->Users = TableRegistry::get('Users'); 
    }
        
    public function current(){ 
        
        $userId = $this->Auth->user('id');
        $user=null;
        
        if($userId != 0 && $userId != null){
            
            $user = $this->Users->find()->select(['id', 'name', 'role', 'email'])->where(['Users.id'=>$userId])->first();   
            
        }
        
        return $user;
    }
    
    public function generateForgotPasswordKey($user){
        
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $recover_key = '';
        for ($i = 0; $i < 50; $i++) {
             $recover_key .= $characters[rand(0, strlen($characters) - 1)];
        }
        $user->recover_key = $recover_key;
        $user->recover_date = Time::now();

        $this->Users->save($user);
        
        return $user;
    }
}