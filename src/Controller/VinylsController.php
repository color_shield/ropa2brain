<?php

// src/Controller/UsersController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

class VinylsController extends AppController {

    public $paginate = [
        'limit' => 15,
        'order' => [
            'Attributes.id' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Auth'); 
        $this->loadComponent('CmpFiles');
        $this->loadComponent('CmpUsers');
    }
    
    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function addVinyl() {
        $this->loadModel('Dyes');
        $dyes = $this->Dyes->find();
        $vinyl = $this->Vinyls->newEntity();
        if ($this->request->is('post')) {

            /**
             * * START INPUT FILE
             * */
            /** VARIABLES ********************************* */
            $input_file_name = 'icon';
            $save_in_folder = 'img' . DS . 'vinyls';
            $extensions = ['png', 'jpg', 'jpeg'];

            /*             * ******************************************** */
            $input_file_data = $this->request->data[$input_file_name];
            if ($input_file_data['size'] > 0) {
                $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                if ($file_result['error'] != '') {
                    $text_error = __('Ocurrió un error.');
                    switch ($file_result['error']) {
                        case 'MAX_FILES_ERROR':
                            $text_error = 'Excedido el límite maximo de imágenes.';
                            break;
                        case 'NOT_VALID_EXTENSION_ERROR':
                            $text_error = 'Extensión no valida.';
                            break;
                        case 'ERROR_OTHER':
                            $text_error = 'Ocurrió un error.';
                            break;
                    }
                    $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                    return $this->redirect(['action' => 'addVinyl']);
                }
                $this->request->data[$input_file_name] = $file_result['file_name'][0];
            }
            /**
             * * END INPUT FILE
             * */
            $vinyl = $this->Vinyls->patchEntity($vinyl, $this->request->getData(), ['associated' => ['Dyes']]);
            //$template = $this->Templates->patchEntity($template, $this->request->getData(), ['associated' => ['Options']]);

            if ($this->Vinyls->save($vinyl)) {
                $this->Flash->success(__('El vinilo se ha guardado correctamente.'));

                return $this->redirect(['action' => 'addVinyl']);
            }
            $this->Flash->error(__('El vinilo no ha sido guardado, intentelo de nuevo.'));
        }

        $this->set(compact('vinyl', 'dyes'));

        $this->viewBuilder()->layout('default');
    }

    public function vinylsList() {
        $vinyls = $this->Vinyls->find();
        $vinyls = $this->paginate($vinyls);
        $this->set(compact('vinyls'));
    }

    public function editVinyl($id = null) {
        $this->loadModel('Dyes');
        $dyes = $this->Dyes->find();
        $vinyl = $this->Vinyls->find()->where(['Vinyls.id' => $id])->contain('Dyes')->first();

        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->request->data['new_image'] != "") {                
                
                $input_file_name = 'new_image';
                $save_in_folder = 'img' . DS . 'vinyls';
                $extensions = ['png', 'jpg', 'jpeg'];
                
                $delete_file=[];
                $delete_file['folder']=$save_in_folder;
                $delete_file['file_name']=$vinyl->icon;

                /*                 * ******************************************** */
                $input_file_data = $this->request->data[$input_file_name];
                if ($input_file_data['size'] > 0) {
                    $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                    if ($file_result['error'] != '') {
                        $text_error = __('Ocurrió un error.');
                        switch ($file_result['error']) {
                            case 'MAX_FILES_ERROR':
                                $text_error = 'Excedido el límite maximo de imágenes.';
                                break;
                            case 'NOT_VALID_EXTENSION_ERROR':
                                $text_error = 'Extensión no valida.';
                                break;
                            case 'ERROR_OTHER':
                                $text_error = 'Ocurrió un error.';
                                break;
                        }
                        $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                        return $this->redirect(['action' => 'editVynil']);
                    }
                    $this->CmpFiles->deleteFile($delete_file);
                    $vinyl->icon = $file_result['file_name'][0];
                }
            }
            
            $data_properties = [];
            if (isset($this->request->data['dyes'])) {
                if ($this->request->data['dyes'] != null || !empty($this->request->data['dyes'])) {
                    $post_properties = $this->request->data['dyes'];
                    
                    for ($i = 0; $i < count($post_properties); $i++) {
                        if (!empty($post_properties[$i]['id'])) {
                            if ($post_properties[$i]['id'] != 0 && $post_properties[$i]['id'] != null && $post_properties[$i]['id'] != '') {
                                $data_properties_element = [];
                                $data_properties_element['id'] = $post_properties[$i]['id'];
                                $data_properties [] = $data_properties_element;
                            }
                        }
                    }                    
                }
            }
            $this->request->data['dyes'] = $data_properties;

            $vinyl = $this->Vinyls->patchEntity($vinyl, $this->request->getData(), ['associated' => ['Dyes']]);
            if ($this->Vinyls->save($vinyl)) {
                $this->Flash->success(__('El vinilo se ha editado correctamente.'));
                return $this->redirect(['action' => 'vinylsList']);
            }
            $this->Flash->error(__('El vinilo no se ha podido editar. Por favor, pruebe otra vez.'));
        }
        $this->set(compact('vinyl', 'dyes'));

        $this->viewBuilder()->layout('default');
    }

    public function delete($vinyl_id) {
        $this->request->allowMethod(['post', 'delete']);
        $vinyl = $this->Vinyls->get($vinyl_id);
        $delete_file=[];
        $delete_file['folder']='img' . DS . 'vinyls';
        $delete_file['file_name']=$vinyl->icon;
        
        if ($this->Vinyls->delete($vinyl)) {
            $this->CmpFiles->deleteFile($delete_file);
            $this->Flash->success(('El vinilo ha sido borrado correctamente.'));
        } else {
            $this->Flash->error(('El vinilo no puede ser borrado. Por favor, pruebe otra vez.'));
        }

        return $this->redirect(['action' => 'vinyls_list']);
        $this->viewBuilder()->layout('default');
    }

}
