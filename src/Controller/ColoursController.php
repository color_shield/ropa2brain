<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ColoursController extends AppController
{
    
    public $paginate = [
        'limit' => 15,
        'order' => [
            'Colours.id' => 'asc'
        ]
    ];
    
    public function initialize()
    {
        parent::initialize();   
        
        $this->loadComponent('Auth');
        $this->loadComponent('CmpInputSecure');
        $this->loadComponent('CmpUsers');
        $this->loadComponent('CmpFiles');
        
        
    }
    
    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function index()
    {
        
    }
    
    
    public function addColour()
    {
        $color = $this->Colours->newEntity();
        if ($this->request->is('post')) {
            
            /**
            ** START INPUT FILE
            **/
            
            /** VARIABLES **********************************/
            
            $input_file_name = 'icon';
            $save_in_folder = 'img' . DS . 'colours';
            $extensions = ['png', 'jpg', 'jpeg'];
            
            /***********************************************/            
            $input_file_data = $this->request->data[$input_file_name];  
            if ($input_file_data['size'] > 0) {
                $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                if($file_result['error'] != '') {
                    $text_error = __('Ocurrió un error.');
                    switch ($file_result['error'])
                    {
                        case 'MAX_FILES_ERROR':
                        $text_error = 'Excedido el límite maximo de imágenes.';
                        break;
                        case 'NOT_VALID_EXTENSION_ERROR':
                        $text_error = 'Extensión no valida.';
                        break;
                        case 'ERROR_OTHER':
                        $text_error = 'Ocurrió un error.';
                        break;
                    }
                    $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                    return $this->redirect(['action' => 'addColour']);
                }       
                $this->request->data[$input_file_name] = $file_result['file_name'][0];
            }            
            /**
            ** END INPUT FILE
            **/
            
            $color = $this->Colours->patchEntity($color, $this->request->getData());
                             
            if ($this->Colours->save($color)) {
                $this->Flash->success(__('El color se ha guardado correctamente.'));

                return $this->redirect(['action' => 'addColour']);
            }
            $this->Flash->error(__('El color no ha sido guardado, intentelo de nuevo.'));
        }
                
        $this->set(compact('color'));
        
        $this->viewBuilder()->layout('default');
    }
    
    public function coloursList(){
        $colours = $this->Colours->find();
        $colours = $this->paginate($colours);
        $this->set(compact('colours'));
    }

    
    public function editColour($id = null) {
       $colour = $this->Colours->get($id);

       if ($this->request->is(['patch', 'post', 'put'])) {
           $colour = $this->Colours->patchEntity($colour, $this->request->getData());
           if ($this->request->data['new_image'] != "") {
               $input_file_name = 'new_image';
               $save_in_folder = 'img' . DS . 'colours';
               $extensions = ['png', 'jpg', 'jpeg'];

               /*                 * ******************************************** */
               $input_file_data = $this->request->data[$input_file_name];
               if ($input_file_data['size'] > 0) {
                   $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                   if ($file_result['error'] != '') {
                       $text_error = __('Ocurrió un error.');
                       switch ($file_result['error']) {
                           case 'MAX_FILES_ERROR':
                               $text_error = 'Excedido el límite maximo de imágenes.';
                               break;
                           case 'NOT_VALID_EXTENSION_ERROR':
                               $text_error = 'Extensión no valida.';
                               break;
                           case 'ERROR_OTHER':
                               $text_error = 'Ocurrió un error.';
                               break;
                       }
                       $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                       return $this->redirect(['action' => 'editColour']);
                   }
                    $delete_file = ['folder'=> $save_in_folder, 'file_name'=> $colour->icon];
                    $this->CmpFiles->deleteFile($delete_file);
                    
                   $colour->icon = $file_result['file_name'][0];                    
               }
           }
           if ($this->Colours->save($colour)) {
               $this->Flash->success(__('El color se ha editado correctamente.'));

               return $this->redirect(['action' => 'coloursList']);
           }
           $this->Flash->error(__('El color no se ha podido editar. Por favor, pruebe otra vez.'));
       }
       $this->set(compact('colour'));

       $this->viewBuilder()->layout('default');
   }

    
    public function delete($colour_id){
        $this->loadModel('Properties');
        $this->loadModel('Features');
        
        
        $this->request->allowMethod(['post', 'delete']);
        $colour = $this->Colours->get($colour_id);
        
        $query_delete_properties = $this->Properties->find()->where(['Properties.colour_id' => $colour->id]);

        $delete_file = ['folder'=> 'img' . DS . 'colours','file_name'=>$colour->icon];
        $this->CmpFiles->deleteFile($delete_file);
        
        if ($this->Colours->delete($colour)) {
            
            foreach($query_delete_properties as $property_del){
                $features_del = $this->Features->find()->where(['Features.property_id' => $property_del->id]);
                
                foreach($features_del as $f_del){
                    $this->Features->delete($f_del);
                }
                
                $this->Properties->delete($property_del);
            }
            
            
            $this->Flash->success(('El color ha sido borrado correctamente.'));
        } else {
            $this->Flash->error(('El color no puede ser borrado. Por favor, pruebe otra vez.'));
        }

        return $this->redirect(['action' => 'colours_list']);
        $this->viewBuilder()->layout('default');
    }
    
    
}
