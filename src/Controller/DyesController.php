<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DyesController extends AppController {

    public $paginate = [
        'limit' => 15,
        'order' => [
            'Dyes.id' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();

        $this->loadComponent('Auth');
        $this->loadComponent('CmpInputSecure');
        $this->loadComponent('CmpUsers');
        $this->loadComponent('CmpFiles');
    }

    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function index() {
        
    }

    public function addDye() {
        $dye = $this->Dyes->newEntity();
        if ($this->request->is('post')) {

            /**
             * * START INPUT FILE
             * */
            /** VARIABLES ********************************* */
            $input_file_name = 'icon';
            $save_in_folder = 'img' . DS . 'dyes';
            $extensions = ['png', 'jpg', 'jpeg'];

            /*             * ******************************************** */
            $input_file_data = $this->request->data[$input_file_name];
            if ($input_file_data['size'] > 0) {
                $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                if ($file_result['error'] != '') {
                    $text_error = __('Ocurrió un error.');
                    switch ($file_result['error']) {
                        case 'MAX_FILES_ERROR':
                            $text_error = 'Excedido el límite maximo de imágenes.';
                            break;
                        case 'NOT_VALID_EXTENSION_ERROR':
                            $text_error = 'Extensión no valida.';
                            break;
                        case 'ERROR_OTHER':
                            $text_error = 'Ocurrió un error.';
                            break;
                    }
                    $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                    return $this->redirect(['action' => 'addDye']);
                }
                $this->request->data[$input_file_name] = $file_result['file_name'][0];
            }
            /**
             * * END INPUT FILE
             * */
            $dye = $this->Dyes->patchEntity($dye, $this->request->getData());

            if ($this->Dyes->save($dye)) {
                $this->Flash->success(__('El color del vinilo se ha guardado correctamente.'));

                return $this->redirect(['action' => 'addDye']);
            }
            $this->Flash->error(__('El color del vinilo no ha sido guardado, intentelo de nuevo.'));
        }

        $this->set(compact('dye'));

        $this->viewBuilder()->layout('default');
    }

    public function dyesList() {
        $dyes = $this->Dyes->find();
        $dyes = $this->paginate($dyes);
        $this->set(compact('dyes'));
    }

    public function editDye($id = null) {
        $dye = $this->Dyes->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $dye = $this->Dyes->patchEntity($dye, $this->request->getData());
            if ($this->request->data['new_image'] != "") {
                $input_file_name = 'new_image';
                $save_in_folder = 'img' . DS . 'dyes';
                $extensions = ['png', 'jpg', 'jpeg'];

                $delete_file = [];
                $delete_file['folder'] = 'img' . DS . 'dyes';
                $delete_file['file_name'] = $dye->icon;

                /*                 * ******************************************** */
                $input_file_data = $this->request->data[$input_file_name];
                if ($input_file_data['size'] > 0) {
                    $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                    if ($file_result['error'] != '') {
                        $text_error = __('Ocurrió un error.');
                        switch ($file_result['error']) {
                            case 'MAX_FILES_ERROR':
                                $text_error = 'Excedido el límite maximo de imágenes.';
                                break;
                            case 'NOT_VALID_EXTENSION_ERROR':
                                $text_error = 'Extensión no valida.';
                                break;
                            case 'ERROR_OTHER':
                                $text_error = 'Ocurrió un error.';
                                break;
                        }
                        $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                        return $this->redirect(['action' => 'editDye']);
                    }
                    $this->CmpFiles->deleteFile($delete_file);
                    $dye->icon = $file_result['file_name'][0];
                }
            }
            if ($this->Dyes->save($dye)) {
                $this->Flash->success(__('El color del vinilo se ha editado correctamente.'));

                return $this->redirect(['action' => 'dyesList']);
            }
            $this->Flash->error(__('El color del vinilo no se ha podido editar. Por favor, pruebe otra vez.'));
        }
        $this->set(compact('dye'));

        $this->viewBuilder()->layout('default');
    }

    public function delete($dye_id) {
        $this->request->allowMethod(['post', 'delete']);
        $dye = $this->Dyes->get($dye_id);

        $delete_file = [];
        $delete_file['folder'] = 'img' . DS . 'dyes';
        $delete_file['file_name'] = $dye->icon;

        if ($this->Dyes->delete($dye)) {
            $this->CmpFiles->deleteFile($delete_file);
            $this->Flash->success(('El color de vinilo ha sido borrado correctamente.'));
        } else {
            $this->Flash->error(('El color de vinilo no puede ser borrado. Por favor, pruebe otra vez.'));
        }

        return $this->redirect(['action' => 'dyes_list']);
        $this->viewBuilder()->layout('default');
    }

}
