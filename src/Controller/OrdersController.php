<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{
    
    public $paginate = [
        'limit' => 15,
        'order' => [
            'Orders.id' => 'desc'
        ]
    ];
    
    public function initialize()
    {
        parent::initialize();   
        
        $this->loadComponent('Auth');
        $this->loadComponent('CmpInputSecure');
        $this->loadComponent('CmpOrderCalculator');
        $this->loadComponent('CmpUsers');
        $this->loadComponent('CmpFiles');
        
        
    }
     
    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function index()
    {
        return $this->redirect(['action' => 'ordersList']);
    }
    
    
    public function ordersList(){
        $orders = $this->Orders->find()->where(['Orders.completed' => 1])->contain(['Statuses','Suborders','Suborders.Features','Suborders.Features.Products','Suborders.Features.Properties']);
        $orders = $this->paginate($orders);
        $this->set(compact('orders'));
    }
    
    public function ordersShow($id = null){
        $this->loadModel('Statuses');
                
        $statuses = $this->Statuses->find();
        
        $order = $this->Orders->find()->where(['Orders.id' => $id])
                ->contain([
                    'Statuses',
                    'Suborders',
                    'Suborders.OptionsSuborders',
                    'Suborders.OptionsSuborders.Options',
                    'Suborders.OptionsSuborders.Vinyls',
                    'Suborders.OptionsSuborders.Dyes',
                    'Suborders.Features',
                    'Suborders.Features.Sizes',
                    'Suborders.Features.Properties',
                    'Suborders.Features.Properties.Colours',
                    'Suborders.Features.Products',
                    'Suborders.Features.Properties',
                    'Suborders.Features.Products.Categories',
                    'Suborders.Features.Products.Categories.ParentCategories'
                ])->first();
        
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            switch ($this->request->data['operation']){
                
                case 'change_status':
                    $order->status_id = $this->request->data['status_id'];
                    if ($this->Orders->save($order)) {
                        $this->Flash->success(__('El estado del pedido se ha modificado correctamente.'));

                        return $this->redirect(['action' => 'ordersShow', $id]);
                    }
                    $this->Flash->error(__('El estado del pedido no se ha podido modificar. Por favor, pruebe otra vez.'));
                break;
                
            }
        }
        
        $this->set(compact('order','statuses'));
    }
        
    public function editOrder($id = null) {
        $this->loadModel('Statuses');
        $this->loadModel('Features');
                
        $statuses = $this->Statuses->find();
        
        $order = $this->Orders->find()->where(['Orders.id' => $id])
                ->contain([
                    'Statuses',
                    'Suborders',
                    'Suborders.OptionsSuborders',
                    'Suborders.OptionsSuborders.Options',
                    'Suborders.OptionsSuborders.Vinyls',
                    'Suborders.OptionsSuborders.Dyes',
                    'Suborders.Features',
                    'Suborders.Features.Sizes',
                    'Suborders.Features.Properties',
                    'Suborders.Features.Properties.Colours',
                    'Suborders.Features.Products',
                    'Suborders.Features.Properties',
                    'Suborders.Features.Products.Categories',
                    'Suborders.Features.Products.Categories.ParentCategories'
                ])->first();
        
        /*$list_current_features_ids = [];
        $list_current_features_ids [] = 0;*/
        
        $list_current_products_ids = [];
        $list_current_products_ids [] = 0;
        
        foreach($order->suborders as $suborder){
            foreach($suborder->features as $feat_elem){
                /*$list_current_features_ids [] = $feat_elem->id;*/
                $list_current_products_ids [] = $feat_elem->product_id;
            }
        }
        
        
        //$all_features = $this->Features->find()->where(['Features.product_id IN'=> $list_current_products_ids, 'Features.id NOT IN ' => $list_current_features_ids])->contain(['Sizes','Properties','Properties.Colours']);
        $all_features = $this->Features->find()->where(['Features.product_id IN'=> $list_current_products_ids])->contain(['Sizes','Properties','Properties.Colours']);
        

        if ($this->request->is(['patch', 'post', 'put'])) {
            
            
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            
            
            
            if ($this->Orders->save($order)) {
                
                $order = $this->Orders->find()->where(['Orders.id' => $order->id])->contain([
                    'Suborders.Options',
                    'Suborders.OptionsSuborders',
                    'Suborders.OptionsSuborders.Options',
                    'Suborders.OptionsSuborders.Dyes',
                    'Suborders.OptionsSuborders.Vinyls',
                    'Suborders.Features',
                    'Suborders.Features.Sizes',
                    'Suborders.Features.Properties',
                    'Suborders.Features.Properties.Colours',
                    'Suborders.Features.Products',
                    'Suborders.Features.Properties',
                    'Suborders.Features.Products.Categories',
                    'Suborders.Features.Products.Categories.ParentCategories'
                ])->first();
            
                $order = $this->CmpOrderCalculator->calc_all($order);

                $this->Orders->save($order);

                $this->Flash->success(__('El pedido se ha modificado correctamente.'));

                return $this->redirect(['action' => 'editOrder', $id]);
            }
            $this->Flash->error(__('El pedido no se ha podido modificar. Por favor, pruebe otra vez.'));
               
            
        }
        
        $this->set(compact('order','statuses'));
        $this->set('all_features',$all_features);
   }
   
      
   
    public function editOrderFeatures($suborder_id = null) {
        $this->loadModel('FeaturesSuborders');
        $this->loadModel('Suborders');
        
        $suborder = $this->Suborders->find()->where(['Suborders.id' => $suborder_id])->contain(['Features','Orders'])->first();
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $post_features = $this->request->data['features'];
            
            /*$list_features_ids = [];
            $list_features_ids [] = 0;
            
            foreach ($post_features as $p_feature){
                if(!isset($p_feature['delete'])){
                    $list_features_ids [] = $p_feature['id'];
                }
            }*/
            
            $features_suborders = $this->FeaturesSuborders->find()->where(['suborder_id'=> $suborder->id]);
            
            $total = 0;
            $count_elem = 0;
            
            if($features_suborders != null){
                $total = $features_suborders->count();
            }
            
            foreach ($features_suborders as $feature_suborder){
                foreach ($post_features as $p_feature){
                    if($feature_suborder->feature_id == $p_feature['id']){
                        if(!isset($p_feature['delete'])){
                            $feature_suborder->quantity = $p_feature['quantity'];
                            $feature_suborder->unit_price = $p_feature['unit_price'];
                            $this->FeaturesSuborders->save($feature_suborder);
                        }
                        else{
                            if($count_elem < ($total - 1) ){
                                $this->FeaturesSuborders->delete($feature_suborder);
                                $count_elem ++;
                            }
                            
                        }
                    }
                }
            }
            
            $this->Suborders->save($suborder);
            
            $order = $this->Orders->find()->where(['Orders.id' => $suborder->order_id])->contain([
                    'Suborders',
                    'Suborders.Options',
                    'Suborders.OptionsSuborders',
                    'Suborders.OptionsSuborders.Options',
                    'Suborders.OptionsSuborders.Dyes',
                    'Suborders.OptionsSuborders.Vinyls',
                    'Suborders.Features',
                    'Suborders.Features.Sizes',
                    'Suborders.Features.Properties',
                    'Suborders.Features.Properties.Colours',
                    'Suborders.Features.Products',
                    'Suborders.Features.Properties',
                    'Suborders.Features.Products.Categories',
                    'Suborders.Features.Products.Categories.ParentCategories'
                ])->first();
            
            $order = $this->CmpOrderCalculator->calc_all($order);
            
            $this->Orders->save($order);
            
            
            //$this->set('data_features',$order);
                                               
            /*if ($this->Orders->save($order)) {
                $this->Flash->success(__('El pedido se ha modificado correctamente.'));

                return $this->redirect(['action' => 'editOrder', $id]);
            }
            $this->Flash->error(__('El pedido no se ha podido modificar. Por favor, pruebe otra vez.'));*/
            
        }
        
        /*$this->Flash->error(__('No post data.'));*/
        return $this->redirect(['action' => 'editOrder', $order->id]);
    }
    
    public function addOrderFeatures($suborder_id = null) {
        $this->loadModel('FeaturesSuborders');
        $this->loadModel('Features');
        $this->loadModel('Suborders');
        
        $suborder = $this->Suborders->find()->where(['Suborders.id' => $suborder_id])->contain(['Features' ])->first();
        
        $order = $this->Orders->find()->where(['Orders.id' => $suborder->order_id])->first();
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            if(!empty($this->request->data['feature_id']) && !empty($this->request->data['quantity'])){
                

                $feature = $this->Features->find()->where(['id' => $this->request->data['feature_id']])->first();

                $new_feature_suborder = $this->FeaturesSuborders->newEntity();
                $new_feature_suborder->suborder_id = $suborder->id;
                $new_feature_suborder->quantity = $this->request->data['quantity'];
                $new_feature_suborder->feature_id = $feature->id;
                $new_feature_suborder->unit_price = $feature->price;

                //$this->set('aaaaa',$new_feature_order);
                //$this->set('feature',$feature);

                if ($this->FeaturesSuborders->save($new_feature_suborder)) {
                    
                    $this->Suborders->save($suborder);
                    
                    $order = $this->Orders->find()->where(['Orders.id' => $suborder->order_id])->contain([
                        'Suborders',
                        'Suborders.Options',
                        'Suborders.OptionsSuborders',
                        'Suborders.OptionsSuborders.Options',
                        'Suborders.OptionsSuborders.Dyes',
                        'Suborders.OptionsSuborders.Vinyls',
                        'Suborders.Features',
                        'Suborders.Features.Sizes',
                        'Suborders.Features.Properties',
                        'Suborders.Features.Properties.Colours',
                        'Suborders.Features.Products',
                        'Suborders.Features.Properties',
                        'Suborders.Features.Products.Categories',
                        'Suborders.Features.Products.Categories.ParentCategories'
                    ])->first();

                    $order = $this->CmpOrderCalculator->calc_all($order);

                    $this->Orders->save($order);
                    
                    
                    $this->Flash->success(__('El pedido se ha modificado correctamente.'));
                    return $this->redirect(['action' => 'editOrder', $order->id]);
                }
                $this->Flash->error(__('El pedido no se ha podido modificar. Por favor, pruebe otra vez.'));
                
            }
            else{
                $this->Flash->error(__('Debes especificar una cantidad y seleccionar una opción.'));
                return $this->redirect(['action' => 'editOrder', $order->id]);
            }
            
        }
        
        $this->Flash->error(__('No post data.'));
        return $this->redirect(['action' => 'editOrder', $order->id]);
    }

    public function delete($order_id){
        
        
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($order_id);

        if ($this->Orders->delete($order)) {
            $this->Flash->success(('El pedido ha sido borrado correctamente.'));
        } else {
            $this->Flash->error(('El pedido no puede ser borrado. Por favor, pruebe otra vez.'));
        }

        return $this->redirect(['action' => 'ordersList']);        
    }
}
