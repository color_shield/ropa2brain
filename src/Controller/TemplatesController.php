<?php

// src/Controller/UsersController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

class TemplatesController extends AppController {

    public $paginate = [
        'limit' => 15,
        'order' => [
            'Attributes.id' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Auth'); 
        $this->loadComponent('CmpFiles');
        $this->loadComponent('CmpUsers');
    }

    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function addTemplate() {
        $this->loadModel('Options');
        $options = $this->Options->find();        
        $template = $this->Templates->newEntity();
        if ($this->request->is('post')) {
            $template = $this->Templates->patchEntity($template, $this->request->getData(), ['associated' => ['Options']]);
            if ($this->Templates->save($template)) {
                $this->Flash->success(__('La plantilla se ha guardado correctamente.'));
                return $this->redirect(['action' => 'add_template']);
            }
            $this->Flash->error(__('La plantilla no ha sido guardada, intentelo de nuevo.'));
        }
        $this->set(compact('template', 'options'));
        $this->viewBuilder()->layout('default');
    }

    public function templatesList() {
        $templates = $this->Templates->find()->contain('Options');
        $templates = $this->paginate($templates);
        $this->set(compact('templates'));
    }

    public function delete($template_id) {
        $this->request->allowMethod(['post', 'delete']);
        $template = $this->Templates->get($template_id);

        if ($this->Templates->delete($template)) {
            $this->Flash->success(__('La talla ha sido borrada correctamente.'));
        } else {
            $this->Flash->error(__('La talla no puede ser borrada. Por favor, pruebe otra vez.'));
        }

        return $this->redirect(['action' => 'templates_list']);
        $this->viewBuilder()->layout('default');
    }

    public function editTemplate($id = null) {
        $template = $this->Templates->find()->where(['Templates.id' => $id])->contain('Options')->first();
        $this->loadModel('Options');
        $options=$this->Options->find();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $template = $this->Templates->patchEntity($template, $this->request->getData(), ['associated' => ['Options']]);
            if ($this->Templates->save($template)) {
                $this->Flash->success(__('La plantilla se ha editado correctamente.'));

                return $this->redirect(['action' => 'templates_list']);
            }
            $this->Flash->error(__('La plantilla no se ha podido editar. Por favor, pruebe otra vez.'));
        }
        $this->set(compact('template','options'));

        $this->viewBuilder()->layout('default');
    }
    
    public function comoCrearUnaPlantilla(){
        $this->viewBuilder()->layout('default');
    }

}
