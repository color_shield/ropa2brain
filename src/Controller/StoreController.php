<?php

// src/Controller/UsersController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

class StoreController extends AppController {

    public $paginate = [
        'limit' => 15,
        'order' => [
            'Attributes.id' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        //$this->loadComponent('Auth'); 
        $this->loadComponent('CmpFiles');
        $this->loadComponent('CmpFeaturedProducts');
        $this->loadComponent('CmpMailer');
        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => true, // true/false
            'sitekey' => '6LeSfFMUAAAAAPFCrU70BYF8_Uv6rOBPfOo_DdiN',
            'secret' => '6LeSfFMUAAAAAE6LY_KEO61dgHI3KuUzAc6eZ0Y5',
            'type' => 'image', // image/audio
            'theme' => 'light', // light/dark
            'lang' => 'es', // default en
            'size' => 'normal'  // normal/compact
        ]);
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }

    public function index() {
        $this->loadModel('Categories');

        $list_new_products = $this->CmpFeaturedProducts->newProducts(4);

        $list_categories = $this->Categories->find()->where(['category_id' => 0])->limit(4);

        $this->set("list_new_products", $list_new_products);
        $this->set("list_categories", $list_categories);

        $this->viewBuilder()->layout('client');
    }

    public function vinylsType() {


        $this->viewBuilder()->layout('client');
    }

    public function faq() {


        $this->viewBuilder()->layout('client');
    }

    public function vinylBasic() {

        $this->viewBuilder()->layout('client');
    }

    public function vinylDeformable() {

        $this->viewBuilder()->layout('client');
    }

    public function vinylSpecial() {

        $this->viewBuilder()->layout('client');
    }

    public function vinylFluor() {

        $this->viewBuilder()->layout('client');
    }

    public function vinylGlitter() {

        $this->viewBuilder()->layout('client');
    }

    public function vinylMetalized() {

        $this->viewBuilder()->layout('client');
    }

    public function vinylMotive() {

        $this->viewBuilder()->layout('client');
    }

    public function vinylTextured() {

        $this->viewBuilder()->layout('client');
    }

    public function sendConditions() {

        $this->viewBuilder()->layout('client');
    }

    public function privacyPolicyAndLegalNotice() {

        $this->viewBuilder()->layout('client');
    }

    public function termsAndConditions() {

        $this->viewBuilder()->layout('client');
    }

    public function aboutUs() {

        $this->viewBuilder()->layout('client');
    }

    public function paySafe() {

        $this->viewBuilder()->layout('client');
    }

    public function contactUs() {
        if ($this->request->is('post')) {
            $response_captcha = $this->request->data('g-recaptcha-response');
            if ($response_captcha == "" || $response_captcha == null || empty($response_captcha)) {
                $this->Flash->error(__('Por favor, marque la casilla de "No soy un robot" del formulario para poder enviarlo.'));
                return $this->redirect(['controller' => 'Store', 'action' => 'contact_us']);
            } else {
                $role_form = $this->request->data('role_form');
                switch ($role_form) {
                    case "contact_us":
                        $contact_name = $this->request->data('contact_name');
                        $contact_destination = $this->request->data('contact_destination');
                        $from = $this->request->data('from');
                        $message = $this->request->data('message');
                        $this->CmpMailer->contactUs($contact_name, $contact_destination, $from, $message);
                        $this->Flash->success(__('Su mensaje ha sido enviado correctamente.'));
                        return $this->redirect(['controller' => 'Store', 'action' => 'contact_us']);
                        break;
                    default:
                        break;
                }
            }
        }
        $this->viewBuilder()->layout('client');
    }

}
