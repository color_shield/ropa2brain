<?php

// src/Controller/UsersController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

class CategoriesController extends AppController {

    public $paginate = [
        'limit' => 15];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Auth'); 
        $this->loadComponent('CmpFiles');
        $this->loadComponent('CmpUsers');
    }
    
    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function addCategory() {
        $categories_list = $this->Categories->find()->where(['Categories.category_id'=>0]);
        $categories_check = $categories_list->first();
        $categories_list = $this->Categories->find()->where(['Categories.category_id'=>0]);
        $cat_control = false;
        if ($categories_check != null) {
            $cat_control = true;
        }

        $new_category = $this->Categories->newEntity();
        if ($this->request->is('post')) {

            /**
             * * START INPUT FILE
             * */
            /** VARIABLES ********************************* */
            $input_file_name = 'image';
            $save_in_folder = 'img' . DS . 'categories';
            $extensions = ['png', 'jpg', 'jpeg'];

            /*             * ******************************************** */
            $input_file_data = $this->request->data[$input_file_name];
            if ($input_file_data['size'] > 0) {
                $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                if ($file_result['error'] != '') {
                    $text_error = __('Ocurrió un error.');
                    switch ($file_result['error']) {
                        case 'MAX_FILES_ERROR':
                            $text_error = 'Excedido el límite maximo de imágenes.';
                            break;
                        case 'NOT_VALID_EXTENSION_ERROR':
                            $text_error = 'Extensión no valida.';
                            break;
                        case 'ERROR_OTHER':
                            $text_error = 'Ocurrió un error.';
                            break;
                    }
                    $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                    return $this->redirect(['action' => 'addCategory']);
                }
                $this->request->data[$input_file_name] = $file_result['file_name'][0];
            }
            /**
             * * END INPUT FILE
             * */
            $new_category = $this->Categories->patchEntity($new_category, $this->request->getData());

            if ($this->Categories->save($new_category)) {
                $this->Flash->success(__('La categoría se ha guardado correctamente.'));

                return $this->redirect(['action' => 'add_category']);
            }
            $this->Flash->error(__('La categoría no ha sido guardada, intentelo de nuevo.'));
        }

        $this->set(compact('categories_list', 'new_category', 'cat_control'));
        $this->viewBuilder()->layout('default');
    }

    public function categoriesList() {
        $categories = $this->Categories->find()->contain('ParentCategories');
        $categories = $this->paginate($categories);
        $this->set(compact('categories'));
    }

    public function delete($category_id) {
        $this->request->allowMethod(['post', 'delete']);
        $category = $this->Categories->get($category_id);
        
        $categories_edit_parent = $this->Categories->find()->where(['Categories.category_id' => $category->id]);
        
        foreach($categories_edit_parent as $c_edit_parent){
            $c_edit_parent->category_id = 0;
            $this->Categories->save($c_edit_parent);
        }

        $delete_file = ['folder'=> 'img' . DS . 'categories','file_name'=> $category->image];
        $this->CmpFiles->deleteFile($delete_file);
        
        if ($this->Categories->delete($category)) {
            $this->Flash->success(__('La categoría ha sido borrada correctamente.'));
        } else {
            $this->Flash->error(__('La categoría no puede ser borrada. Por favor, pruebe otra vez.'));
        }

        return $this->redirect(['action' => 'categories_list']);
        $this->viewBuilder()->layout('default');
    }

    public function editCategory($id = null) {
        $category = $this->Categories->get($id);

        $categories_list = $this->Categories->find()->where(['Categories.category_id'=>0, 'Categories.id != ' => $category->id]);
        $cat_control = $categories_list->first();
        $categories_list = $this->Categories->find()->where(['Categories.category_id'=>0, 'Categories.id != ' => $category->id]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());
            if ($this->request->data['new_image'] != "") {
                $input_file_name = 'new_image';
                $save_in_folder = 'img' . DS . 'categories';
                $extensions = ['png', 'jpg', 'jpeg'];

                /*                 * ******************************************** */
                $input_file_data = $this->request->data[$input_file_name];
                if ($input_file_data['size'] > 0) {
                    $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                    if ($file_result['error'] != '') {
                        $text_error = __('Ocurrió un error.');
                        switch ($file_result['error']) {
                            case 'MAX_FILES_ERROR':
                                $text_error = 'Excedido el límite maximo de imágenes.';
                                break;
                            case 'NOT_VALID_EXTENSION_ERROR':
                                $text_error = 'Extensión no valida.';
                                break;
                            case 'ERROR_OTHER':
                                $text_error = 'Ocurrió un error.';
                                break;
                        }
                        $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                        return $this->redirect(['action' => 'editCategory']);
                    }
                    
                    $delete_file = ['folder'=> $save_in_folder, 'file_name'=> $category->image];
                    $this->CmpFiles->deleteFile($delete_file);
                    
                    $category->image = $file_result['file_name'][0];                    
                }
            }
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('La categoría se ha editado correctamente.'));

                return $this->redirect(['action' => 'categories_list']);
            }
            $this->Flash->error(__('La categoría no se ha podido editar. Por favor, pruebe otra vez.'));
        }
        $this->set(compact('category','categories_list','cat_control'));

        $this->viewBuilder()->layout('default');
    }
}
