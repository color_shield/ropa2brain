<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
    
    public $paginate = [
        'limit' => 15,
        'order' => [
            'Products.id' => 'asc'
        ]
    ];
    
    public function initialize()
    {
        parent::initialize();   
        
        $this->loadComponent('Auth');
        $this->loadComponent('CmpInputSecure');
        $this->loadComponent('CmpUsers');
        $this->loadComponent('CmpFiles');
        
        
    }
    
    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function index()
    {
        
    }
    
    public function productsList(){
        $products = $this->Products->find()->contain(['Categories','Sizes','Properties','Properties.Colours']);
        $products = $this->paginate($products);
        $this->set(compact('products'));
    }
    
    
    public function addProduct()
    {
        $this->loadModel('Templates');
        $this->loadModel('Categories');
        $this->loadModel('Attributes');        
        
        $product = $this->Products->newEntity();
        
        if ($this->request->is('post')) {
            
            /**
            ** START INPUT FILE
            **/
            
            /** VARIABLES **********************************/
            
            $input_file_name = 'image';
            $save_in_folder = 'img' . DS . 'products';
            $extensions = ['png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'];
            
            /***********************************************/            
            $input_file_data = $this->request->data[$input_file_name];  
            if ($input_file_data['size'] > 0) {
                $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                if($file_result['error'] != '') {
                    $text_error = __('Ocurrió un error.');
                    switch ($file_result['error'])
                    {
                        case 'MAX_FILES_ERROR':
                        $text_error = 'Excedido el límite maximo de imágenes.';
                        break;
                        case 'NOT_VALID_EXTENSION_ERROR':
                        $text_error = 'Extensión no valida.';
                        break;
                        case 'ERROR_OTHER':
                        $text_error = 'Ocurrió un error.';
                        break;
                    }
                    $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                    return $this->redirect(['action' => 'addAttribute']);
                }       
                $this->request->data[$input_file_name] = $file_result['file_name'][0];
            }            
            /**
            ** END INPUT FILE
            **/
            
            /** VARIABLES **********************************/
            
            $input_file_name = 'care';
            $save_in_folder = 'img' . DS . 'products';
            $extensions = ['png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'];
            
            /***********************************************/            
            $input_file_data = $this->request->data[$input_file_name];  
            if ($input_file_data['size'] > 0) {
                $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                if($file_result['error'] != '') {
                    $text_error = __('Ocurrió un error.');
                    switch ($file_result['error'])
                    {
                        case 'MAX_FILES_ERROR':
                        $text_error = 'Excedido el límite maximo de imágenes.';
                        break;
                        case 'NOT_VALID_EXTENSION_ERROR':
                        $text_error = 'Extensión no valida.';
                        break;
                        case 'ERROR_OTHER':
                        $text_error = 'Ocurrió un error.';
                        break;
                    }
                    $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                    return $this->redirect(['action' => 'addAttribute']);
                }       
                $this->request->data[$input_file_name] = $file_result['file_name'][0];
            }            
            /**
            ** END INPUT FILE
            **/
                                   
            $product = $this->Products->patchEntity($product, $this->request->data, [ 'associated' => ['Attributes'] ]);
                             
            if ($this->Products->save($product)) {
                $this->Flash->success(__('El producto se ha guardado correctamente.'));

                return $this->redirect(['controller'=>'Features', 'action' => 'stepOne', $product->id]);
            }
            $this->Flash->error(__('El producto no ha sido guardado, intentelo de nuevo.'));
        }
                
        $attributes = $this->Attributes->find();
        //$categories = $this->Categories->find('list', ['limit' => 200]);
        
        $categories = $this->Categories->find()->where(['category_id'=>0])->contain(['SubCategories']);
        
        $templates = $this->Templates->find('list', ['limit' => 200]);
                
        $this->set(compact('product'));
        $this->set(compact('attributes'));
        $this->set(compact('categories'));
        $this->set(compact('templates'));
        
        $this->viewBuilder()->layout('default');
    }

    
    public function editProduct($id)
    {
        $this->loadModel('Templates');
        $this->loadModel('Categories');
        $this->loadModel('Attributes');        
        
        $product = $this->Products->find()->where(['Products.id' => $id])->contain(['Attributes'])->first();
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $data_properties = [];
            if (isset($this->request->data['attributes'])) {
                if ($this->request->data['attributes'] != null || !empty($this->request->data['attributes'])) {
                    $post_properties = $this->request->data['attributes'];

                    for ($i = 0; $i < count($post_properties); $i++) {
                        if (!empty($post_properties[$i]['id'])) {
                            if ($post_properties[$i]['id'] != 0 && $post_properties[$i]['id'] != null && $post_properties[$i]['id'] != '') {
                                $data_properties_element = [];
                                $data_properties_element['id'] = $post_properties[$i]['id'];
                                $data_properties [] = $data_properties_element;
                            }
                        }
                    }
                }
            }
            $this->request->data['attributes'] = $data_properties;
           
                                   
            $product = $this->Products->patchEntity($product, $this->request->getData(), ['associated' => ['Attributes']]);
            
            if ($this->request->data['new_image'] != "") {
               $input_file_name = 'new_image';
               $save_in_folder = 'img' . DS . 'products';
                $extensions = ['png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'];

               /*                 * ******************************************** */
               $input_file_data = $this->request->data[$input_file_name];
               if ($input_file_data['size'] > 0) {
                    $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                    if ($file_result['error'] != '') {
                        $text_error = __('Ocurrió un error.');
                        switch ($file_result['error']) {
                            case 'MAX_FILES_ERROR':
                                $text_error = 'Excedido el límite maximo de imágenes.';
                                break;
                            case 'NOT_VALID_EXTENSION_ERROR':
                                $text_error = 'Extensión no valida.';
                                break;
                            case 'ERROR_OTHER':
                                $text_error = 'Ocurrió un error.';
                                break;
                       }
                       $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                       return $this->redirect(['action' => 'editProduct',$id]);
                    }
                   
                    $delete_file = ['folder'=> $save_in_folder, 'file_name'=> $product->image];
                    $this->CmpFiles->deleteFile($delete_file);
                   
                    $product->image = $file_result['file_name'][0];                    
               }
           }
           
           if ($this->request->data['new_care_image'] != "") {
               $input_file_name = 'new_care_image';
               $save_in_folder = 'img' . DS . 'products';
                $extensions = ['png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'];

               /*                 * ******************************************** */
               $input_file_data = $this->request->data[$input_file_name];
               if ($input_file_data['size'] > 0) {
                    $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                    if ($file_result['error'] != '') {
                        $text_error = __('Ocurrió un error.');
                        switch ($file_result['error']) {
                            case 'MAX_FILES_ERROR':
                                $text_error = 'Excedido el límite maximo de imágenes.';
                                break;
                            case 'NOT_VALID_EXTENSION_ERROR':
                                $text_error = 'Extensión no valida.';
                                break;
                            case 'ERROR_OTHER':
                                $text_error = 'Ocurrió un error.';
                                break;
                       }
                       $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                       return $this->redirect(['action' => 'editProduct',$id]);
                    }
                   
                    $delete_file = ['folder'=> $save_in_folder, 'file_name'=> $product->care];
                    $this->CmpFiles->deleteFile($delete_file);
                   
                    $product->care = $file_result['file_name'][0];                    
               }
           }
                             
            if ($this->Products->save($product)) {
                $this->Flash->success(__('El producto se ha guardado correctamente.'));

                return $this->redirect(['controller'=>'Products', 'action' => 'productsList']);
            }
            $this->Flash->error(__('El producto no ha sido guardado, intentelo de nuevo.'));
        }
                
        $attributes = $this->Attributes->find();
        $categories = $this->Categories->find()->where(['category_id'=>0])->contain(['SubCategories']);
        $templates = $this->Templates->find('list', ['limit' => 200]);
                
        $this->set(compact('product'));
        $this->set(compact('attributes'));
        $this->set(compact('categories'));
        $this->set(compact('templates'));
        
        $this->viewBuilder()->layout('default');
    }
    
    
    public function duplicateProduct($product_id)
    {
        $this->loadModel('Templates');
        $this->loadModel('Categories');
        $this->loadModel('Attributes'); 
        $this->loadModel('AttributesProducts'); 
        $this->loadModel('Features');    
        $this->loadModel('Properties');        
        
        $product_base = $this->Products->find()->where(['Products.id'=> $product_id])->first();
        $features_base = $this->Features->find()->where(['product_id'=> $product_id]);
        $attributes_products_base = $this->AttributesProducts->find()->where(['product_id'=> $product_id]);
        
        $product = $this->Products->newEntity();
                
        $product->name = $product_base->name.' - Copia';
        $product->description = $product_base->description;
        $product->category_id = $product_base->category_id;
        $product->template_id = $product_base->template_id;
        $product->image = $this->CmpFiles->duplicateFile(['folder'=>'img'. DS . 'products','file_name'=>$product_base->image]);
        $product->care = $this->CmpFiles->duplicateFile(['folder'=>'img'. DS . 'products','file_name'=>$product_base->care]);
        

        if ($this->Products->save($product)) {
                                
            foreach ($attributes_products_base as $attribute_product_base){
                $attribute_product = $this->AttributesProducts->newEntity();
                
                $attribute_product->product_id = $product->id;
                $attribute_product->attribute_id = $attribute_product_base->attribute_id;
                
                $this->AttributesProducts->save($attribute_product);
            }
            
            $associated_properties_ids = [];
            
            
            foreach ($features_base as $feature_base){
                
                $property_base = $this->Properties->find()->where(['Properties.id' => $feature_base->property_id])->first();
                
                if(empty($associated_properties_ids[$property_base->id])){
                    $property = $this->Properties->newEntity();
                
                    $property->colour_id = $property_base->colour_id;
                    $property->image = $this->CmpFiles->duplicateFile(['folder'=>'img'. DS . 'properties','file_name'=>$property_base->image]);

                    if ($this->Properties->save($property)) {
                       $associated_properties_ids[$property_base->id] = $property->id;
                    }          
                }
                
                      
            }
                        
            foreach ($features_base as $feature_base){
                
                $feature = $this->Features->newEntity();

                $feature->product_id = $product->id;
                $feature->size_id = $feature_base->size_id;
                $feature->quantity = $feature_base->quantity;
                $feature->price = $feature_base->price;
                $feature->property_id = $associated_properties_ids[$feature_base->property_id];

                $this->Features->save($feature);                               
            }
            
            
            $this->Flash->success(__('El se ha duplicado correctamente.'));
            return $this->redirect(['action' => 'productsList']);
        }
        
        $this->Flash->error(__('El producto no ha sido duplicado, inténtelo de nuevo.'));
        return $this->redirect(['action' => 'productsList']);
    }


    public function delete($product_id) {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($product_id);

        if ($this->Products->delete($product)) {
            $this->Flash->success(__('El producto ha sido borrada correctamente.'));
        } else {
            $this->Flash->error(__('El producto no puede ser borrada. Por favor, pruebe otra vez.'));
        }

        return $this->redirect(['action' => 'productsList']);
        $this->viewBuilder()->layout('default');
    }
}
