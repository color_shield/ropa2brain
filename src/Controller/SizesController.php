<?php

// src/Controller/UsersController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;


class SizesController extends AppController
{
    public $paginate = [
        'limit' => 15,
        'order' => [
            'Attributes.id' => 'asc'
        ]
    ];
    
    public function initialize()
    {
        parent::initialize();   
        $this->loadComponent('Auth'); 
        $this->loadComponent('CmpFiles');
        $this->loadComponent('CmpUsers');
    } 
    
    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }
            
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event); 
    }
    
    public function addSize()
    {        
       $size = $this->Sizes->newEntity();
        if ($this->request->is('post')) {
            
            /**
            ** START INPUT FILE
            **/
            
            /** VARIABLES **********************************/
            
            $input_file_name = 'icon';
            $save_in_folder = 'img' . DS . 'sizes';
            $extensions = ['png', 'jpg', 'jpeg'];
            
            /***********************************************/            
            $input_file_data = $this->request->data[$input_file_name];  
            if ($input_file_data['size'] > 0) {
                $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                if($file_result['error'] != '') {
                    $text_error = __('Ocurrió un error.');
                    switch ($file_result['error'])
                    {
                        case 'MAX_FILES_ERROR':
                        $text_error = 'Excedido el límite maximo de imágenes.';
                        break;
                        case 'NOT_VALID_EXTENSION_ERROR':
                        $text_error = 'Extensión no valida.';
                        break;
                        case 'ERROR_OTHER':
                        $text_error = 'Ocurrió un error.';
                        break;
                    }
                    $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                    return $this->redirect(['action' => 'addSize']);
                }       
                $this->request->data[$input_file_name] = $file_result['file_name'][0];
            }            
            /**
            ** END INPUT FILE
            **/
            
            $size = $this->Sizes->patchEntity($size, $this->request->getData());
                             
            if ($this->Sizes->save($size)) {
                $this->Flash->success(__('la talla se ha guardado correctamente.'));

                return $this->redirect(['action' => 'add_size']);
            }
            $this->Flash->error(__('La talla no ha sido guardada, intentelo de nuevo.'));
        }
                
        $this->set(compact('size'));
        
        $this->viewBuilder()->layout('default');
    }
    
    public function sizesList(){
        $sizes = $this->Sizes->find()->order(['date_order' => 'ASC']);
        $sizes = $this->paginate($sizes);  
        $this->set(compact('sizes'));
    }
    
    public function delete($size_id) {
        $this->loadModel('Features');
        
        
        $this->request->allowMethod(['post', 'delete']);
        $size = $this->Sizes->get($size_id);
        
        $features_del = $this->Features->find()->where(['Features.size_id' => $size->id]);

        $delete_file = ['folder'=> 'img' . DS . 'sizes','file_name'=> $size->icon];
        $this->CmpFiles->deleteFile($delete_file);
        
        if ($this->Sizes->delete($size)) {
                            
            foreach($features_del as $f_del){
                $this->Features->delete($f_del);
            }
            
            $this->Flash->success(__('La talla ha sido borrada correctamente.'));
        } else {
            $this->Flash->error(__('La talla no puede ser borrada. Por favor, pruebe otra vez.'));
        }

        return $this->redirect(['action' => 'sizes_list']);
        $this->viewBuilder()->layout('default');
    }
    
    public function editSize($id = null) {
        $size = $this->Sizes->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $size = $this->Sizes->patchEntity($size, $this->request->getData());
            if ($this->request->data['new_image'] != "") {
                $input_file_name = 'new_image';
                $save_in_folder = 'img' . DS . 'sizes';
                $extensions = ['png', 'jpg', 'jpeg'];

                /*                 * ******************************************** */
                $input_file_data = $this->request->data[$input_file_name];
                if ($input_file_data['size'] > 0) {
                    $file_result = $this->CmpFiles->upload($input_file_data, ['folder' => $save_in_folder, 'multiple' => false, 'extensions' => $extensions]);
                    if ($file_result['error'] != '') {
                        $text_error = __('Ocurrió un error.');
                        switch ($file_result['error']) {
                            case 'MAX_FILES_ERROR':
                                $text_error = 'Excedido el límite maximo de imágenes.';
                                break;
                            case 'NOT_VALID_EXTENSION_ERROR':
                                $text_error = 'Extensión no valida.';
                                break;
                            case 'ERROR_OTHER':
                                $text_error = 'Ocurrió un error.';
                                break;
                        }
                        $this->Flash->error(__($text_error)); // <--- Se crea la traduccion en esta linea en vez de las anteriores.
                        return $this->redirect(['action' => 'editSize']);
                    }
                    
                    $delete_file = ['folder'=> $save_in_folder, 'file_name'=> $size->icon];
                    $this->CmpFiles->deleteFile($delete_file);
                    
                    $size->icon = $file_result['file_name'][0];                    
                }
            }
            if ($this->Sizes->save($size)) {
                $this->Flash->success(__('La talla se ha editado correctamente.'));

                return $this->redirect(['action' => 'sizesList']);
            }
            $this->Flash->error(__('La talla no se ha podido editar. Por favor, pruebe otra vez.'));
        }
        $this->set(compact('size'));

        $this->viewBuilder()->layout('default');
    }
    
    
}

