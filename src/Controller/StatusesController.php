<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\I18n;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StatusesController extends AppController
{
    
    public $paginate = [
        'limit' => 15,
        'order' => [
            'Statuses.id' => 'asc'
        ]
    ];
    
    public function initialize()
    {
        parent::initialize();   
        
        $this->loadComponent('Auth');
        $this->loadComponent('CmpInputSecure');
        $this->loadComponent('CmpUsers');
        $this->loadComponent('CmpFiles');
        
        
    }
    
    public function isAuthorized() {   
        //return true;
        $user = $this->CmpUsers->current();
        if($user->role == 'admin'){
            return true;
        }
        else {           
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $statuses = $this->Statuses->find();
        
        $this->set('statuses',$statuses);
        
        $this->viewBuilder()->layout('default');
    }
    
    
    public function addStatus()
    {
        $status = $this->Statuses->newEntity();
        if ($this->request->is('post')) {
            
            $status = $this->Statuses->patchEntity($status, $this->request->getData());
            
            
            if(substr($status->hex_code,0,1) != '#'){
                $status->hex_code = '#'.$status->hex_code;
            }
                             
            if ($this->Statuses->save($status)) {
                $this->Flash->success(__('El estado se ha guardado correctamente.'));

                return $this->redirect(['action' => 'addStatus']);
            }
            $this->Flash->error(__('El estado no ha sido guardado, intentelo de nuevo.'));
        }
                
        $this->set(compact('status'));
        
        $this->viewBuilder()->layout('default');
    }
    
    public function statusesList(){
        $statuses = $this->Statuses->find();
        $statuses = $this->paginate($statuses);
        $this->set(compact('statuses'));
    }
    

    
    public function editStatus($id = null) {
        
       $status = $this->Statuses->get($id);

       if ($this->request->is(['patch', 'post', 'put'])) {
           $status = $this->Statuses->patchEntity($status, $this->request->getData());
           
            if(substr($status->hex_code,0,1) != '#'){
                $status->hex_code = '#'.$status->hex_code;
            }
           
           if ($this->Statuses->save($status)) {
               $this->Flash->success(__('El estado se ha editado correctamente.'));

               return $this->redirect(['action' => 'statusesList']);
           }
           $this->Flash->error(__('El estado no se ha podido editar. Por favor, pruebe otra vez.'));
       }
       $this->set(compact('status'));

       $this->viewBuilder()->layout('default');
   }

    public function delete($status_id){
        $this->request->allowMethod(['post', 'delete']);
        $status = $this->Statuses->get($status_id);

        if ($this->Statuses->delete($status)) {
            $this->Flash->success(('El estado ha sido borrado correctamente.'));
        } else {
            $this->Flash->error(('El estado no puede ser borrado. Por favor, pruebe otra vez.'));
        }

        return $this->redirect(['action' => 'statusesList']);
        $this->viewBuilder()->layout('default');
    }
}
