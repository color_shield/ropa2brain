
admin_form_stock_disabled = true;

total_item_count_value = 0;

function clear_all_options_multiple(){
    $(".option-multiple-clear").prop('checked', false);
}

function clear_all_options_input_multiple(){
    $(".option-input-multiple-clear").val(0);
}

function select_all_multiple(div_id){
    $('#'+div_id+' input[type="checkbox"]').prop('checked', true);
}

function select_none_multiple(div_id){
    $('#'+div_id+' input[type="checkbox"]').prop('checked', false);
}


function stock_edit_multiple(){
    $( ".stock-edit-multiple-color-checkbox" ).each(function() {
        if($(this).is(':checked') ){
            var color_id = $(this).val();
            var quantity_value = $("#stock-edit-multiple-input-quantity").val();
            var price_value = $("#stock-edit-multiple-input-price").val();
            
            if($.isNumeric(quantity_value)){
                $(".stock-quantity-input-"+color_id).val(quantity_value);
            }
            if($.isNumeric(price_value)){
                $(".stock-price-input-"+color_id).val(price_value);
            }
        }
    });    
}

function edit_features_edit_multiple(){
    $( ".edit-feautes-edit-multiple-color-checkbox" ).each(function() {
        if($(this).is(':checked') ){
            var color_id = $(this).val();
            
            $( ".edit-feautes-multiple-size-checkbox" ).each(function() {
                if($(this).is(':checked') ){
                    var size_id = $(this).val();
                    console.log(".edit-feature-input-size-"+size_id+".edit-feature-input-color-"+color_id);
                    $( ".edit-feature-input-size-"+size_id+".edit-feature-input-color-"+color_id ).prop('checked', true);
                }
                 
            });        
        }
    });    
}

function edit_properies_form_message_action(check_elem){
    
        var color_value = check_elem.val();
        var color_check_start_state = check_elem.attr("start-state");
        var color_check_middle_state = check_elem.attr("middle-state");
        console.log(color_check_middle_state);
        
        if(color_check_start_state == 1){ 
            if(check_elem.is(':checked') ){
                if(color_check_middle_state==0){
                    $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').html("No modificar");
                    $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('red-msg');
                    $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('green-msg');
                    $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('orange-msg');
                    $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').addClass('grey-msg');
                }
                else{
                    $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').html("Editar imagen");
                    $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('grey-msg');
                    $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('green-msg');
                    $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('red-msg');
                    $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').addClass('orange-msg');
                }
                
            }
            else{
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').html("Eliminar");
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('grey-msg');
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('green-msg');
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('orange-msg');
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').addClass('red-msg');
            }
        }
        else{ 
            if(check_elem.is(':checked') ){
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').html("Añadir");
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('grey-msg');
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('orange-msg');
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('red-msg');
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').addClass('green-msg');
            }
            else{
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').html("Sin acción");
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('orange-msg');
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('green-msg');
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').removeClass('red-msg');
                $('.edit-properties-form-element-msg[cstm-id="'+color_value+'"]').addClass('grey-msg');
            }
        }
        
}

function almost_one_element_form(e){
    
    var checkbox_elems = $('.almost-one-element-form input[type="checkbox"]:not(.checkbox-multiple-selection):checked').length;    
  
    if(checkbox_elems <= 0){
        alert("Debe seleccionar al menos 1 elemento.");
        e.preventDefault();  //prevent form from submitting
    }
    /*else{
        e.submit();
    }*/
}

function almost_one_none_element_form(e){
    
    var checkbox_elems = $('.almost-one-none-element-form input[type="checkbox"]:not(.checkbox-multiple-selection):not(:checked)').length;    
  
    if(checkbox_elems <= 0){
        alert("Debe dejar al menos 1 elemento.");
        e.preventDefault();  //prevent form from submitting
    }
    /*else{
        e.submit();
    }*/
}

function no_zero_price_element_form(e){
    
    var input_elems = $('.no-zero-price-element-form input.price-no-zero');    
  
    var count_elements_zero = 0;
  
    $.each(input_elems, function( index, value ) {
        console.log($(this).val());
        if( isNaN( $(this).val() ) ){
            $(this).css("color","red");
            count_elements_zero ++;
            
        }
        else{
            if($(this).val() <= 0){
                $(this).css("color","red");
                count_elements_zero ++;
            }
            else{
                $(this).css("color","#333");                
            }
        }
    });
    
    if(count_elements_zero > 0){
        e.preventDefault();  //prevent form from submitting
        alert("Los precios deben tener un valor superior a 0");
    }
     
}


function status_order_html_select_options_color(elem){
    
    
    /*$.each(elem.children("option"), function( index, value ) {
        $(".status-order-html-select-color").css("background-color",$(this).attr("cstm-hex-code"));
        
    });*/
    
}

function status_order_html_select(elem){
    
    
    $.each(elem.children("option"), function( index, value ) {
        if($(this).val() == elem.val()){
            console.log($(this).attr("cstm-hex-code"));
            $(".status-order-html-select-color").css("background-color",$(this).attr("cstm-hex-code"));
            //alert($(this).attr("cstm-hex-code"));
        }
    });
    
    status_order_html_select_options_color(elem);
    
}


function no_zero_quantity_shop_product_form(e){
    
    var input_elems = $('.product-shop-full-form .shop-product-quantity-input');    
  
    var count_elements_no_zero = 0;
  
    $.each(input_elems, function( index, value ) {
        
        if( isNaN( $(this).val() ) ){
            /*$(this).css("color","red");
            count_elements_zero ++;*/
            
        }
        else{
            if($(this).val() <= 0){
                /*$(this).css("color","red");
                count_elements_zero ++;*/
            }
            else{
                count_elements_no_zero += $(this).val(); 
            }
        }
    });
    
    if(count_elements_no_zero < 5){
        e.preventDefault();  //prevent form from submitting
        alert("Debe seleccionar al menos una cantidad de 5 elemento.");
    }
}

function no_empty_design_select_shop_product_form(e){
    
    var select_elems = $('.product-shop-full-form .shop-product-design-select');    
  
    var count_elements_no_empty = 0;
    var error_no_dye_selected = false;
  
    $.each(select_elems, function( index, value ) {
        
        if( isNaN( $(this).val() ) ){
            /*$(this).css("color","red");
            count_elements_zero ++;*/
            
        }
        else{
            if($(this).val() <= 0){
                /*$(this).css("color","red");
                count_elements_zero ++;*/
            }
            else{
                count_elements_no_empty ++;    
                if($(this).parent().parent().children("td").children(".order_color_chooser").val() <= 0){
                    error_no_dye_selected = true;
                }
                
            }
        }
    });
    
    if(count_elements_no_empty <= 0){
        e.preventDefault();  //prevent form from submitting
        alert("Debe seleccionar al menos un diseño.");
    }
    
    if(error_no_dye_selected){
        e.preventDefault();  //prevent form from submitting
        alert("Debe seleccionar el color de vinilo.");
    }
    
}

function no_empty_image_design_shop_product_form(e){
    
        
        
        
    var select_elems = $('.product-shop-full-form .shop-product-design-select');    
  
    var count_elements_no_empty = 0;
  
    $.each(select_elems, function( index, value ) {
        
        if( isNaN( $(this).val() ) ){
            /*$(this).css("color","red");
            count_elements_zero ++;*/
            
        }
        else{
            if($(this).val() <= 0){
                /*$(this).css("color","red");
                count_elements_zero ++;*/
            }
            else{
                if($(this).parent().parent().children("td").children("div.upload-btn-wrapper").children(".shop-product-design-image").val() == ""){
                    count_elements_no_empty ++;
                }
            }
        }
    });
    
    if(count_elements_no_empty > 0){
        e.preventDefault();  //prevent form from submitting
        alert("Debe seleccionar la imagen para el diseño.");
    }
    
    
    
}

function slide_frame_to(slide_id,number_frame){
    $('#product-images-slider').carousel(parseInt(number_frame));
    //$('#product-images-slider').carousel('pause');
}

function shipping_to_billing_lock_inputs(check_elem){
    if(check_elem.is(':checked') ){
        check_elem.parent().parent().children("div").children("div").children('input:not([type="checkbox"])').prop('disabled', true);
        check_elem.parent().parent().children("div").children("div").children('input:not([type="checkbox"])').prop('required', false);
    }   
    else{
        check_elem.parent().parent().children("div").children("div").children('input:not([type="checkbox"])').prop('disabled', false);
        check_elem.parent().parent().children("div").children("div").children('input:not([type="checkbox"])').prop('required', true);
    }
}

last_option_id_select_dynamic = 0;
last_vinyl_id_select_dynamic = 0;


function select_product_design_vinyl_dynamic(){
    //if(last_vinyl_id_select_dynamic == 0){
    //}
    
    var text_show = "Selecciona un color";
    if(last_vinyl_id_select_dynamic <= 0){
        text_show = "Selecciona un tipo";
    }
    
    $(".order_color_chooser-"+last_option_id_select_dynamic).parent().children("span.ui-selectmenu-button").children('span.ui-selectmenu-text').text(text_show);
    
    
    $(".dye-option-for-option-select-"+last_option_id_select_dynamic).hide();
    $(".dye-option-for-option-select-"+last_option_id_select_dynamic+".for-vinyle-"+last_vinyl_id_select_dynamic).show();
    $(".order_color_chooser-"+last_option_id_select_dynamic).val(0);
    //$(".order_color_chooser-"+last_option_id_select_dynamic).attr("real-value",0);
    
    
    
    console.log("vinyl "+last_vinyl_id_select_dynamic);
    console.log("option "+last_option_id_select_dynamic);
    
    console.log("ocultar .dye-option-for-option-select-"+last_option_id_select_dynamic);
    console.log("Mostrar .dye-option-for-option-select-"+last_option_id_select_dynamic+".for-vinyle-"+last_vinyl_id_select_dynamic);
    

}

function calc_total_count_items_quantity(){
    
    total_item_count_value = 0;
    
    $('[cstm-to-badge]').each(function( index ) {
        total_item_count_value = total_item_count_value + parseInt($( this ).val());
    });
}

function recal_total_vinyl_row_by_vinyl(){
            
    calc_total_count_items_quantity();
    
    $(".shop-product-design-select").each(function( index ) {
            var option_id = $(this).attr("cstm-option-id");
            var vinyl_id = $(this).val();
        
            var vinyl_unit_price = $(".vinyl-option-price-option-id-"+option_id+".vinyl-option-price-vinyl-id-"+vinyl_id).children('span.price-value').html();
            if(vinyl_unit_price == undefined || vinyl_unit_price == "undefined"){
                vinyl_unit_price = "0";
            }
        
            console.log(vinyl_unit_price+" €");
            vinyl_unit_price= vinyl_unit_price.replace(',','.');
            vinyl_unit_price = parseFloat(vinyl_unit_price);
            

            var vinyl_total_in_row = total_item_count_value * vinyl_unit_price;

            vinyl_total_in_row = parseFloat(vinyl_total_in_row);
            vinyl_total_in_row = Math.round(vinyl_total_in_row*100)/100;
            
            if(vinyl_total_in_row<= 0){
                vinyl_total_in_row = '--.--';
            }

            $(".vinyl-option-price-total-"+option_id).children('span.price-value').html(vinyl_total_in_row);
    });
    
}

$(document).ready(function(){
    //alert($("#stock-starts-enabled").html());
    if( $("#stock-starts-enabled").html() == "1"){
        admin_form_stock_disabled = false;
        $("#enable-admin-form-stock").html('<i class="fa fa-lock" aria-hidden="true"></i> Deshabilitar edición');
    }
    else{
        $("#admin-form-stock input").prop('disabled', true);
    }
    
    
    $("#enable-admin-form-stock").on("click",function(){
        if(admin_form_stock_disabled){
            $("#admin-form-stock input").prop('disabled', false);
            admin_form_stock_disabled = false;
            $("#enable-admin-form-stock").html('<i class="fa fa-lock" aria-hidden="true"></i> Deshabilitar edición');
        }
        else{
            $("#admin-form-stock input").prop('disabled', true);
            admin_form_stock_disabled = true;
            $("#enable-admin-form-stock").html('<i class="fa fa-unlock-alt" aria-hidden="true"></i> Habilitar edición');
        }
    });
    
    $("#stock-edit-multiple-input-btn").on("click",function(){
        stock_edit_multiple();
    });
    
    $("#edit-feautes-edit-multiple-input-btn").on("click",function(){
        edit_features_edit_multiple();
    });
    
    $(".edit-multiple-select-all").on("click",function(){
       
        select_all_multiple($(this).attr("cstm-id"));
    });
    
    $(".edit-multiple-select-none").on("click",function(){
        select_none_multiple($(this).attr("cstm-id"));
    });
    
    $(".msew-main-title").on("click",function(){
        if($(this).parent().children(".msew-submit").css("display") == "none"){
            $(this).parent().children(".msew-title").show();
            $(this).parent().children(".msew-content").show();
            $(this).parent().children(".msew-submit").show();
        }
        else{
            $(this).parent().children(".msew-title").hide();
            $(this).parent().children(".msew-content").hide();
            $(this).parent().children(".msew-submit").hide();
        }
    });
    
    
    $(".edit-properties-form-element-image").on("change",function(){
        //$(this).parent().parent().children('div').children('.edit-properties-form-element-check');
       
        $(this).parent().parent().children(".edit-properties-form-name-file-new-image").children('span.image-name').html($(this).val());
        var check_elem = $(this).parent().parent().parent().children('div').children('label').children('.edit-properties-form-element-check');
        check_elem.attr('middle-state',1);
        edit_properies_form_message_action(check_elem);        
    });
    
    $(".edit-properties-form-clear-new-image").on("click",function(){
        $(this).parent().children('span.image-name').html("Ninguna imagen seleccionada");
        console.log($(this).parent().parent().children(".upload-btn-wrapper").children(".edit-properties-form-element-image").val());
        $(this).parent().parent().children(".upload-btn-wrapper").children(".edit-properties-form-element-image").val("");
        var check_elem = $(this).parent().parent().parent().children('div').children('label').children('.edit-properties-form-element-check');
        check_elem.attr('middle-state',0);
        edit_properies_form_message_action(check_elem);        
    });
    
    
    $(".multiple-edit-options-clear-btn").on("click",function(){
        clear_all_options_multiple();
    });
    
    $(".multiple-edit-options-input-clear-btn").on("click",function(){
        clear_all_options_input_multiple();
    });
    
    
    $(".edit-properties-form-element-check").on("click",function(){
        var check_elem = $(this);
        edit_properies_form_message_action(check_elem);
    });
    
    
    
    $(".almost-one-element-form").on("submit",function(e){
        almost_one_element_form(e);        
    });
    
    $(".almost-one-none-element-form").on("submit",function(e){
        almost_one_none_element_form(e);        
    });
    
    $(".no-zero-price-element-form").on("submit",function(e){       
        no_zero_price_element_form(e);        
    });
    
    $(".status-order-html-select").on("change",function(){       
        status_order_html_select($(this));        
    });
    
    status_order_html_select($(".status-order-html-select"));
    
    
    $('.colorpickerField').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                    $(el).val(hex);
                    $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                    $(this).ColorPickerSetColor(this.value);
            }
    })
    .bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
    });
    
    
    $(".product-shop-image-small").on("click",function(){
        var frame_to = $(this).parent().attr('cstm-frame-to');
        console.log(frame_to);
        slide_frame_to('#product-images-slider',frame_to)
    });
    
    $(".product-shop-full-form").on("submit",function(e){
        no_zero_quantity_shop_product_form(e);   
        no_empty_design_select_shop_product_form(e);
        no_empty_image_design_shop_product_form(e);
    });
    
    $(".shipping_to_billing").on("click",function(){
        var check_elem = $(this);
        shipping_to_billing_lock_inputs(check_elem);
    });
    
    $(".shop-product-quantity-input").on("change",function(){
        var curr_val = $(this).val();
        var to_id = $(this).attr("cstm-to-price");
        var unit_price = $(this).attr("cstm-unit-price");
        
        var badge_val = 0;
        
        $('[cstm-to-badge="'+$(this).attr("cstm-to-badge")+'"]').each(function( index ) {
            badge_val = badge_val + parseInt($( this ).val());
        });
        
        badge_val = parseInt(badge_val);
        
        console.log(badge_val);
        
        $("#"+$(this).attr("cstm-to-badge")).text(badge_val);
        
        
        var total = unit_price * curr_val;
        if(total < 0){
            total = 0;
        }
        
        total = parseFloat(total);
        //total = total.toFixed(2);
        total = Math.round(total*100)/100;
        
        //total = $(total).parseNumber(total, {format:"#.###,00", locale:"es"});
        
        $("#"+to_id).html(total+"€");
        
        recal_total_vinyl_row_by_vinyl();
        
    });
    
    var start_colour_table_features_product = $(".colour-activation-show-sizes").first().attr("for-colour-id");
    $('.table-colour-tr').hide();
    $('.table-colour-tr-'+start_colour_table_features_product).show();
    
    $(".colour-activation-show-sizes").on("click",function(){
        var colour_id = $(this).attr("for-colour-id");
        $('.table-colour-tr').hide();
        $('.table-colour-tr-'+colour_id).show();
        $('.panel-features').children(".panel-body").show();
        
    });
    
    
    $(".panel.shide-show > .panel-heading").on("click",function(){
        if($(this).parent().children(".panel-body").css("display")=="none"){
            $(this).children("h3").children("i.fa").removeClass("fa-caret-down");
            $(this).children("h3").children("i.fa").addClass("fa-caret-up");
            $(this).parent().children(".panel-body").show();
        }
        else{            
            $(this).children("h3").children("i.fa").removeClass("fa-caret-up");
            $(this).children("h3").children("i.fa").addClass("fa-caret-down");
            $(this).parent().children(".panel-body").hide();
        }
    });
    
    //$(".order_color_chooser").selectmenu( "option", "disabled", true );
    
    
        
    
        
    $(".shop-product-design-select").on("change",function(){
        last_option_id_select_dynamic = $(this).attr("cstm-option-id");
        last_vinyl_id_select_dynamic = $(this).val();        
        select_product_design_vinyl_dynamic();
        
        $(".vinyl-option-price-option-id-"+last_option_id_select_dynamic).hide();
        $(".vinyl-option-price-option-id-"+last_option_id_select_dynamic+".vinyl-option-price-vinyl-id-"+last_vinyl_id_select_dynamic).show();
        
        
        recal_total_vinyl_row_by_vinyl();
        
                
        /*console.log("===============================================");
        console.log(".vinyl-option-price-option-id-"+last_option_id_select_dynamic);
        console.log(".vinyl-option-price-option-id-"+last_option_id_select_dynamic+".vinyl-option-price-vinyl-id-"+last_vinyl_id_select_dynamic);
        console.log("===============================================");*/
    });
    
    
    
    
    //$(".dye-option-for-option-select").hide();
    
    $(function () {
        $.widget("custom.iconselectmenu", $.ui.selectmenu, {
            _renderItem: function (ul, item) {                
                var li = $("<li>"),
                        wrapper = $("<div>", {text: item.label});

                if (item.disabled) {
                    li.addClass("ui-state-disabled");
                }

                //console.log(item.element[0].className);
               li.addClass(item.element[0].className);

                $("<span>", {
                    style: item.element.attr("data-style"),
                    "class": "ui-icon " + item.element.attr("data-class")
                })
                        .appendTo(wrapper);

                
                return li.append(wrapper).appendTo(ul);
            },
            _renderMenu: function( ul, items ) {
                var that = this;
                $.each( items, function( index, item ) {
                  that._renderItemData( ul, item );
                });
                $(".dye-option-for-option-select").hide();
                select_product_design_vinyl_dynamic();
                
                //console.log("RENDER");
                //$(".order_color_chooser").parent().children("span.ui-selectmenu-button").children('span.ui-selectmenu-text').text("Selecciona un tipo");

              }
              
        });
        
        

        $(".order_color_chooser")
                .iconselectmenu()
                .iconselectmenu("menuWidget")
                .addClass("ui-menu-icons avatar");
        
       $(".order_color_chooser").on( "selectmenuopen", function( event, ui ) {
                
                console.log("OPEN");
        } );
        
    }).ready(function(){
        
        $(".order_color_chooser").parent().children("span.ui-selectmenu-button").children('span.ui-selectmenu-text').text("Selecciona un tipo");
        $(".order_color_chooser").val(0);
        
        $('.ui-selectmenu-button').on("click",function(){
            
            //console.log($(this).parent().children(".order_color_chooser").attr("name"));
            //var value_select = $(this).parent().children(".order_color_chooser").attr("real-value");
            //console.log("the_val = "+value_select);
            //if(value_select == 0 || value_select == null){
            //    console.log("clicked");
                $(this).children('span.ui-selectmenu-text').text("Selecciona un color");
            //}                
        });
        
        //$("li.ui-menu-item")
        
        //$(".order_color_chooser-"+last_option_id_select_dynamic).attr("real-value",0);
        
    });
    
    
    
});
